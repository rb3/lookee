//
//  LSlider.m
//  Writeability
//
//  Created by Ryan Blonna on 24/9/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LSlider.h"

#import "LConstants.h"

#import "LPopupView.h"

#import <Utilities/LMacros.h>
#import <Utilities/LFastMath.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LSlider
#pragma mark -
//*********************************************************************************************************************//





@interface LSlider ()

@property (nonatomic, readonly  , strong) UILabel       *thumb;

@property (nonatomic, readonly  , strong) UILabel       *headLabel;

@property (nonatomic, readonly  , strong) UILabel       *tailLabel;

@property (nonatomic, readwrite , strong) LPopupView    *popup;

@property (nonatomic, readwrite , weak  ) UIView        *popupContainer;

@property (nonatomic, readwrite , assign) NSInteger     currentValue;

@end

@implementation LSlider

#pragma mark - Static Objects
//*********************************************************************************************************************//

static CGSize   const kLSliderDefaultThumbSize  = {24., 24.};

static CGFloat  const kLSliderDefaultTrackWidth = 2.;

static NSRange  const kLSliderDefaultRange      = {1, 10};


#pragma mark - Constructor/Destructor
//*********************************************************************************************************************//

- (instancetype)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) {
        [self setOpaque:NO];

        _sliderColor    = [UIColor LTealColor];

        _sliderFont     = [[UIFont LDefaultFont] fontWithSize:18.];

        _trackWidth     = kLSliderDefaultTrackWidth;

        _thumbSize      = kLSliderDefaultThumbSize;

        _value          = kLSliderDefaultRange.location;

        _currentValue   = kLSliderDefaultRange.location;

        _thumb          = [UILabel new];

        _headLabel      = [UILabel new];

        _tailLabel      = [UILabel new];


        [self setupHeadLabel];
        [self setupTailLabel];
        [self setupThumb];

        [self setRange:kLSliderDefaultRange];

        [self addSubview:self.headLabel];
        [self addSubview:self.tailLabel];
        [self addSubview:self.thumb];
    }

    return self;
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();

    CGContextSaveGState(context);

    CGFloat knobWidth   = [self thumbSize].width;
    CGFloat knobHeight  = [self thumbSize].height;

    CGFloat dashLength  = [self trackWidth];

    CGFloat thumbX      = CGRectGetMidX([self.thumb frame]);
    CGFloat thumbY      = CGRectGetMidY([self.thumb frame]);

    CGFloat minX = CGRectGetMinX(rect) + [self trackWidth];
    CGFloat maxX = CGRectGetMaxX(rect) - [self trackWidth];
    CGFloat midY = CGRectGetMidY(rect);
    CGFloat maxY = (midY - knobHeight /2.);

    CGContextSetLineWidth(context, [self trackWidth]);
    CGContextSetStrokeColorWithColor(context, [self sliderColor].CGColor);

    CGMutablePathRef path = CGPathCreateMutable();

    CGPathMoveToPoint(path, NULL, minX, midY);
    CGPathAddEllipseInRect(path, NULL, ((CGRect){{minX, maxY}, {knobWidth, knobHeight}}));

    CGPathCloseSubpath(path);

    CGPathMoveToPoint(path, NULL, minX + knobWidth, midY);
    CGPathAddLineToPoint(path, NULL, maxX - knobWidth, midY);

    CGPathCloseSubpath(path);

    CGPathAddEllipseInRect(path, NULL, ((CGRect){{maxX - knobWidth, maxY}, {knobWidth, knobHeight}}));

    CGPathCloseSubpath(path);

    CGContextAddPath(context, path);

    CGPathRelease(path);

    CGContextDrawPath(context, kCGPathStroke);

    CGContextRestoreGState(context);
}


#pragma mark - Properties
//*********************************************************************************************************************//

@synthesize trackWidth = _trackWidth;

- (void)setTrackWidth:(CGFloat)trackWidth
{
    {_trackWidth = trackWidth;}

    [self setNeedsDisplay];
}

@synthesize thumbSize = _thumbSize;

- (void)setThumbSize:(CGSize)thumbSize
{
    {_thumbSize = thumbSize;}

    [self.thumb setBounds:((CGRect) {
        .origin = {0},
        .size   = thumbSize
    })];

    [self setNeedsDisplay];
}

@synthesize sliderColor = _sliderColor;

- (void)setSliderColor:(UIColor *)sliderColor
{
    {_sliderColor = sliderColor;}

    [self.thumb setBackgroundColor:sliderColor];
}

@synthesize range = _range;

- (void)setRange:(NSRange)range
{
    {_range = range;}

    NSInteger value = [self valueForCurrentThumbPosition];

    [self.thumb setText:[@(value) stringValue]];
    [self.headLabel setText:[@(self.range.location) stringValue]];
    [self.tailLabel setText:[@(self.range.location + self.range.length -1) stringValue]];
}

@synthesize value = _value;

- (void)setValue:(NSInteger)value
{
    [self setValue:value animated:NO];
}

- (void)setValue:(NSInteger)value animated:(BOOL)animated
{
    if ([self valueForCurrentThumbPosition] != value) {
        (_value = value);

        void $sub(setThumbXCoord, CGFloat xCoord) {
            [self.thumb setFrame:((CGRect){
                .origin = {
                    .x = xCoord,
                    .y = [self trackWidth] /2.
                },
                .size   = [self thumbSize]
            })];
        };

        NSInteger   start   = [self range].location;
        NSInteger   end     = [self range].location + [self range].length -1;

        CGFloat     ratio   = LFMReverseLinearInterpolate(start, end, value);

        CGFloat     xCoord  = (ratio
                               *(
                                 +self.bounds.size.width
                                 -self.thumb.frame.size.width
                                 -self.trackWidth *2.
                                 )
                               +self.trackWidth);

        [self.thumb setText:[@(value) stringValue]];

        if (animated) {
            [UIView animateWithDuration:.3 animations:^{
                setThumbXCoord(xCoord);
            }];
        } else {
            setThumbXCoord(xCoord);
        }
    }
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (void)showNotificationsInView:(UIView *)view
{
    if (![self popup]) {
        [self setPopup:[LPopupView new]];
    }

    if ([self.popup targetView] != view) {
        [self.popup dismissAnimated:YES];
    }

    [self setPopupContainer:view];
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (void)setupHeadLabel
{
    [self.headLabel setBounds:((CGRect) {
        .origin = {0},
        .size   = {
            .width  = (self.thumbSize.width  - self.trackWidth /2.),
            .height = (self.thumbSize.height - self.trackWidth /2.)
        }
    })];

    [self.headLabel setCenter:((CGPoint) {
        .x = CGRectGetMinX(self.bounds) + CGRectGetWidth(self.headLabel.bounds) /2. + [self trackWidth],
        .y = CGRectGetMidY(self.bounds)
    })];

    [self.headLabel setAutoresizingMask:UIViewAutoresizingFlexibleRightMargin];
    [self.headLabel setBackgroundColor:[UIColor clearColor]];
    [self.headLabel setTextAlignment:NSTextAlignmentCenter];
    [self.headLabel setFont:[self sliderFont]];
    [self.headLabel setTextColor:[UIColor LTealColor]];
    [self.headLabel setAdjustsFontSizeToFitWidth:YES];
    [self.headLabel setMinimumScaleFactor:1./3.];
}

- (void)setupTailLabel
{
    [self.tailLabel setBounds:((CGRect) {
        .origin = {0},
        .size   = {
            .width  = (self.thumbSize.width  - self.trackWidth /2.),
            .height = (self.thumbSize.height - self.trackWidth /2.)
        }
    })];

    [self.tailLabel setCenter:((CGPoint) {
        .x = CGRectGetMaxX(self.bounds) - CGRectGetWidth(self.tailLabel.bounds) /2. - [self trackWidth],
        .y = CGRectGetMidY(self.bounds)
    })];

    [self.tailLabel setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin];
    [self.tailLabel setBackgroundColor:[UIColor clearColor]];
    [self.tailLabel setTextAlignment:NSTextAlignmentCenter];
    [self.tailLabel setFont:[self sliderFont]];
    [self.tailLabel setTextColor:[UIColor LTealColor]];
    [self.tailLabel setAdjustsFontSizeToFitWidth:YES];
    [self.tailLabel setMinimumScaleFactor:1./3.];
}

- (void)setupThumb
{
    [self.thumb setBounds:((CGRect) {
        .origin = {0},
        .size   = [self thumbSize]
    })];

    [self.thumb setCenter:((CGPoint) {
        .x = CGRectGetMinX(self.bounds) + CGRectGetWidth(self.thumb.bounds) /2. + [self trackWidth],
        .y = CGRectGetMidY(self.bounds)
    })];

    [self.thumb setBackgroundColor:self.sliderColor];
    [self.thumb setUserInteractionEnabled:YES];
    [self.thumb setTextAlignment:NSTextAlignmentCenter];
    [self.thumb setFont:[self sliderFont]];
    [self.thumb setTextColor:[UIColor whiteColor]];
    [self.thumb setAdjustsFontSizeToFitWidth:YES];
    [self.thumb setMinimumScaleFactor:1./3.];

    [self.thumb.layer setCornerRadius:self.thumbSize.width /2.];

    UIPanGestureRecognizer *recognizer = [UIPanGestureRecognizer new];
    [recognizer addTarget:self action:@selector(didPanThumb:)];

    [self.thumb addGestureRecognizer:recognizer];
}

- (NSInteger)valueForCurrentThumbPosition
{
    CGRect      thumbFrame  = [self.thumb frame];

    CGFloat     ratio       = (thumbFrame.origin.x / (self.bounds.size.width - thumbFrame.size.width));

    NSInteger   start       = [self range].location;
    NSInteger   end         = [self range].location + [self range].length -1;

    return roundf(LFMLinearInterpolate(start, end, ratio));
}


#pragma mark - UIGestureRecognizer Callbacks
//*********************************************************************************************************************//

- (void)didPanThumb:(UIPanGestureRecognizer *)recognizer
{
    CGPoint    location        = [recognizer locationInView:self];
    CGPoint    translation     = [recognizer translationInView:self];

    CGRect     thumbFrame      = [self.thumb frame];

    NSInteger  value           = [self valueForCurrentThumbPosition];

    switch ([recognizer state]) {
        case UIGestureRecognizerStateBegan: {

        } break;
        case UIGestureRecognizerStateChanged: {
            thumbFrame.origin.x += translation.x;

            if (CGRectContainsRect([self bounds], CGRectInset(thumbFrame, -self.trackWidth, -self.trackWidth))) {
                [recognizer.view setFrame:thumbFrame];
            } else if (isless(thumbFrame.origin.x - self.trackWidth, self.bounds.origin.x)) {
                thumbFrame.origin.x = [self trackWidth];
            } else if (isgreater(thumbFrame.origin.x + thumbFrame.size.width + self.trackWidth, self.frame.size.width)) {
                thumbFrame.origin.x = (self.bounds.size.width - thumbFrame.size.width - self.trackWidth);
            }

            [recognizer setTranslation:CGPointZero inView:self];

            [self.thumb setFrame:thumbFrame];

            if ([self currentValue] != value) {
                [self setCurrentValue:value];

                [self.thumb setText:[@(value) stringValue]];

                if ([self.delegate respondsToSelector:@selector(sliderNotificationForValue:)]) {
                    id notification = [self.delegate sliderNotificationForValue:value];

                    if ([notification isKindOfClass:[NSString class]]) {
                        [self.popup setText:notification];
                    } else if ([notification isKindOfClass:[NSDictionary class]]) {
                        [self.popup setValuesForKeysWithDictionary:notification];
                    } else if ([notification isKindOfClass:[UIView class]]) {
                        [self.popup setCustomView:notification];
                    }
                }
            }

            if ([self popupContainer]) {
                if ([self value] != value) {
                    [self.popup presentPointingAtView:self.thumb inView:self.popupContainer animated:YES];
                } else {
                    [self.popup dismissAnimated:YES];
                }
            }
        } break;
        default: {
            if ([self popupContainer]) {
                [self.popup dismissAnimated:YES];
            }

            [self setValue:value animated:YES];
            [self.delegate slider:self didChangeToValue:value];
        } break;
    }
}

@end