//
//  LLineLayer.m
//  Writeability
//
//  Created by Jelo Agnasin on 10/9/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//

#import "LLineLayer.h"

@interface LLineLayer ()

@end

@implementation LLineLayer


#pragma mark - Static Constant
//*********************************************************************************************************************//

const CGFloat kLineLayerHeight = 1.0;


#pragma mark - Memory Management
//*********************************************************************************************************************//

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    self.frame = CGRectMake(0, 0, 0, kLineLayerHeight);
    
    return self;
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (void)setFrame:(CGRect)frame
{
    CGRect newFrame = frame;
    newFrame.size.height = kLineLayerHeight;
    [super setFrame:newFrame];
}

@end
