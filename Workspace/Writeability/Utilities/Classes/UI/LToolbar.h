//
//  LTopToolbar.h
//  Writeability
//
//  Created by Jelo Agnasin on 10/1/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import <UIKit/UIKit.h>



@interface LToolbar : UIView

@property (nonatomic, readwrite , strong) NSArray   *leftButtons;
@property (nonatomic, readwrite , strong) NSArray   *rightButtons;

@property (nonatomic, readwrite , strong) UIView    *centerView;

@property (nonatomic, readwrite , assign) BOOL      shouldOffset;


- (CGFloat)preferredHeight;

- (UIButton *)leftButtonWithTag:(NSInteger)tag;
- (UIButton *)rightButtonWithTag:(NSInteger)tag;

- (void)addTarget:(id)target action:(SEL)action;
- (void)removeTarget:(id)target action:(SEL)action;

- (void)addTargetsFromToolbar:(LToolbar *)toolbar;
- (void)removeTargetsInToolbar:(LToolbar *)toolbar;

- (void)selectButtonWithTag:(NSInteger)tag;
- (void)selectButtonWithTag:(NSInteger)tag sendEvent:(BOOL)sendEvent;

@end