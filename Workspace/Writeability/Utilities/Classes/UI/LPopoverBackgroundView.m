//
//  LPopoverBackgroundView.m
//  Writeability
//
//  Created by Jelo Agnasin on 10/3/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//

#import "LPopoverBackgroundView.h"

// first-party
#import <QuartzCore/QuartzCore.h>


#pragma mark - LPopoverBackgroundView Interface
//*********************************************************************************************************************//

@interface LPopoverBackgroundView ()

@property (nonatomic, readwrite, strong) UIView *backgroundView;

@property (nonatomic, readwrite, assign) CGFloat arrowOffset;
@property (nonatomic, readwrite, assign) UIPopoverArrowDirection arrowDirection;

@end


//*********************************************************************************************************************//
#pragma mark -
#pragma mark LPopoverBackgroundView
#pragma mark -
//*********************************************************************************************************************//

/***
 * LPopoverBackgroundView
 *
 * Strip down version of UIPopoverBackgroundView
 *
 * - Removes drop shadows.
 * - Removes inset shadows.
 * - Removes rounded corners.
 * - Removes thick border lines.
 * - Removes arrow. (This is accomplished by NOT adding arrowView as subview)
 */

@implementation LPopoverBackgroundView

@synthesize arrowOffset = _arrowOffset;
@synthesize arrowDirection = _arrowDirection;


#pragma mark - Memory Management
//*********************************************************************************************************************//

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.shadowColor = [UIColor clearColor].CGColor; // removes drop shadows.
        
        self.backgroundView = [UIView new];
        self.backgroundView.backgroundColor = [UIColor clearColor];
        [self addSubview:self.backgroundView];
        // [self addSubview:self.arrowView];
    }
    return self;
}


#pragma mark - Layout Subviews
//*********************************************************************************************************************//

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat arrowHeight = [LPopoverBackgroundView arrowHeight];
    
    CGRect frame = self.bounds;
    frame.origin.y = arrowHeight;
    frame.size.height -= arrowHeight;
    self.backgroundView.frame = frame;
}


#pragma mark - Properties
//*********************************************************************************************************************//

+ (UIEdgeInsets)contentViewInsets
{
    return UIEdgeInsetsMake(0, 0, 0, 0); // removes thick border lines.
}

+ (CGFloat)arrowHeight
{
    return 8; // this would offset the popover from the origin
}

+ (CGFloat)arrowBase
{
    return 0;
}

+ (BOOL)wantsDefaultContentAppearance
{
    return NO; // removes rounded corners and inset shadows.
}

@end