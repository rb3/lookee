//
//  LTextBox.h
//  Writeability
//
//  Created by Ryan on 12/4/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LFloatingView.h"



@class LTextBox;


@protocol LTextBoxDelegate <LFloatingViewDelegate>
@required

- (void)textBoxDidBeginEditing:(LTextBox *)textBox;
- (void)textBoxDidEndEditing:(LTextBox *)textBox;

- (void)textBoxDidChangeText:(LTextBox *)textBox;
- (void)textBoxDidChangeTextRect:(LTextBox *)textBox;


@optional

- (void)textBox:(LTextBox *)textBox willChangeHeight:(CGFloat)height;
- (void)textBox:(LTextBox *)textBox didChangeHeight:(CGFloat)height;

@end



@interface LTextBox : LFloatingView

@property (nonatomic, readwrite , weak  ) id                    <LTextBoxDelegate>delegate;

@property (nonatomic, readwrite , assign) CGFloat               minimumHeight;
@property (nonatomic, readwrite , assign) CGFloat               maximumHeight;

@property (nonatomic, readwrite , strong) NSString              *placeholderText;
@property (nonatomic, readwrite , strong) UIColor               *placeholderColor;

@property (nonatomic, readwrite , strong) NSDictionary          *editingAttributes;
@property (nonatomic, readwrite , strong) NSAttributedString    *attributedText;

@property (nonatomic, readonly  , assign) CGRect                textRect;



- (instancetype)initWithFrame:(CGRect)frame limits:(CGRect)limits attributes:(NSDictionary *)typingAttributes;


- (BOOL)hasText;

@end
