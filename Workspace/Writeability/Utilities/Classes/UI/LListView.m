//
//  LListView.m
//  Writeability
//
//  Created by Ryan Blonna on 19/11/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LListView.h"

#import "LConstants.h"

#import "LButton.h"

#import "RATreeView.h"

#import "SWTableViewCell.h"

#import <Utilities/UIView+Utilities.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark NSIndexPath+LListView
#pragma mark -
//*********************************************************************************************************************//





@implementation NSIndexPath (LListView)

#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

+ (instancetype)indexPathWithItemIndex:(NSUInteger)itemIndex optionIndex:(NSUInteger)optionIndex
{
    const NSUInteger indexes[2] = {itemIndex, optionIndex};

    return [self indexPathWithIndexes:indexes length:sizeof(indexes) / sizeof(*indexes)];
}


#pragma mark - Properties
//*********************************************************************************************************************//

- (NSUInteger)itemIndex
{
    return [self indexAtPosition:0];
}

- (NSUInteger)optionIndex
{
    return [self indexAtPosition:1];
}

@end





//*********************************************************************************************************************//
#pragma mark -
#pragma mark LListView
#pragma mark -
//*********************************************************************************************************************//





@interface LListView () <UIGestureRecognizerDelegate, RATreeViewDelegate, RATreeViewDataSource, SWTableViewCellDelegate>

@property (nonatomic, readonly  , strong) RATreeView        *treeView;

@property (nonatomic, readonly  , strong) SWTableViewCell   *headerCell;
@property (nonatomic, readonly  , strong) UITableViewCell   *footerCell;

@end

@implementation LListView

#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) {
        _treeView   = [[RATreeView alloc] initWithFrame:CGRectZero style:RATreeViewStylePlain];

        _headerCell = [[SWTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];

        _footerCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];

        CGFloat headerHeight    = 44.;
        CGFloat footerHeight    = 24.;

        CGFloat headerPadding   = 0.;
        CGFloat footerPadding   = 0.;

        CGRect  headerFrame = {
            .origin = {0},
            .size   = {
                .width  = CGRectGetWidth(frame),
                .height = headerHeight
            }
        };

        CGRect  footerFrame = {
            .origin = {
                .x = 0.,
                .y = CGRectGetHeight(frame) - footerHeight
            },
            .size   = {
                .width  = CGRectGetWidth(frame),
                .height = footerHeight
            }
        };

        CGRect  treeViewFrame = {
            .origin = {
                .x = 0.,
                .y = CGRectGetHeight(headerFrame) + headerPadding
            },
            .size   = {
                .width  = frame.size.width,
                .height = CGRectGetMinY(footerFrame) - footerPadding
            }
        };

        UIGestureRecognizer *recognizer = [UITapGestureRecognizer new];

        [recognizer setDelegate:self];
        [recognizer addTarget:self action:@selector(headerCellTapped:)];

        [self.headerCell setFrame:headerFrame];
        [self.headerCell setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
        [self.headerCell setBottomBorderWidth:1.];
        [self.headerCell setBottomBorderColor:[UIColor LTealColor]];
        [self.headerCell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        [self.headerCell setDelegate:self];
        [self.headerCell addGestureRecognizer:recognizer];

        [self.footerCell setFrame:footerFrame];
        [self.footerCell setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
        [self.footerCell setTopBorderWidth:1.];
        [self.footerCell setTopBorderColor:[UIColor LTealColor]];

        [self.treeView setBackgroundColor:[UIColor clearColor]];
        [self.treeView setFrame:treeViewFrame];
        [self.treeView setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
        [self.treeView setDataSource:self];
        [self.treeView setDelegate:self];
        [self.treeView setSeparatorStyle:RATreeViewCellSeparatorStyleNone];
        [self.treeView setRowsCollapsingAnimation:RATreeViewRowAnimationFade];

        [self addSubview:self.headerCell];
        [self addSubview:self.footerCell];
        [self addSubview:self.treeView];
    }
    
    return self;
}


#pragma mark - Properties
//*********************************************************************************************************************//

@synthesize headerAttributes = _headerAttributes;

- (void)setHeaderAttributes:(NSDictionary *)headerAttributes
{
    {_headerAttributes = [headerAttributes copy];}

    for (NSString *attributeName in headerAttributes) {
        [self.headerCell setValue:headerAttributes[attributeName] forKeyPath:attributeName];
    }

    [self.headerCell setNeedsLayout];
}


@synthesize footerAttributes = _footerAttributes;

- (void)setFooterAttributes:(NSDictionary *)footerAttributes
{
    {_footerAttributes = [footerAttributes copy];}

    for (NSString *attributeName in footerAttributes) {
        [self.footerCell setValue:footerAttributes[attributeName] forKeyPath:attributeName];
    }

    [self.footerCell setNeedsLayout];
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (void)batchUpdateListWithVerb:(void(^)(void))verb
{
    [self.treeView beginUpdates];
    {
        verb();
    }
    [self.treeView endUpdates];
}

- (void)reload
{
    [self.treeView reloadData];
}

- (void)reloadItemAtIndex:(NSUInteger)index animated:(BOOL)animated
{
    RATreeViewRowAnimation animation = animated? RATreeViewRowAnimationFade: RATreeViewRowAnimationNone;

    [self.treeView collapseRowForItem:@(index) withRowAnimation:animation];
    [self.treeView reloadRowsForItems:@[@(index)] withRowAnimation:animation];
}

- (void)insertItemAtIndex:(NSUInteger)index animated:(BOOL)animated
{
    RATreeViewRowAnimation animation = animated? RATreeViewRowAnimationLeft: RATreeViewRowAnimationNone;

    [self.treeView insertItemsAtIndexes:[NSIndexSet indexSetWithIndex:0] inParent:nil withAnimation:animation];
}

- (void)deleteItemAtIndex:(NSUInteger)index animated:(BOOL)animated
{
    RATreeViewRowAnimation animation = animated? RATreeViewRowAnimationLeft: RATreeViewRowAnimationNone;

    [self.treeView deleteItemsAtIndexes:[NSIndexSet indexSetWithIndex:0] inParent:nil withAnimation:animation];
}

- (void)showLeftHeaderOptionsAnimated:(BOOL)animated
{
    [self.headerCell showLeftUtilityButtonsAnimated:animated];
}

- (void)showRightHeaderOptionsAnimated:(BOOL)animated
{
    [self.headerCell showRightUtilityButtonsAnimated:animated];
}

- (void)hideHeaderOptionsAnimated:(BOOL)animated
{
    [self.headerCell hideUtilityButtonsAnimated:animated];
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (NSRange)rangeForItems:(NSArray *)items
{
    NSUInteger location = NSNotFound;

    if ([items count]) {
        location = [items[ 0 ] integerValue];
    }

    return ((NSRange) {
        .length     = items.count,
        .location   = location
    });
}


#pragma mark - RATreeViewDelegate
//*********************************************************************************************************************//

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if ([self.delegate respondsToSelector:@selector(listView:didScrollToItemsInRange:)]) {
        NSArray *items = [self.treeView itemsForVisibleCells];

        [self.delegate listView:self didScrollToItemsInRange:[self rangeForItems:items]];
    }
}

- (void)treeView:(RATreeView *)treeView didSelectRowForItem:(id)item
{
    if ([item isKindOfClass:[NSIndexPath class]]) {
        id parent = @([item itemIndex]);
        
        [self.treeView collapseRowForItem:parent];

        if ([self.delegate respondsToSelector:@selector(listView:didSelectOptionAtIndexPath:)]) {
            [self.delegate listView:self didSelectOptionAtIndexPath:item];
        }
    }
}

- (BOOL)treeView:(RATreeView *)treeView canEditRowForItem:(id)item
{
    return NO;
}

- (BOOL)treeView:(RATreeView *)treeView shouldExpandRowForItem:(id)item
{
    return [item isKindOfClass:[NSNumber class]];
}

- (CGFloat)treeView:(RATreeView *)treeView heightForRowForItem:(id)item
{
    if (![item isKindOfClass:[NSNumber class]]) {
        return 32.;
    }

    return 44.;
}

- (NSInteger)treeView:(RATreeView *)treeView indentationLevelForRowForItem:(id)item
{
    if (![item isKindOfClass:[NSNumber class]]) {
        return 2;
    }

    return 1;
}


#pragma mark - UIGestureRecognizerDelegate
//*********************************************************************************************************************//

- (BOOL)
gestureRecognizer                                   :(UIGestureRecognizer *)gestureRecognizer
shouldRecognizeSimultaneouslyWithGestureRecognizer  :(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}


#pragma mark - UITableViewDataSource
//*********************************************************************************************************************//

- (NSInteger)treeView:(RATreeView *)treeView numberOfChildrenOfItem:(id)item
{
    if (item) {
        NSUInteger optionCount = 0;

        if ([self.delegate respondsToSelector:@selector(listView:optionCountForItemAtIndex:)]) {
            optionCount = [self.delegate listView:self optionCountForItemAtIndex:[item integerValue]];
        }

        return optionCount;
    }

    return [self.delegate itemCountForListView:self];
}

- (id)treeView:(RATreeView *)treeView child:(NSInteger)index ofItem:(id)item
{
    if (item) {
        return [NSIndexPath indexPathWithItemIndex:[item integerValue] optionIndex:index];
    }

    return [NSNumber numberWithUnsignedInteger:index];
}

- (UITableViewCell *)treeView:(RATreeView *)treeView cellForItem:(id)item
{
    static NSString *const kLListViewItemReuseIdentifier    = @"Item";
    static NSString *const kLListViewOptionReuseIdentifier  = @"Option";

    NSString *reuseIdentifier = ([item isKindOfClass:[NSNumber class]]?
                                 kLListViewItemReuseIdentifier:
                                 kLListViewOptionReuseIdentifier);

    UITableViewCell *cell = [self.treeView dequeueReusableCellWithIdentifier:reuseIdentifier];

    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];

        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

        if ([item isKindOfClass:[NSNumber class]]) {
            if ([self.delegate respondsToSelector:@selector(listView:optionCountForItemAtIndex:)]) {
                if ([self.delegate listView:self optionCountForItemAtIndex:[item integerValue]]) {
                    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                }
            }
        }
    }

    NSDictionary *attributes;

    if ([item isKindOfClass:[NSNumber class]]) {
        attributes = [self.delegate listView:self attributesForItemAtIndex:[item integerValue]];
    } else {
        attributes = [self.delegate listView:self attributesForOptionAtIndexPath:item];
    }

    {
        for (NSString *attributeName in attributes) {
            [cell setValue:attributes[attributeName] forKeyPath:attributeName];
        }
    }

    return cell;
}


#pragma mark - SWTableViewCellDelegate
//*********************************************************************************************************************//

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index;
{
    if ([self.delegate respondsToSelector:@selector(listView:didSelectRightHeaderOptionAtIndex:)]) {
        [self.delegate listView:self didSelectRightHeaderOptionAtIndex:index];
    }
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index;
{
    if ([self.delegate respondsToSelector:@selector(listView:didSelectLeftHeaderOptionAtIndex:)]) {
        [self.delegate listView:self didSelectLeftHeaderOptionAtIndex:index];
    }
}


#pragma mark - UIGestureRecognizer Callbacks
//*********************************************************************************************************************//

- (void)headerCellTapped:(UITapGestureRecognizer *)recognizer
{
    if ([recognizer state] == UIGestureRecognizerStateRecognized) {
        if ([self.headerCell isUtilityButtonsHidden]) {
            [self.headerCell showRightUtilityButtonsAnimated:YES];
        }
    }
}


#pragma mark - UIButton Callbacks
//*********************************************************************************************************************//

- (void)headerAccessoryPressed:(LButton *)button
{
    [self.headerCell showRightUtilityButtonsAnimated:YES];
}

@end
