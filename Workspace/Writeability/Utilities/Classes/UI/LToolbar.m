//
//  LTopToolbar.m
//  Writeability
//
//  Created by Jelo Agnasin on 10/1/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LToolbar.h"

#import "LConstants.h"

#import "LLineLayer.h"

#import <Utilities/NSObject+Utilities.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LTopToolbar
#pragma mark -
//*********************************************************************************************************************//




@interface LToolbar ()

@property (nonatomic, readonly  , strong) NSMapTable *actions;

@end

@implementation LToolbar

#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)init
{
    if ((self = [super init])) {
        _actions                = [NSMapTable strongToWeakObjectsMapTable];

        self.backgroundColor    = [UIColor whiteColor];
    }
    
    return self;
}

#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (void)layoutSubviews
{
    [super layoutSubviews];

    CGFloat offsetY = self.shouldOffset? 20.: 0.;
    CGFloat height  = self.bounds.size.height;

    CGRect frame;
    CGFloat x;

    x = LC_DEFAULT_MARGIN;

    for (UIButton *leftButton in [self leftButtons]) {
        frame               = leftButton.frame;
        frame.origin.x      = x;
        frame.origin.y      = ((height + offsetY) - frame.size.height) /2.;
        leftButton.frame    = frame;

        [self addSubview:leftButton];
        
        x = (x + frame.size.width + floorf(frame.size.width /5.));
    }
    

    x = (self.bounds.size.width - LC_DEFAULT_MARGIN);

    for (UIButton *rightButton in [self.rightButtons reverseObjectEnumerator]) {
        frame               = rightButton.frame;
        frame.origin.x      = (x - frame.size.width);
        frame.origin.y      = ((height + offsetY) - frame.size.height) /2.;
        rightButton.frame   = frame;

        [self addSubview:rightButton];
        
        x = (x - frame.size.width - floorf(frame.size.width /5.));
    }
    
    
    if ([self centerView]) {
        frame           = self.centerView.bounds;
        frame.origin.x  = (self.bounds.size.width - frame.size.width) /2.;
        frame.origin.y  = ((height + offsetY) - frame.size.height) /2.;

        [self.centerView setFrame:frame];
    }
}


#pragma mark - Properties
//*********************************************************************************************************************//

- (void)setLeftButtons:(NSArray *)leftButtons
{
    for (id button in [self leftButtons]) {
        for (NSString *selector in [self actions]) {
            id object = [self.actions objectForKey:selector];

            [button removeTarget:object action:NSSelectorFromString(selector) forControlEvents:UIControlEventTouchUpInside];
        }
    }

    [self.leftButtons makeObjectsPerformSelector:@selector(removeFromSuperview)];
    {_leftButtons = leftButtons;}

    for (id button in [self leftButtons]) {
        for (NSString *selector in [self actions]) {
            id object = [self.actions objectForKey:selector];

            [button addTarget:object action:NSSelectorFromString(selector) forControlEvents:UIControlEventTouchUpInside];
        }
    }

    [self setNeedsLayout];
    [self layoutIfNeeded];
}

- (void)setRightButtons:(NSArray *)rightButtons
{
    for (id button in [self rightButtons]) {
        for (NSString *selector in [self actions]) {
            id object = [self.actions objectForKey:selector];

            [button removeTarget:object action:NSSelectorFromString(selector) forControlEvents:UIControlEventTouchUpInside];
        }
    }

    [self.rightButtons makeObjectsPerformSelector:@selector(removeFromSuperview)];
    {_rightButtons = rightButtons;}

    for (id button in [self rightButtons]) {
        for (NSString *selector in [self actions]) {
            id object = [self.actions objectForKey:selector];

            [button addTarget:object action:NSSelectorFromString(selector) forControlEvents:UIControlEventTouchUpInside];
        }
    }

    [self setNeedsLayout];
    [self layoutIfNeeded];
}

- (void)setCenterView:(UIView *)centerView
{
    [_centerView removeFromSuperview];
    {_centerView = centerView;}

    [self addSubview:_centerView];
    
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

- (BOOL)shouldOffset
{
    return (_shouldOffset && [UIDevice currentDevice].systemVersion.floatValue >= 7.);
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (CGFloat)preferredHeight
{
    if ([self shouldOffset]) {
        return 64.;
    } else {
        return 44.;
    }
}

- (UIButton *)leftButtonWithTag:(NSInteger)tag
{
    for (UIButton *button in [self leftButtons]) {
        if ([button tag] == tag) {
            return button;
        }
    }

    return nil;
}

- (UIButton *)rightButtonWithTag:(NSInteger)tag
{
    for (UIButton *button in [self rightButtons]) {
        if ([button tag] == tag) {
            return button;
        }
    }

    return nil;
}

- (void)addTarget:(id)target action:(SEL)action
{
    [self.actions setObject:target forKey:NSStringFromSelector(action)];

    for (id button in [self leftButtons]) {
        [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    }

    for (id button in [self rightButtons]) {
        [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)removeTarget:(id)target action:(SEL)action
{
    if ([self.actions objectForKey:NSStringFromSelector(action)] == target) {
        for (id button in [self leftButtons]) {
            [button removeTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
        }

        for (id button in [self rightButtons]) {
            [button removeTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
        }
    }
}

- (void)addTargetsFromToolbar:(LToolbar *)toolbar
{
    for (NSString *action in [toolbar actions]) {
        id  target      = [toolbar.actions objectForKey:action];
        SEL selector    = NSSelectorFromString(action);

        [self addTarget:target action:selector];
    }
}

- (void)removeTargetsInToolbar:(LToolbar *)toolbar
{
    for (NSString *action in [toolbar actions]) {
        id target = [toolbar.actions objectForKey:action];

        if ([self.actions objectForKey:action] == target) {
            SEL selector = NSSelectorFromString(action);

            [self removeTarget:target action:selector];
        }
    }
}

- (void)selectButtonWithTag:(NSInteger)tag
{
    [self selectButtonWithTag:tag sendEvent:NO];
}

- (void)selectButtonWithTag:(NSInteger)tag sendEvent:(BOOL)sendEvent
{
    id button = [self viewWithTag:tag];

    if ([button isKindOfClass:[UIButton class]]) {
        for (id button in [self subviews]) {
            if ([button isKindOfClass:[UIButton class]] && [button isSelected]) {
                [button setSelected:NO];
                break;
            }
        }

        if (sendEvent) {
            [button sendActionsForControlEvents:UIControlEventTouchUpInside];
        } else {
            [button setSelected:YES];
        }
    }
}

@end
