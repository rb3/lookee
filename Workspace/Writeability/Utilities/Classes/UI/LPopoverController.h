//
//  LPopoverController.h
//  Writeability
//
//  Created by Ryan on 8/19/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//

#import <UIKit/UIKit.h>


// TODO: Fix this up
@interface LPopoverController : UIPopoverController

@property (nonatomic, readwrite, weak) UIView *presenterView;
@property (nonatomic, readonly, weak) UIView *contentView;
// FIXME: This property is not working
@property (nonatomic, readwrite, strong) UIColor *borderColor;

@property (nonatomic, readwrite, assign) UIPopoverArrowDirection arrowDirection;
@property (nonatomic, readwrite, copy) CGSize (^contentSizeCallback)(LPopoverController *popover);
@property (nonatomic, readwrite, copy) void (^dismissCallback)(LPopoverController *popover);

- (instancetype)initWithContentView:(UIView *)contentView;

- (void)presentPopoverAnimated:(BOOL)animated;

@end
