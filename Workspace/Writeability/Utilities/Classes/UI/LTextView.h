//
//  LTextView.h
//  Writeability
//
//  Created by Ryan on 2/18/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <UIKit/UIKit.h>



@interface LTextView : UITextView

@property (nonatomic, readwrite, strong) NSString   *placeholderText;
@property (nonatomic, readwrite, strong) UIColor    *placeholderColor;

@end
