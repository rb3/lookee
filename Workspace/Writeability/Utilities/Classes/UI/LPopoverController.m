//
//  LPopoverController.m
//  Writeability
//
//  Created by Ryan on 8/19/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//

#import "LPopoverController.h"

#import "LConstants.h"

// background view
#import "LPopoverBackgroundView.h"

// first-party
#import <QuartzCore/QuartzCore.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Private Classes
#pragma mark -
//*********************************************************************************************************************//




@interface LPopoverContentController : UIViewController

- (instancetype)initWithContentView:(UIView *)contentView;

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LPopoverController
#pragma mark -
//*********************************************************************************************************************//





@interface LPopoverController () <UIPopoverControllerDelegate, UINavigationControllerDelegate>

@end

@implementation LPopoverController


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)initWithContentView:(UIView *)contentView
{
    LPopoverContentController *content = [[LPopoverContentController alloc] initWithContentView:contentView];

    if ((self = [super initWithContentViewController:content])) {
        _contentView = contentView;

        [self initialize];
    }

	return self;
}

- (instancetype)initWithContentViewController:(UIViewController *)viewController
{
    self = [super initWithContentViewController:viewController];
    if (!self) return nil;
    
    [self initialize];
    if ([viewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navController = (UINavigationController *)viewController;
        navController.delegate = self;
    }
    
    return self;
}

- (void)initialize
{
    self.delegate                   = self;
    self.arrowDirection             = UIPopoverArrowDirectionUp;
    self.popoverBackgroundViewClass = [LPopoverBackgroundView class];

    [self setBorderColor:[UIColor LBorderColor]];

    self.contentViewController.view.layer.borderColor   = [self borderColor].CGColor;
    self.contentViewController.view.layer.borderWidth   = 1.0;
    self.contentViewController.view.layer.cornerRadius  = 3.0;
    self.contentViewController.view.layer.masksToBounds = YES;
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (void)presentPopoverAnimated:(BOOL)animated
{
    assert(self.presenterView);
    assert(self.contentSizeCallback);
    
    CGSize size = self.contentSizeCallback(self);
    [self setPopoverContentSize:size animated:NO];
    [self
     presentPopoverFromRect:CGRectInset(self.presenterView.bounds, 0., 8.)
     inView:self.presenterView
     permittedArrowDirections:self.arrowDirection
     animated:animated];
}

- (void)dismissPopoverAnimated:(BOOL)animated
{
    [super dismissPopoverAnimated:animated];
    if (self.dismissCallback) self.dismissCallback(self);
}


#pragma mark - UIPopoverController Delegate
//*********************************************************************************************************************//

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    if (self.dismissCallback) self.dismissCallback(self);
}


#pragma mark - UINavigationController Delegate
//*********************************************************************************************************************//

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    viewController.contentSizeForViewInPopover = navigationController.topViewController.view.frame.size;
}


@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LPopoverContentController
#pragma mark -
//*********************************************************************************************************************//





@interface LPopoverContentController ()

@property (nonatomic, readwrite, strong) UIView *contentView;

@end

@implementation LPopoverContentController


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)initWithContentView:(UIView *)contentView
{
    self = [super init];
    if (!self) return nil;
    
    self.contentView = contentView;
    
    return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	[self.view addSubview:self.contentView];
}

@end
