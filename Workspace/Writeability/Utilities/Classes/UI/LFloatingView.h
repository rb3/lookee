//
//  LFloatingView.h
//  Writeability
//
//  Created by Ryan on 11/26/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LContainerView.h"



typedef NS_OPTIONS(Byte, LFloatingViewHandle) {
    kLFloatingViewHandleUpperLeft   = 1 << 0,
    kLFloatingViewHandleUpperRight  = 1 << 1,
    kLFloatingViewHandleLowerRight  = 1 << 2,
    kLFloatingViewHandleLowerLeft   = 1 << 3,
    kLFloatingViewHandleUpperMiddle = 1 << 4,
    kLFloatingViewHandleLowerMiddle = 1 << 5,
    kLFloatingViewHandleMiddleLeft  = 1 << 6,
    kLFloatingViewHandleMiddleRight = 1 << 7
};



@protocol LFloatingViewDelegate;



@interface LFloatingView : LContainerView

@property (nonatomic, readonly  , strong) UIView                *content;
@property (nonatomic, readwrite , assign) CGRect                limits;

@property (nonatomic, readwrite , assign) LFloatingViewHandle   visibleHandles;

@property (nonatomic, readwrite , weak  ) id                    <LFloatingViewDelegate>delegate;


- (instancetype)initWithFrame:(CGRect)frame limits:(CGRect)limits;

@end



@protocol LFloatingViewDelegate <NSObject>
@optional

- (void)floatingViewWillMove:(LFloatingView *)floatingView;
- (void)floatingViewDidMove:(LFloatingView *)floatingView;

- (void)floatingViewWillResize:(LFloatingView *)floatingView;
- (void)floatingViewDidResize:(LFloatingView *)floatingView;

@end
