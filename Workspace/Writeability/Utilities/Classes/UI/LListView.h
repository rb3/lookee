//
//  LListView.h
//  Writeability
//
//  Created by Ryan Blonna on 19/11/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <UIKit/UIKit.h>


@protocol LListViewDelegate;


@interface LListView : UIView

@property (nonatomic, readwrite , copy  ) NSDictionary *headerAttributes;
@property (nonatomic, readwrite , copy  ) NSDictionary *footerAttributes;

@property (nonatomic, readwrite , weak  ) id <LListViewDelegate>delegate;


- (instancetype)initWithFrame:(CGRect)frame;

- (void)batchUpdateListWithVerb:(void(^)(void))verb;

- (void)reload;

- (void)reloadItemAtIndex:(NSUInteger)index animated:(BOOL)animated;

- (void)insertItemAtIndex:(NSUInteger)index animated:(BOOL)animated;

- (void)deleteItemAtIndex:(NSUInteger)index animated:(BOOL)animated;

- (void)showLeftHeaderOptionsAnimated:(BOOL)animated;
- (void)showRightHeaderOptionsAnimated:(BOOL)animated;
- (void)hideHeaderOptionsAnimated:(BOOL)animated;

@end


@protocol LListViewDelegate <NSObject>
@required

- (NSUInteger)itemCountForListView:(LListView *)listView;

- (NSDictionary *)listView:(LListView *)listView attributesForItemAtIndex:(NSUInteger)index;

@optional

- (NSUInteger)listView:(LListView *)listView optionCountForItemAtIndex:(NSUInteger)index;

- (NSDictionary *)listView:(LListView *)listView attributesForOptionAtIndexPath:(NSIndexPath *)indexPath;

@optional

- (void)listView:(LListView *)listView didScrollToItemsInRange:(NSRange)range;

- (void)listView:(LListView *)listView didSelectOptionAtIndexPath:(NSIndexPath *)indexPath;

- (void)listView:(LListView *)listView didSelectLeftHeaderOptionAtIndex:(NSUInteger)index;

- (void)listView:(LListView *)listView didSelectRightHeaderOptionAtIndex:(NSUInteger)index;

@end


@interface NSIndexPath (LListView)

@property (nonatomic, readonly, assign) NSUInteger itemIndex;

@property (nonatomic, readonly, assign) NSUInteger optionIndex;

@end
