//
//  LTextBox.m
//  Writeability
//
//  Created by Ryan on 12/4/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LTextBox.h"

#import "LTextView.h"

#import "LTextPicker.h"

#import <Utilities/LMacros.h>
#import <Utilities/LDelegateProxy.h>





//*********************************************************************************************************************//
#pragma mark -
#pragma mark Private Classes
#pragma mark -
//*********************************************************************************************************************//




@proxyclass(LFloatingViewProxy, LFloatingViewDelegate);





//*********************************************************************************************************************//
#pragma mark -
#pragma mark LTextBox
#pragma mark -
// Derived from the GrowingTextViewExample Project @ https://github.com/HansPinckaers/GrowingTextView
//*********************************************************************************************************************//





@interface LTextBox () <LFloatingViewDelegate, LProxyDelegate, UITextViewDelegate, LTextPickerDelegate>

@property (nonatomic, readwrite , weak  ) id                    <LFloatingViewDelegate>receiver;
@property (nonatomic, readonly  , strong) LFloatingViewProxy    *proxy;

@property (nonatomic, readwrite , weak  ) LTextPicker           *picker;
@property (nonatomic, readwrite , weak  ) LTextView             *textView;

@property (nonatomic, readwrite , assign) CGFloat               minimumWidth;

@end

@implementation LTextBox

#pragma mark - Instance Variables
//*********************************************************************************************************************//
{
@private
    struct {
        BOOL textBoxWillChangeHeight:1;
        BOOL textBoxDidChangeHeight :1;
    } _delegateRespondsTo;
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)initWithFrame:(CGRect)frame limits:(CGRect)limits attributes:(NSDictionary *)typingAttributes
{
    if ((self = [super initWithFrame:frame limits:limits])) {
        LTextPicker *picker = [LTextPicker new];
        [picker setDelegate:self];

        LTextView  *textView                    = [LTextView.alloc initWithFrame:self.bounds];
        textView.autoresizingMask               = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
        textView.contentMode                    = UIViewContentModeRedraw;
        textView.allowsEditingTextAttributes    = YES;
        textView.scrollEnabled                  = NO;
        textView.clipsToBounds                  = NO;
        textView.backgroundColor                = [UIColor clearColor];
        textView.autocorrectionType             = UITextAutocorrectionTypeNo;
        textView.delegate                       = self;
        textView.contentInset                   = UIEdgeInsetsZero;
        textView.inputAccessoryView             = picker;

        [self.content addSubview:textView];

        [self setPicker:picker];
        [self setTextView:textView];
        [self setEditingAttributes:typingAttributes];

        textView.font       = [self editingAttributes][NSFontAttributeName];
        textView.textColor  = [self editingAttributes][NSForegroundColorAttributeName];

        [self.content setClipsToBounds:YES];

        [self setVisibleHandles:(kLFloatingViewHandleMiddleLeft|
                                 kLFloatingViewHandleMiddleRight)];

        _proxy = [LFloatingViewProxy.alloc initWithDelegate:self];
        [super setDelegate:_proxy];

        [self calculateLimits];
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame limits:(CGRect)limits
{
    return (self = [self initWithFrame:frame limits:limits attributes:nil]);
}

- (void)dealloc
{
    DBGMessage(@"deallocated %@", self);
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];

    [self calculateLimits];
}

- (BOOL)isFirstResponder
{
    return [_textView isFirstResponder];
}

- (BOOL)becomeFirstResponder
{
    return [_textView becomeFirstResponder];
}

- (BOOL)resignFirstResponder
{
    return [_textView resignFirstResponder];
}

- (CGSize)sizeThatFits:(CGSize)size
{
    if (![_textView hasText]) {
        size.height = _minimumHeight;
    }

    return size;
}

- (void)setLimits:(CGRect)limits
{
    [super setLimits:limits];

    [self calculateLimits];
}


#pragma mark - Properties
//*********************************************************************************************************************//

- (void)setDelegate:(id <LFloatingViewDelegate>)delegate
{
    {_receiver = delegate;}

    _delegateRespondsTo.textBoxWillChangeHeight = [delegate respondsToSelector:@selector(textBox:willChangeHeight:)];
    _delegateRespondsTo.textBoxDidChangeHeight  = [delegate respondsToSelector:@selector(textBox:didChangeHeight:)];

    super.delegate = nil;
    super.delegate = _proxy;
}

- (id <LFloatingViewDelegate>)delegate
{
    return _receiver;
}

- (void)setPlaceholderText:(NSString *)placeholderText
{
    [_textView setPlaceholderText:placeholderText];
}

- (NSString *)placeholderText
{
    return [_textView placeholderText];
}

- (void)setPlaceholderColor:(UIColor *)placeholderColor
{
    [_textView setPlaceholderColor:placeholderColor];
}

- (UIColor *)placeholderColor
{
    return [_textView placeholderColor];
}

- (void)setEditingAttributes:(NSDictionary *)typingAttributes
{
    NSMutableDictionary *attributes = ([typingAttributes mutableCopy]?:
                                       [NSMutableDictionary new]);

    if (!attributes[NSFontAttributeName]) {
        attributes[NSFontAttributeName]
        =
        [self editingAttributes]
        [NSFontAttributeName]?:
        [UIFont systemFontOfSize:12.];
    }

    if (!attributes[NSForegroundColorAttributeName]) {
        attributes[NSForegroundColorAttributeName]
        =
        [self editingAttributes]
        [NSForegroundColorAttributeName]?:
        [UIColor blackColor];
    }

    UIFont  *font       = attributes[NSFontAttributeName];
    UIColor *textColor  = attributes[NSForegroundColorAttributeName];

    self.textView.typingAttributes  = attributes;

    self.picker.color               = textColor;
    self.picker.size                = @([font pointSize]);
    self.picker.fontName            = [font familyName];

    {_editingAttributes = attributes;}
}

- (void)setAttributedText:(NSAttributedString *)attributedText
{
    if (![_textView.attributedText isEqualToAttributedString:attributedText]) {
        [_textView setAttributedText:attributedText];

        [self adjustHeight];
        [self.delegate textBoxDidChangeText:self];
    }
}

- (NSAttributedString *)attributedText
{
    return [_textView attributedText];
}

- (CGRect)textRect
{
    CGSize dimensions       = [self measureDimensions];

	if (![_textView hasText] || isless(dimensions.height, _minimumHeight)) {
        dimensions.height   = _minimumHeight;
    } else if (!IsZero(_maximumHeight) && isgreater(dimensions.height, _maximumHeight)) {
        dimensions.height   = _maximumHeight;
    }

    CGRect rect         = [self frame];
    rect.size.width     = dimensions.width;
    rect.size.height    = dimensions.height;

    return rect;
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (BOOL)hasText
{
    return [_textView hasText];
}


#pragma mark - UITextViewDelegate
//*********************************************************************************************************************//

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [self.delegate textBoxDidBeginEditing:self];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [self.delegate textBoxDidEndEditing:self];
}

- (void)textViewDidChange:(UITextView *)textView
{
    [self adjustHeight];
    [self.delegate textBoxDidChangeText:self];
}


#pragma mark - LFloatingViewDelegate
//*********************************************************************************************************************//

- (void)floatingViewDidResize:(LFloatingView *)floatingView
{
    [self adjustHeight];
    [self.delegate textBoxDidChangeTextRect:self];
}

- (void)floatingViewDidMove:(LFloatingView *)floatingView
{
    [self.delegate textBoxDidChangeTextRect:self];
}


#pragma mark - LTextPickerDelegate
//*********************************************************************************************************************//

- (void)textPicker:(LTextPicker *)textPicker didChangeColor:(UIColor *)color
{
    self.editingAttributes = (@{ NSForegroundColorAttributeName: color });

    NSRange range   = [self.textView selectedRange];

    if (range.length) {
        NSMutableAttributedString *edit = [self.textView.attributedText mutableCopy];
        [edit setAttributes:self.editingAttributes range:range];
        [self setAttributedText:edit];
        [self.textView setSelectedRange:range];
    }
}

- (void)textPicker:(LTextPicker *)textPicker didChangeSize:(NSNumber *)size
{
    UIFont  *font   = [self.editingAttributes[NSFontAttributeName] fontWithSize:size.floatValue];

    self.editingAttributes = (@{ NSFontAttributeName: font });

    NSRange range   = [self.textView selectedRange];

    if (range.length) {
        NSMutableAttributedString *edit = [self.textView.attributedText mutableCopy];
        [edit setAttributes:self.editingAttributes range:range];
        [self setAttributedText:edit];
        [self.textView setSelectedRange:range];
    }
}

- (void)textPicker:(LTextPicker *)textPicker didChangeFontName:(NSString *)fontName
{
    CGFloat fontSize    = [self.editingAttributes[NSFontAttributeName] pointSize];
    UIFont  *font       = [UIFont fontWithName:fontName size:fontSize];

    self.editingAttributes = (@{ NSFontAttributeName: font });

    NSRange range       = [self.textView selectedRange];

    if (range.length) {
        NSMutableAttributedString *edit = [self.textView.attributedText mutableCopy];
        [edit setAttributes:self.editingAttributes range:range];
        [self setAttributedText:edit];
        [self.textView setSelectedRange:range];
    }
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (CGSize)measureDimensions
{
    CGRect  frame   = [_textView frame];

    CGSize  dimensions;

    if (CGRectIsEmpty(frame)) {
        [_textView setHidden:YES];
        [_textView setDelegate:nil];
        [_textView setAttributedText:
         [[NSAttributedString alloc]
          initWithString:@"|W|"
          attributes    :self.editingAttributes]];

        [_textView sizeToFit];

        if ([self respondsToSelector:@selector(snapshotViewAfterScreenUpdates:)]) {
            dimensions = [_textView sizeThatFits:[_textView frame].size];

            if (!IsZero(dimensions.height)) {
                dimensions.height += 1.;
            }
        } else {
            dimensions = [_textView contentSize];

            if (!IsZero(dimensions.height)) {
                dimensions.height += 8.;
            }
        }

        [_textView setAttributedText:
         [[NSAttributedString alloc]
          initWithString:@""
          attributes    :self.editingAttributes]];

        [_textView setFrame:frame];

        [_textView setDelegate:self];
        [_textView setHidden:NO];
    } else {
        if ([self respondsToSelector:@selector(snapshotViewAfterScreenUpdates:)]) {
            dimensions = [_textView sizeThatFits:frame.size];

            if (!IsZero(dimensions.height)) {
                dimensions.height += 1.;
            }
        } else {
            dimensions = [_textView contentSize];

            if (!IsZero(dimensions.height)) {
                dimensions.height += 8.;
            }
        }
    }

    dimensions.width  = ceilf(dimensions.width);
    dimensions.height = ceilf(dimensions.height);

    return dimensions;
}

- (CGFloat)measureMaximumHeight
{
    CGFloat height;

    if (!CGRectIsEmpty([self limits])) {
        height = (self.limits.size.height + self.limits.origin.y - self.frame.origin.y);
    } else {
        height = [UIScreen mainScreen].bounds.size.height;
    }

    return ceilf(height);
}

- (void)calculateLimits
{
    if (IsZero(_minimumHeight) || IsZero(_minimumWidth)) {
        CGSize dimensions   = [self measureDimensions];

        if (IsZero(_minimumWidth)) {
            _minimumWidth   = dimensions.width;
        }

        if (IsZero(_minimumHeight)) {
            _minimumHeight  = dimensions.height;
        }
    }

    if (IsZero(_maximumHeight)) {
        _maximumHeight      = [self measureMaximumHeight];
    }
}

- (void)resetScrollPosition
{
    CGRect  rect    = [_textView caretRectForPosition:[_textView.selectedTextRange end]];
    CGFloat caretY  = fmaxf(rect.origin.y - _textView.frame.size.height + rect.size.height +8., 0.);

    if (isless([_textView contentOffset].y, caretY) && !CGRectIsNull(rect)) {
        [_textView setContentOffset:((CGPoint){0, caretY})];
    }
}

- (void)resizeTextView:(CGSize)dimensions
{
    if (_delegateRespondsTo.textBoxWillChangeHeight) {
        [self.delegate textBox:self willChangeHeight:dimensions.height];
    }

    CGRect bounds   = [self frame];
    bounds.size     = dimensions;

    if (!CGRectEqualToRect(self.frame, bounds)) {
        [self setFrame:bounds];
    }
}

- (void)adjustHeight
{
	CGSize dimensions = [self textRect].size;

    if (isgreater(CGRectGetWidth(self.frame), dimensions.width)) {
        dimensions.width = self.frame.size.width;
    }

	if (!CGSizeEqualToSize([_textView frame].size, dimensions)) {
        [self resizeTextView:dimensions];

        if (_delegateRespondsTo.textBoxDidChangeHeight) {
            [self.delegate textBox:self didChangeHeight:dimensions.height];
        }
	}

    if ([self respondsToSelector:@selector(snapshotViewAfterScreenUpdates:)]) {
        [self performSelector:@selector(resetScrollPosition) withObject:nil afterDelay:.1];
    }
}

@end