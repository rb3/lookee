//
//  LFloatingView.m
//  Writeability
//
//  Created by Ryan on 11/26/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LFloatingView.h"

#import <Utilities/LGeometry.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Private Classes
#pragma mark -
//*********************************************************************************************************************//





@interface LFBorderedView : UIView

@property (nonatomic, readwrite , assign) LFloatingViewHandle   visibleHandles;

@property (nonatomic, readonly  , assign) CGFloat               borderSize;


- (instancetype)initWithFrame:(CGRect)frame borderSize:(CGFloat)borderSize;

@end





//*********************************************************************************************************************//
#pragma mark -
#pragma mark LFloatingView
#pragma mark -
//*********************************************************************************************************************//





@interface LFloatingView ()

@property (nonatomic, readonly, weak) LFBorderedView *border;

@end

@implementation LFloatingView
{
    CGPoint __position;
    CGSize  __dimensions;

    struct {
        BOOL floatingViewWillMove   :1;
        BOOL floatingViewDidMove    :1;
        BOOL floatingViewWillResize :1;
        BOOL floatingViewDidResize  :1;
    } _delegateRespondsTo;
}

#pragma mark - Static Objects
//*********************************************************************************************************************//

static CGFloat kLFloatingViewInteractiveBorderSize = 24.;


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)init __ILLEGALMETHOD__;
- (instancetype)initWithFrame:(CGRect)frame __ILLEGALMETHOD__;

- (instancetype)initWithFrame:(CGRect)frame limits:(CGRect)limits
{
    if ((self = [super initWithFrame:CGRectClampToRect(frame, limits)])) {
        self.clipsToBounds  = NO;

        LFBorderedView *border      = [[LFBorderedView alloc]
                                       initWithFrame:CGRectInset([self bounds], -kLFloatingViewInteractiveBorderSize, 0.)
                                       borderSize   :kLFloatingViewInteractiveBorderSize];

        border.autoresizingMask     = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);

        UIView *content             = [[UIView alloc] initWithFrame:[self bounds]];
        content.autoresizingMask    = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);

        {
            UIPanGestureRecognizer *recognizer = [UIPanGestureRecognizer new];
            [recognizer addTarget:self action:@selector(transformContent:)];
            [border addGestureRecognizer:recognizer];
        }

        {
            UIPanGestureRecognizer *recognizer = [UIPanGestureRecognizer new];
            [recognizer addTarget:self action:@selector(translateContent:)];
            [content addGestureRecognizer:recognizer];
        }

        [self addSubview:border];
        [self addSubview:content];

        _limits     = limits;
        _border     = border;
        _content    = content;
    }

    return self;
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (void)setFrame:(CGRect)frame
{
    if (!CGRectIsEmpty(_limits)) {
        frame = CGRectIntersection(frame, [self limits]);
    }

    [super setFrame:frame];
}

- (void)setDelegate:(id <LFloatingViewDelegate>)delegate
{
    {_delegate = delegate;}

    _delegateRespondsTo.floatingViewWillMove    = [delegate respondsToSelector:@selector(floatingViewWillMove:)];
    _delegateRespondsTo.floatingViewDidMove     = [delegate respondsToSelector:@selector(floatingViewDidMove:)];
    _delegateRespondsTo.floatingViewWillResize  = [delegate respondsToSelector:@selector(floatingViewWillResize:)];
    _delegateRespondsTo.floatingViewDidResize   = [delegate respondsToSelector:@selector(floatingViewDidResize:)];
}


#pragma mark - Properties
//*********************************************************************************************************************//

- (void)setVisibleHandles:(LFloatingViewHandle)visibleHandles
{
    [_border setVisibleHandles:visibleHandles];
}

- (LFloatingViewHandle)visibleHandles
{
    return [_border visibleHandles];
}


#pragma mark - UIGestureRecognizer Callbacks
//*********************************************************************************************************************//

- (void)translateContent:(UIPanGestureRecognizer *)recognizer
{
    CGPoint translation = [recognizer translationInView:self.superview];
    CGRect  frame       = [self frame];

    switch ([recognizer state]) {
        case UIGestureRecognizerStateBegan      : {
            if (_delegateRespondsTo.floatingViewWillMove) {
                [_delegate floatingViewWillMove:self];
            }

            __position      = frame.origin;
        } break;
        case UIGestureRecognizerStateChanged    : {
            frame.origin    = ((CGPoint) {
                .x = __position.x + translation.x,
                .y = __position.y + translation.y
            });

            frame = CGRectClampToRect(frame, [self limits]);

            [self setFrame:CGRectIntersection(frame, [self limits])];
        } break;
        case UIGestureRecognizerStateCancelled:
        case UIGestureRecognizerStateEnded: {
            if (_delegateRespondsTo.floatingViewDidMove) {
                [_delegate floatingViewDidMove:self];
            }
        } break;
        default: {
            
        } break;
    }
}

- (void)transformContent:(UIPanGestureRecognizer *)recognizer
{
    CGPoint translation = [recognizer translationInView:self.border];
    CGPoint location    = [recognizer locationInView:self.border];

    CGRect  frame       = [self frame];
    CGPoint origin      = frame.origin;

    switch ([recognizer state]) {
        case UIGestureRecognizerStateBegan      : {
            if (_delegateRespondsTo.floatingViewWillResize) {
                [_delegate floatingViewWillResize:self];
            }

            __position   = frame.origin;
            __dimensions = frame.size;
        } break;
        case UIGestureRecognizerStateChanged    : {
            CGPoint position    = ((CGPoint) {
                .x = __position.x + translation.x,
                .y = __position.y + translation.y
            });

            CGSize  dimensions  = ((CGSize) {
                .width  = __dimensions.width  + translation.x,
                .height = __dimensions.height + translation.y
            });

            if (isless(location.x, CGRectGetMidX(self.border.bounds))) {
                frame.origin.x      = +position.x;
                frame.size.width   += -position.x + origin.x;
            } else {
                frame.size.width    = +dimensions.width;
            }

            [self setFrame:frame];
        } break;
        case UIGestureRecognizerStateEnded: {
            if (_delegateRespondsTo.floatingViewDidResize) {
                [_delegate floatingViewDidResize:self];
            }
        } break;
        default: {

        } break;
    }
}

@end





//*********************************************************************************************************************//
#pragma mark -
#pragma mark LFBorderedView
#pragma mark -
// Derived from the SPUserResizableView Project @ https://github.com/spoletto/SPUserResizableView
//*********************************************************************************************************************//





@implementation LFBorderedView

#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)initWithFrame:(CGRect)frame __ILLEGALMETHOD__;

- (instancetype)initWithFrame:(CGRect)frame borderSize:(CGFloat)borderSize
{
    if ((self = [super initWithFrame:frame])) {
        {_borderSize = borderSize;}

        [self.layer setShouldRasterize:YES];

        [self setContentMode:UIViewContentModeRedraw];
        [self setBackgroundColor:[UIColor clearColor]];
    }

    return self;
}

#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);

    CGContextSetLineWidth(context, 1.);
    CGContextSetStrokeColorWithColor(context, [UIColor blueColor].CGColor);
    CGContextAddRect(context, CGRectInset([self bounds], _borderSize /2., _borderSize /2.));
    CGContextStrokePath(context);

    CGSize size         = [self bounds].size;

    CGRect upperLeft    = ((CGRect) {
        {0},
        {_borderSize, _borderSize}
    });

    CGRect upperRight   = ((CGRect) {
        {(size.width - _borderSize), 0.},
        {_borderSize, _borderSize}
    });

    CGRect lowerRight   = ((CGRect) {
        {(size.width  - _borderSize),
         (size.height - _borderSize)},
        {_borderSize, _borderSize}
    });

    CGRect lowerLeft    = ((CGRect) {
        {0., (size.height - _borderSize)},
        {_borderSize, _borderSize}
    });

    CGRect upperMiddle  = ((CGRect) {
        {(size.width - _borderSize) /2., 0.},
        {_borderSize, _borderSize}
    });

    CGRect lowerMiddle  = ((CGRect) {
        {(size.width  - _borderSize) /2.,
         (size.height - _borderSize)},
        {_borderSize, _borderSize}
    });

    CGRect middleLeft   = ((CGRect) {
        {0., (size.height - _borderSize) /2.},
        {_borderSize, _borderSize}
    });

    CGRect middleRight  = ((CGRect) {
        {(size.width  - _borderSize),
         (size.height - _borderSize) /2.},
        {_borderSize, _borderSize}
    });

    CGFloat colors [] = {
        .4, .8, 1., 1.,
        0., 0., 1., 1.
    };

    CGColorSpaceRef baseSpace   = CGColorSpaceCreateDeviceRGB();
    CGGradientRef   gradient    = CGGradientCreateWithColorComponents(baseSpace, colors, NULL, 2);

    CGColorSpaceRelease(baseSpace);

    CGContextSetLineWidth(context, 1);
    CGContextSetShadow(context, ((CGSize){.5, .5}), 1.);
    CGContextSetStrokeColorWithColor(context, [UIColor whiteColor].CGColor);

    CGRect rects[8] = {
        upperLeft   , upperRight    ,
        lowerRight  , lowerLeft     ,
        upperMiddle , lowerMiddle   ,
        middleLeft  , middleRight
    };

    for (NSInteger index = 0; index < 8; ++ index) {
        if (IsBitAsserted(_visibleHandles, index)) {
            CGRect  rect        = rects[index];

            CGContextSaveGState(context);
            CGContextAddEllipseInRect(context, rect);
            CGContextClip(context);

            CGPoint startPoint  = CGPointMake(CGRectGetMidX(rect), CGRectGetMinY(rect));
            CGPoint endPoint    = CGPointMake(CGRectGetMidX(rect), CGRectGetMaxY(rect));

            CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);
            CGContextRestoreGState(context);
            CGContextStrokeEllipseInRect(context, CGRectInset(rect, 1., 1.));
        }
    }

    CGGradientRelease(gradient);
    CGContextRestoreGState(context);
}

@end
