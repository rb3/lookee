//
//  LPassthruView.m
//  Writeability
//
//  Created by Ryan on 11/13/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LContainerView.h"



//*********************************************************************************************************************//
#pragma mark -
#pragma mark LPassthruView
#pragma mark -
//*********************************************************************************************************************//



@implementation LContainerView

#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    if (![self clipsToBounds]) {
        for (UIView *subview in [self.subviews reverseObjectEnumerator]) {
            UIView *hit = [subview hitTest:[self convertPoint:point toView:subview] withEvent:event];

            if (hit) {
                return hit;
            }
        }
    }

    UIView *hit = [super hitTest:point withEvent:event];

    return (hit == self? nil: hit);
}

@end
