//
//  LToolbarButton.m
//  Writeability
//
//  Created by Jelo Agnasin on 10/10/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LButton.h"

#import "LConstants.h"




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LToolbarButton
#pragma mark -
//*********************************************************************************************************************//




@implementation LButton


#pragma mark - Static Objects
//*********************************************************************************************************************//

static const CGFloat kLToolbarButtonSize = 44.;


#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (LButton *)button
{
    return [self new];
}

+ (LButton *)buttonWithFrame:(CGRect)frame
{
    return [[LButton alloc] initWithFrame:frame];
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)init
{
    CGRect frame = ((CGRect) {
        {0},
        {kLToolbarButtonSize,
            kLToolbarButtonSize}
    });

    if ((self = [self initWithFrame:frame])) {

    }

    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) {
        [self setAdjustsImageWhenDisabled:NO];
        [self setAdjustsImageWhenHighlighted:NO];
    }

    return self;
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (BOOL)canBecomeFirstResponder
{
    return YES;
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (void)setNormalImage:(UIImage *)image
{
    [self setImage:image forState:UIControlStateNormal];
}

- (void)setSelectedImage:(UIImage *)image
{
    [self setImage:image forState:UIControlStateHighlighted];
    [self setImage:image forState:UIControlStateSelected];
    [self setImage:image forState:(UIControlStateHighlighted | UIControlStateSelected)];
}

- (void)setBackgroundImage:(UIImage *)image
{
    [self setBackgroundImage:[UIImage imageNamed:LC_SELECTED_BACKGROUND_IMAGE] forState:UIControlStateSelected];
    [self setBackgroundImage:[UIImage imageNamed:LC_SELECTED_BACKGROUND_IMAGE] forState:UIControlStateHighlighted];
    [self setBackgroundImage:[UIImage imageNamed:LC_SELECTED_BACKGROUND_IMAGE] forState:(UIControlStateSelected | UIControlStateHighlighted)];
}

@end
