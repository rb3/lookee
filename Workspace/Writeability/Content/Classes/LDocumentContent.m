//
//  LDocumentContent.m
//  Writeability
//
//  Created by Ryan on 3/19/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LDocumentContent_Private.h"

#import "LConstants.h"

#import "LDocument.h"

#import "LPageLoader.h"

#import "LContainerView.h"

#import "LButton.h"

#import "LPopupView.h"

#import "LPopoverController.h"

#import "LAnnotationPicker.h"

#import "LSlider.h"

#import "LPage+UndoManager.h"

#import <Annotation/LAnnotator.h>
#import <Annotation/LATool.h>

#import <Utilities/LMacros.h>

#import <Utilities/NSObject+Utilities.h>
#import <Utilities/UIScrollView+Scroll.h>

#import <MessageUI/MessageUI.h>

// TODO: Make LBottomToolbar a subclass/category of LToolbar




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LDocumentContent
#pragma mark -
//*********************************************************************************************************************//




@interface LDocumentContent () <LDocumentDelegate, LSliderDelegate, MFMailComposeViewControllerDelegate>

@property (nonatomic, readonly  , weak  ) UIView                            *master;

@property (nonatomic, readwrite , strong) UIView                            *titleView;
@property (nonatomic, readwrite , strong) UIToolbar                         *toolbar;
@property (nonatomic, readwrite , weak  ) LSlider                           *pageSlider;

@property (nonatomic, readonly  , strong) UIPanGestureRecognizer            *pageTurnRecognizer;
@property (nonatomic, readonly  , strong) UITapGestureRecognizer            *zoomResetRecognizer;

@property (nonatomic, readwrite , assign) BOOL                              strokeContentEnabled;
@property (nonatomic, readwrite , assign) BOOL                              textContentEnabled;

@property (nonatomic, readonly  , strong) LAnnotator                        *currentAnnotator;
@property (nonatomic, readonly  , strong) LPage                             *currentPage;

@property (nonatomic, readwrite , weak  ) LButton                           *defaultButton;

@property (nonatomic, readwrite , strong) LATool                   *annotationTool;

@property (nonatomic, readonly  , strong) LATool                   *penTool;
@property (nonatomic, readonly  , strong) LATool                   *highlighterTool;
@property (nonatomic, readonly  , strong) LATool                   *eraserTool;

@property (nonatomic, readonly  , strong) NSMutableArray                    *hiddenLeftItems;
@property (nonatomic, readonly  , strong) NSMutableArray                    *hiddenRightItems;

@property (nonatomic, readwrite , assign) BOOL                              panDirection;

@property (nonatomic, readwrite , assign, getter = isLoaded)                BOOL loaded;
@property (nonatomic, readwrite , assign, getter = isRotating)              BOOL rotating;
@property (nonatomic, readwrite , assign, getter = isAnimating)             BOOL animating;
@property (nonatomic, readwrite , assign, getter = isDetailShown)           BOOL showDetail;

@end

@interface LDocumentContent (Setup)

- (void)setupNavigationBar;
- (void)setupToolbar;
- (void)setupContent;

@end

@implementation LDocumentContent
{
    struct {
        BOOL documentContentDidLoad                     :1;
        BOOL documentContentShouldAllowUserPageTurn     :1;
        BOOL documentContentShouldShowPageAtLocation    :1;
        BOOL documentContentDidBeginAnnotationAtPoint   :1;
        BOOL documentContentDidMoveAnnotationToPoint    :1;
        BOOL documentContentDidEndAnnotationAtPoint     :1;
        BOOL documentContentDidUndo                     :1;
        BOOL documentContentDidRedo                     :1;
    } _delegateRespondsTo;
}

#pragma mark - Static Objects
//*********************************************************************************************************************//

static const float kLDCMasterDetailHeightRatio  = .7;
static const float kLDCDetailBorderWidthRatio   = .875;
static const float kLDCMagnifyToolbarAlphaValue = .8;
static const float kLDCDetailBorderAlphaValue   = .1;


#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (BOOL)automaticallyNotifiesObserversForKey:(NSString *)key
{
    return (![key isEqualToString:@"zoomRect"] &&
            ![key isEqualToString:@"pageNumber"]);
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)init __ILLEGALMETHOD__;

- (instancetype)initWithDocument:(LDocument *)document
{
    DBGParameterAssert(document != nil);

    if ((self = [super init])) {
        _interactionEnabled = YES;

        _document           = document;
        _pageNumber         = 1;

        _penTool            = [LATool newPen];
        _eraserTool         = [LATool newEraser];
        _highlighterTool    = [LATool newHighlighter];

        _hiddenLeftItems    = [NSMutableArray new];
        _hiddenRightItems   = [NSMutableArray new];

        [_document setDelegate:self];
    }

    return self;
}

- (void)dealloc
{
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];

    DBGMessage(@"deallocated %@", self);
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (void)viewDidLoad
{
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];

    [super viewDidLoad];

    UIView *view = [self view];

    [view setClipsToBounds:YES];
    [view setBackgroundColor:[UIColor LBorderColor]];

    UIView *master = [UIView new];
    UIView *detail = [UIView new];

    [master setClipsToBounds:YES];
    [detail setClipsToBounds:YES];

    [master setBackgroundColor:self.view.backgroundColor];
    [detail setBackgroundColor:self.view.backgroundColor];

    [view addSubview:master];
    [view addSubview:detail];

    [self setupNavigationBar];
    [self setupToolbar];

    {_master = master;}
    {_detail = detail;}
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    if (![self isLoaded]) {
        [self setLoaded:YES];

        [self setShowDetail:NO];
        [self setupContent];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [self.pageSlider showNotificationsInView:self.view];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];

    if ([self isRotating]) {
        [self setShowDetail:self.showDetail];
        [self.pageContent scaleContentToFitSize:[self view].bounds.size];
    }
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self setRotating:YES];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self setRotating:NO];
}


#pragma mark - Properties
//*********************************************************************************************************************//

@dynamic pageCount;

- (NSUInteger)pageCount
{
    return [self.document pageCount];
}

@synthesize pageNumber = _pageNumber;

- (void)setPageNumber:(NSUInteger)pageNumber
{
    [self setPageNumber:pageNumber animated:NO];
}

- (void)setPageNumber:(NSUInteger)pageNumber animated:(BOOL)animated
{
    [self setPageNumber:pageNumber animated:animated completion:NULL];
}

- (void)setPageNumber:(NSUInteger)pageNumber animated:(BOOL)animated completion:(void(^)(void))completion
{
    [self willChangeValueForKey:@"pageNumber"];

    [self
     showContentAtLocation  :pageNumber
     animated               :animated
     completion             :completion];

    {_pageNumber = pageNumber;}

    [self didChangeValueForKey:@"pageNumber"];
}

@synthesize zoomRect = _zoomRect;

- (void)setZoomRect:(CGRect)zoomRect
{
    [self setZoomRect:zoomRect animated:NO];
}

- (void)setZoomRect:(CGRect)zoomRect animated:(BOOL)animated
{
    [self willChangeValueForKey:@"zoomRect"];

    if (!CGRectEqualToRect(_zoomRect, zoomRect)) {
        {_zoomRect = zoomRect;}

        [self.pageContent zoomToRect:zoomRect animated:animated];
    }

    [self didChangeValueForKey:@"zoomRect"];
}

@dynamic textEditingAttributes;

- (NSDictionary *)textEditingAttributes
{
    return self.pageContent.editingAttributes;
}

@synthesize leftUtilityItems = _leftUtilityItems;

- (void)setLeftUtilityItems:(NSArray *)leftUtilityItems
{
    {_leftUtilityItems = [leftUtilityItems copy];}

    if (![self.navigationItem leftBarButtonItems]) {
        [self.navigationItem setLeftBarButtonItems:@[]];
    }

    [self.navigationItem setLeftBarButtonItems:self.navigationItem.leftBarButtonItems.Plus(leftUtilityItems) animated:YES];
}

@synthesize rightUtilityItems = _rightUtilityItems;

- (void)setRightUtilityItems:(NSArray *)rightUtilityItems
{
    {_rightUtilityItems = [rightUtilityItems copy];}

    if (![self.navigationItem rightBarButtonItems]) {
        [self.navigationItem setRightBarButtonItems:@[]];
    }

    [self.navigationItem setRightBarButtonItems:rightUtilityItems.Plus(self.navigationItem.rightBarButtonItems) animated:YES];
}

@synthesize leftToolbarItems = _leftToolbarItems;

- (void)setLeftToolbarItems:(NSArray *)leftToolbarItems
{
    {_leftToolbarItems = [leftToolbarItems copy];}

    [self setupToolbar];
}

@synthesize rightToolbarItems = _rightToolbarItems;

- (void)setRightToolbarItems:(NSArray *)rightToolbarItems
{
    {_rightToolbarItems = [rightToolbarItems copy];}

    [self setupToolbar];
}

@synthesize strokeContentEnabled = _strokeContentEnabled;

- (void)setStrokeContentEnabled:(BOOL)strokeContentEnabled
{
    {_strokeContentEnabled = strokeContentEnabled;}

    [self.pageContent setStrokeContentEnabled:strokeContentEnabled];
}

@synthesize textContentEnabled = _textContentEnabled;

- (void)setTextContentEnabled:(BOOL)textContentEnabled
{
    {_textContentEnabled = textContentEnabled;}

    [self.pageContent setTextContentEnabled:textContentEnabled];
}

@dynamic currentAnnotator;

- (LAnnotator *)currentAnnotator
{
    return [self.currentPage currentAnnotator];
}

@dynamic currentPage;

- (LPage *)currentPage
{
    return self.document.pages[self.pageNumber-1];
}

@dynamic annotationTool;

- (LATool *)annotationTool
{
    return [self.currentAnnotator annotationTool];
}

- (void)setAnnotationTool:(LATool *)annotationTool
{
    [self.currentAnnotator setAnnotationTool:annotationTool];
}

@synthesize delegate = _delegate;

- (void)setDelegate:(id)delegate
{
    {_delegate = delegate;}

    _delegateRespondsTo.documentContentDidLoad
    = [delegate respondsToSelector:@selector(documentContentDidLoad:)];
    _delegateRespondsTo.documentContentShouldAllowUserPageTurn
    = [delegate respondsToSelector:@selector(documentContentShouldAllowUserPageTurn:)];
    _delegateRespondsTo.documentContentShouldShowPageAtLocation
    = [delegate respondsToSelector:@selector(documentContent:shouldShowPageAtLocation:)];
    _delegateRespondsTo.documentContentDidBeginAnnotationAtPoint
    = [delegate respondsToSelector:@selector(documentContent:didBeginAnnotationAtPoint:)];
    _delegateRespondsTo.documentContentDidMoveAnnotationToPoint
    = [delegate respondsToSelector:@selector(documentContent:didMoveAnnotationToPoint:)];
    _delegateRespondsTo.documentContentDidEndAnnotationAtPoint
    = [delegate respondsToSelector:@selector(documentContent:didEndAnnotationAtPoint:)];
    _delegateRespondsTo.documentContentDidUndo
    = [delegate respondsToSelector:@selector(documentContentDidUndo:)];
    _delegateRespondsTo.documentContentDidRedo
    = [delegate respondsToSelector:@selector(documentContentDidRedo:)];
}

@synthesize showDetail = _showDetail;

- (void)setShowDetail:(BOOL)showDetail
{
    [self setShowDetail:showDetail animated:NO];
}

- (void)setShowDetail:(BOOL)showDetail animated:(BOOL)animated
{
    [self setShowDetail:showDetail animated:animated invoke:nil finish:nil];
}

- (void)
setShowDetail   :(BOOL          )showDetail
animated        :(BOOL          )animated
invoke          :(void(^)(void) )invoke
finish          :(void(^)(void) )finish
{
    {_showDetail = showDetail;}

    UIView *view = [self view];
    CGRect frame = [view bounds];

    CGRect masterFrame;
    CGRect detailFrame;

    if (showDetail) {
        masterFrame = (CGRect)
        {
            .origin = {0},
            .size   = {
                .width  = ceilf(frame.size.width),
                .height = ceilf(frame.size.height *kLDCMasterDetailHeightRatio)
            }
        };
    } else {
        masterFrame = frame;
    }

    detailFrame = (CGRect)
    {
        .origin = {
            .x = (0.),
            .y = floorf(masterFrame.size.height)
        },
        .size   = {
            .width  = ceilf(masterFrame.size.width),
            .height = floorf(frame.size.height - frame.size.height *kLDCMasterDetailHeightRatio)
        }
    };

    if (!CGRectEqualToRect([_master frame], masterFrame)) {
        void(^animation)(void) = (^{
            [self.pageContent setZoomScale:[_pageContent minimumZoomScale]];
            [self.detail setFrame:detailFrame];

            if (invoke) {
                invoke();
            }
        });

        void(^completion)(BOOL) = (^(__UNUSEDARG(BOOL)){
            if (showDetail) {
                [self.master setFrame:masterFrame];
            } else {
                [self.pageTurnRecognizer setEnabled:YES];
            }

            if (finish) {
                finish();
            }
        });

        if (!showDetail) {
            [self.master setFrame:masterFrame];
        } else {
            [self.pageTurnRecognizer setEnabled:NO];
        }

        if (animated) {
            [UIView
             animateWithDuration:.3
             animations         :animation
             completion         :completion];
        } else {
            animation();
            completion(YES);
        }
    }
}

@synthesize interactionEnabled = _interactionEnabled;

- (void)setInteractionEnabled:(BOOL)interactionEnabled
{
    {_interactionEnabled = interactionEnabled;}

    [self.master setUserInteractionEnabled:interactionEnabled];
    [self.detail setUserInteractionEnabled:interactionEnabled];
    [self.pageSlider setUserInteractionEnabled:interactionEnabled];

    id leftItems    = [NSMutableArray new];
    id rightItems   = [NSMutableArray new];

    if (!interactionEnabled) {
        [self.hiddenLeftItems setArray:self.navigationItem.leftBarButtonItems];
        [self.hiddenRightItems setArray:self.navigationItem.rightBarButtonItems];

        [self.hiddenLeftItems removeObjectsInArray:self.leftUtilityItems];
        [self.hiddenRightItems removeObjectsInArray:self.rightUtilityItems];

        [self.navigationItem setTitleView:nil];
    } else {
        [leftItems setArray:self.hiddenLeftItems];
        [rightItems setArray:self.hiddenRightItems];

        [self.hiddenLeftItems removeAllObjects];
        [self.hiddenRightItems removeAllObjects];

        [self.navigationItem setTitleView:self.titleView];
    }

    NSUInteger itemCount = [self.rightUtilityItems count];

    NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange:
                            (( NSRange ) { 0, itemCount })];

    [leftItems addObjectsFromArray:self.leftUtilityItems];
    [rightItems insertObjects:self.rightUtilityItems atIndexes:indexSet];

    [self.navigationItem setLeftBarButtonItems:leftItems];
    [self.navigationItem setRightBarButtonItems:rightItems];
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (void)setToDefaultState
{
    [self.defaultButton sendActionsForControlEvents:UIControlEventTouchUpInside];
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (LPageContent *)createPageContentForLocation:(NSUInteger)location annotationTool:(LATool *)tool
{
    LPageContent        *content    = [[LPageContent alloc]
                                       initWithFrame:self.master.bounds
                                       page         :[self.document pageAtLocation:location]
                                       scale        :self.document.scale];

    UIViewAutoresizing  mask        = (UIViewAutoresizingFlexibleWidth|
                                       UIViewAutoresizingFlexibleHeight);

    [content setDelegate:self];
    [content setAutoresizingMask:mask];

    [content scaleContentToFitSize:[self view].bounds.size];
    [content scrollToTop];

    [content initializeStrokeContent];
    [content initializeTextContent];

    [content setStrokeContentEnabled:self.strokeContentEnabled];
    [content setTextContentEnabled:self.textContentEnabled];

    [content.page.currentAnnotator setAnnotationTool:tool];

    return content;
}

- (void)showContentAtLocation:(NSInteger)location animated:(BOOL)animated completion:(void(^)(void))completion
{
    if (![self isAnimating] && ([self pageCount] >= location && location >= 1)) {
        BOOL showPage = YES;

        if (_delegateRespondsTo.documentContentShouldShowPageAtLocation) {
            showPage = [self.delegate documentContent:self shouldShowPageAtLocation:location];
        }

        if (showPage) {
            animated = (animated && [self pageNumber] != location);

            if (animated) {
                [self setAnimating:YES];
            }
            
            BOOL        slideRight  = [self pageNumber] > location;
            CGFloat     offset      = [self master].bounds.size.width * (slideRight? -1: 1);

            LPageContent __block *content = nil;

            void(^currentPageAnimation)(void) = (^{
                self.pageContent.transform  = CGAffineTransformTranslate([_pageContent transform], -offset, 0.);
            });

            void(^currentPageCompletion)(BOOL) = (^(__UNUSEDARG(BOOL)) {
                id tool = (self.pageContent? self.pageContent.page.currentAnnotator.annotationTool: self.annotationTool);

                [self.pageContent removeFromSuperview];

                content                 = [self createPageContentForLocation:location annotationTool:tool];
                content.transform       = CGAffineTransformTranslate([content transform], offset, 0.);

                [self.master addSubview:content];
                [self.pageSlider setValue:location animated:YES];
                [self setPageContent:content];
            });

            void(^nextPageAnimation)(void) = (^{
                content.transform       = CGAffineTransformTranslate([content transform], -offset, 0);
            });

            void(^nextPageCompletion)(BOOL) = (^(__UNUSEDARG(BOOL)) {
                if (animated) {
                    [self setAnimating:NO];
                }

                if (completion) {
                    completion();
                }
            });

            if (animated) {
                [UIView
                 animateWithDuration:.2
                 animations         :currentPageAnimation
                 completion         :^(BOOL finished) {
                    currentPageCompletion(finished);

                    [UIView
                     animateWithDuration:.3
                     animations         :nextPageAnimation
                     completion         :nextPageCompletion];
                }];
            } else {
                currentPageAnimation();
                currentPageCompletion(YES);
                nextPageAnimation();
                nextPageCompletion(YES);
            }
        }
    }
}

- (void)exportURLToEmail:(NSURL *)exportURL withName:(NSString *)exportName
{
    if ([MFMailComposeViewController canSendMail]) {
        [self.document exportToDestination:exportURL];

        NSDataReadingOptions
        options				= (NSDataReadingMappedIfSafe | NSDataReadingUncached);

        NSData *attachment	= [NSData dataWithContentsOfURL:exportURL options:options error:nil];

        if (attachment != nil) {
            MFMailComposeViewController *mailComposer = [MFMailComposeViewController new];

            [mailComposer addAttachmentData:attachment mimeType:@"application/pdf" fileName:exportName];
            [mailComposer setSubject:exportName];

            mailComposer.modalTransitionStyle = [self modalTransitionStyle];
            mailComposer.modalPresentationStyle = UIModalPresentationFormSheet;

            mailComposer.mailComposeDelegate = self;

            [self presentViewController:mailComposer animated:YES completion:nil];
        } else {
            UIAlertView *alertView = [UIAlertView new];

            [alertView setTitle:@"Export To E-Mail"];
            [alertView setMessage:@Format(@"Unable to attach file '%@'", exportName)];
            [alertView addButtonWithTitle:@"OK"];

            [alertView show];
        }
    } else {
        UIAlertView *alertView = [UIAlertView new];

        [alertView setTitle:@"Export To E-Mail"];
        [alertView setMessage:@"Please check your email configuration in the Settings App."];
        [alertView addButtonWithTitle:@"OK"];

        [alertView show];
    }
}


#pragma mark - LDocumentDelegate
//*********************************************************************************************************************//

- (void)document:(LDocument *)document didExportPageAtLocation:(NSUInteger)location
{
}


#pragma mark - LPageContentDelegate
//*********************************************************************************************************************//

- (void)pageContent:(LPageContent *)content didZoomToRect:(CGRect)rect
{
    if (!CGRectEqualToRect([self zoomRect], rect)) {
        [self setZoomRect:rect];
    }
}

- (void)pageContent:(LPageContent *)content didBeginAnnotationAtPoint:(CGPoint)point
{
    if ([self.annotationTool isEditor]) {
        [self.currentPage removeAnnotationAtIndex:[self.currentAnnotator annotationIndexForPoint:point]];
    }

    if (_delegateRespondsTo.documentContentDidBeginAnnotationAtPoint) {
        [self.delegate documentContent:self didBeginAnnotationAtPoint:point];
    }
}

- (void)pageContent:(LPageContent *)content didMoveAnnotationToPoint:(CGPoint)point
{
    if ([self.annotationTool isEditor]) {
        [self.currentPage removeAnnotationAtIndex:[self.currentAnnotator annotationIndexForPoint:point]];
    }

    if (_delegateRespondsTo.documentContentDidMoveAnnotationToPoint) {
        [self.delegate documentContent:self didMoveAnnotationToPoint:point];
    }
}

- (void)pageContent:(LPageContent *)content didEndAnnotationAtPoint:(CGPoint)point
{
    if ([self.annotationTool isEditor]) {
        [self.currentPage removeAnnotationAtIndex:[self.currentAnnotator annotationIndexForPoint:point]];
    } else {
        [self.currentPage.recordUndoOperation removeLastAnnotation];
    }

    if (_delegateRespondsTo.documentContentDidEndAnnotationAtPoint) {
        [self.delegate documentContent:self didEndAnnotationAtPoint:point];
    }
}

- (void)pageContent:(LPageContent *)content didBeginEditingInRect:(CGRect)rect
{
    [self.delegate documentContent:self didBeginEditingTextInRect:rect];
}

- (void)pageContent:(LPageContent *)content isEditingText:(NSAttributedString *)text inRect:(CGRect)rect
{
    [self.delegate documentContent:self isEditingText:text inRect:rect];
}

- (void)pageContent:(LPageContent *)content didEndEditingInRect:(CGRect)rect
{
    [self.delegate documentContent:self didEndEditingTextInRect:rect];
}

- (void)pageContent:(LPageContent *)content willEditTextAtPoint:(CGPoint)point
{
    [self.delegate documentContent:self willEditAnnotationAtPoint:point];
}

- (void)pageContent:(LPageContent *)content didMoveTextToRect:(CGRect)rect
{
    [self.delegate documentContent:self didPlaceAnnotationInRect:rect];
}


#pragma mark - LToolbar Callbacks
//*********************************************************************************************************************//

- (void)toolbarPressToolButton:(LButton *)button
{
    BOOL            isSelected          = [button isSelected];

    NSMutableArray  *passthroughViews   = [NSMutableArray new];

    for (UIBarButtonItem *item in [self.toolbar items]) {
        LButton *button = (id)[item customView];

        [button setSelected:NO];
        [passthroughViews addObject:button];
        [button.associatedObject dismissPopoverAnimated:YES];
    }

    if (isSelected) {
        [passthroughViews removeObject:button];
        [button.associatedObject presentPopoverAnimated:YES];
        [button.associatedObject setPassthroughViews:passthroughViews];
    }

    [button setSelected:YES];
}


#pragma mark - MFMailComposeViewControllerDelegate
//*********************************************************************************************************************//

- (void)
mailComposeController	:(MFMailComposeViewController *	)controller
didFinishWithResult		:(MFMailComposeResult			)result
error					:(NSError *						)error
{
	[self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - UIGestureRecognizer Callbacks
//*********************************************************************************************************************//

- (void)didPan:(UIPanGestureRecognizer *)recognizer
{
    BOOL shouldTurn = YES;

    if (_delegateRespondsTo.documentContentShouldAllowUserPageTurn) {
        shouldTurn = [self.delegate documentContentShouldAllowUserPageTurn:self];
    }

    if (shouldTurn && ![self isAnimating]) {
        switch ([recognizer state]) {
            case UIGestureRecognizerStateBegan:
            case UIGestureRecognizerStateChanged: {
                CGPoint velocity    = [recognizer velocityInView:[self.pageContent superview]];

                CGPoint translation = [recognizer translationInView:[self.pageContent superview]];
                CGPoint viewcenter  = [self.pageContent center];

                viewcenter.x += translation.x;

                [_pageContent setCenter:viewcenter];
                [recognizer setTranslation:CGPointZero inView:[self.pageContent superview]];

                [self setPanDirection:isgreater(velocity.x, 0.)];
            } break;
            case UIGestureRecognizerStateEnded: {
                CGRect bounds  = [self.pageContent.superview bounds];
                CGRect visible = CGRectIntersection(self.pageContent.frame, bounds);

                if ([self panDirection]) {
                    if ([self pageNumber] > 1) {
                        if (IsEqual(bounds.size.width,
                                        visible.origin.x +
                                        visible.size.width) &&
                            isless(visible.size.width, 640.)) {
                            [self setPageNumber:[self pageNumber]-1 animated:YES];

                            break;
                        }
                    }
                } else {
                    if ([self pageNumber] < [self pageCount]) {
                        if (IsZero(visible.origin.x) &&
                            isless(visible.size.width, 640.)) {
                            [self setPageNumber:[self pageNumber]+1 animated:YES];

                            break;
                        }
                    }
                }

                [self setAnimating:YES];

                [UIView animateWithDuration:.3 animations:^{
                    [self.pageContent setCenter:self.pageContent.superview.center];
                } completion:^(BOOL finished) {
                    [self setAnimating:NO];
                }];
            } break;
            default: {

            } break;
        }
    }
}

- (void)didDoubleTap:(UITapGestureRecognizer *)recognizer
{
    if (![self isAnimating] && [recognizer state] == UIGestureRecognizerStateEnded) {
        [self.pageContent setZoomScale:self.pageContent.minimumZoomScale animated:YES];
    }
}

#pragma mark - LSliderDelegate
//*********************************************************************************************************************//

- (void)sliderDidCancelValueChange:(LSlider *)slider
{
    [self.pageSlider setValue:self.pageNumber animated:YES];
}

- (void)slider:(LSlider *)slider didChangeToValue:(NSInteger)value
{
    [self setPageNumber:value animated:YES];
}

- (id)sliderNotificationForValue:(NSInteger)value
{
    LPage   *page   = [self.document pageAtLocation:value];

    CGFloat scale   = [self document].scale *.25;

    CGRect  frame   = [self master].bounds;

    if (isgreater(page.dimensions.height, page.dimensions.width)) {
        frame.size.height   *= .25;
        frame.size.width    = (frame.size.height) * (page.dimensions.width / page.dimensions.height);
    } else {
        frame.size.width    *= .25;
        frame.size.height   = (frame.size.width) * (page.dimensions.height / page.dimensions.width);;
    }

    LPageLoader *loader = [[LPageLoader alloc] initWithFrame:frame page:page scale:scale];
    {
        [loader setBackgroundColor:self.pageSlider.backgroundColor];
    }

    return loader;
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Private Implementations
#pragma mark -
//*********************************************************************************************************************//




@implementation LDocumentContent (Setup)

#pragma mark - Public Methods
//*********************************************************************************************************************//

- (void)setupNavigationBar
{
    UIBarButtonItem *$sub(spacer, void) {
        return
        [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:NULL];
    };

    UIBarButtonItem *$sub(newToolButton, NSString *name) {
        NSString *iconName      = [name lowercaseString];
        NSString *tapIconName   = @Join(name.lowercaseString, @"-tap");
        NSString *selname       = @Format(@"setup%@Button:", name.capitalizedString);

        LButton *button   = [LButton button];
        button.normalImage      = [UIImage imageNamed:iconName];
        button.selectedImage    = [UIImage imageNamed:tapIconName];
        button.tag              = 'dflt';
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [self performSelector:NSSelectorFromString(selname) withObject:button];
#pragma clang diagnostic pop
        return [[UIBarButtonItem alloc] initWithCustomView:button];
    };

    [self setTitleView:[UIView new]];
    [self setToolbar:[UIToolbar new]];

    [self.navigationItem setHidesBackButton:YES];

    NSMutableArray *toolItems = [NSMutableArray new];

    [toolItems addObject:newToolButton(@"pen")];
    [toolItems addObject:newToolButton(@"highlighter")];
    [toolItems addObject:newToolButton(@"eraser")];
    [toolItems addObject:newToolButton(@"text")];

    NSMutableArray *leftItems = [NSMutableArray new];

    [leftItems addObjectsFromArray:self.leftUtilityItems];

    NSMutableArray *rightItems = [NSMutableArray new];

    [rightItems addObjectsFromArray:self.rightUtilityItems];

    [rightItems addObject:newToolButton(@"export")];
    [rightItems addObject:spacer()];
    [rightItems addObject:newToolButton(@"undo")];
    [rightItems addObject:spacer()];

    [self.toolbar setClipsToBounds:YES];
    [self.toolbar setBackgroundImage:[UIImage new] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];

    [self.toolbar setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
    [self.toolbar setFrame:((CGRect) {
        .origin = {0},
        .size   = {
            .width  = 44. *5.5,
            .height = 44.
        }
    })];

    [self.titleView setFrame:self.toolbar.bounds];
    [self.titleView addSubview:self.toolbar];

    [self.navigationItem setTitleView:self.titleView];

    [self.toolbar setItems:toolItems animated:YES];

    [self.navigationItem setLeftBarButtonItems:leftItems animated:YES];
    [self.navigationItem setRightBarButtonItems:rightItems animated:YES];
}

- (void)setupToolbar
{
    UIBarButtonItem *$sub(spacer, void) {
        return
        [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:NULL];
    };


    NSMutableArray *items = [NSMutableArray new];


    LSlider *slider = [[LSlider alloc] initWithFrame:((CGRect) {
        .origin = CGPointZero,
        .size   = {
            .width  = $screensize.width -176.,
            .height = 26.
        }
    })];

    [slider setRange:((NSRange) {
        .location   = 1,
        .length     = [self pageCount]
    })];

    [slider setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [slider setDelegate:self];

    [self setPageSlider:slider];

    UIBarButtonItem *sliderItem = [[UIBarButtonItem alloc] initWithCustomView:slider];


    [items addObjectsFromArray:self.leftToolbarItems];
    [items addObject:spacer()];
    [items addObject:sliderItem];
    [items addObject:spacer()];
    [items addObjectsFromArray:self.rightToolbarItems];


    [self setToolbarItems:items animated:YES];
}

- (void)setupContent
{
    {
        UIPanGestureRecognizer *recognizer = [UIPanGestureRecognizer new];
        [recognizer addTarget:self action:@selector(didPan:)];
        [recognizer setMaximumNumberOfTouches:2];
        [recognizer setMinimumNumberOfTouches:2];

        [self.master addGestureRecognizer:recognizer];

        {_pageTurnRecognizer = recognizer;}
    }

    {
        UITapGestureRecognizer *recognizer = [UITapGestureRecognizer new];
        [recognizer addTarget:self action:@selector(didDoubleTap:)];
        [recognizer setNumberOfTouchesRequired:2];
        [recognizer setNumberOfTapsRequired:2];

        [self.master addGestureRecognizer:recognizer];

        {_zoomResetRecognizer = recognizer;}
    }

    [self setPageNumber:1];

    if (_delegateRespondsTo.documentContentDidLoad) {
        [_delegate documentContentDidLoad:self];
    }
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (void)setupToggleButton:(LButton *)button withPopover:(LPopoverController *)popover
{
    [button setAssociatedObject:popover];
    [button.associatedObject setPresenterView:button];

    [button addTarget:self action:@selector(toolbarPressToolButton:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setupPenButton:(LButton *)penButton
{
    LAnnotationPicker *penPicker = [LAnnotationPicker new];
    [penPicker setColor:[LAnnotationPicker colorForIndex:0]];
    [penPicker setWeight:[LAnnotationPicker weightForIndex:0]];

    LPopoverController *penPopover = [[LPopoverController alloc] initWithContentView:penPicker];

    [penPopover setContentSizeCallback:^(LPopoverController *popover) {
        return [penPicker bounds].size;
    }];

    [self.penTool.properties setAlpha:1.];
    [self.highlighterTool.properties setMultiplier:1.];

    [penPicker mapKeyPaths:@[@"color", @"weight"]](self.penTool.properties);

    SEL selector = NSSelectorFromString(@"didPressPenButton");

    [penButton addTarget:self action:selector forControlEvents:UIControlEventTouchUpInside],
    [self.class addMethodWithSelector:selector withBlockImplementation:

     ^(typeof(self) self) {
        [self setAnnotationTool:[self penTool]];
        [self setStrokeContentEnabled:YES];
        [self setTextContentEnabled:NO];
     }

     ];

    [self setupToggleButton:penButton withPopover:penPopover];

    [self setDefaultButton:penButton];
}

- (void)setupHighlighterButton:(LButton *)highlighterButton
{
    LAnnotationPicker *highlighterPicker = [LAnnotationPicker new];
    [highlighterPicker setColor:[LAnnotationPicker colorForIndex:3]];
    [highlighterPicker setWeight:[LAnnotationPicker weightForIndex:2]];

    LPopoverController *highlighterPopover = [[LPopoverController alloc] initWithContentView:highlighterPicker];

    [highlighterPopover setContentSizeCallback:^(LPopoverController *popover) {
        return [highlighterPicker bounds].size;
    }];

    [self.highlighterTool.properties setAlpha:.3];
    [self.highlighterTool.properties setMultiplier:3.];

    [highlighterPicker mapKeyPaths:@[@"color", @"weight"]](self.highlighterTool.properties);

    SEL selector = NSSelectorFromString(@"didPressHighlighterButton");

    [highlighterButton addTarget:self action:selector forControlEvents:UIControlEventTouchUpInside],
    [self.class addMethodWithSelector:selector withBlockImplementation:

     ^(typeof(self) self) {
         [self setAnnotationTool:[self highlighterTool]];
         [self setStrokeContentEnabled:YES];
         [self setTextContentEnabled:NO];
     }

     ];

    [self setupToggleButton:highlighterButton withPopover:highlighterPopover];
}

- (void)setupEraserButton:(LButton *)eraserButton
{
    SEL selector = NSSelectorFromString(@"didPressEraserButton");

    [eraserButton addTarget:self action:selector forControlEvents:UIControlEventTouchUpInside],
    [self.class addMethodWithSelector:selector withBlockImplementation:

     ^(typeof(self) self) {
         [self setAnnotationTool:[self eraserTool]];
         [self setStrokeContentEnabled:YES];
         [self setTextContentEnabled:NO];
     }

     ];

    [self setupToggleButton:eraserButton withPopover:nil];
}

- (void)setupTextButton:(LButton *)textButton
{
    SEL selector = NSSelectorFromString(@"didPressTextButton");

    [textButton addTarget:self action:selector forControlEvents:UIControlEventTouchUpInside],
    [self.class addMethodWithSelector:selector withBlockImplementation:

     ^(typeof(self) self) {
         [self setAnnotationTool:nil];
         [self setStrokeContentEnabled:NO];
         [self setTextContentEnabled:YES];
     }

     ];

    [self setupToggleButton:textButton withPopover:nil];
}

- (void)setupUndoButton:(LButton *)undoButton
{
    SEL undoSelector = NSSelectorFromString(@"didPressUndoButton");
    SEL redoSelector = NSSelectorFromString(@"didPressRedoButton");
    SEL menuSelector = NSSelectorFromString(@"didLongPressUndoButton");

    LButton *redoButton = [LButton button];
    [redoButton setTitle:@"redo" forState:UIControlStateNormal];
    [redoButton.titleLabel setFont:[[UIFont LDefaultFont] fontWithSize:16.]];
    [redoButton sizeToFit];

    LPopupView *popup = [LPopupView new];

    [popup setBackgroundColor:[UIColor whiteColor]];
    [popup setShouldDismissOnTapAnywhere:YES];
    [popup setHasShadow:YES];
    [popup setCustomView:redoButton];


    [undoButton addTarget:self action:undoSelector forControlEvents:UIControlEventTouchUpInside],
    [self.class addMethodWithSelector:undoSelector withBlockImplementation:

     ^(typeof(self) self) {
         if ([self.currentPage canUndo]) {
             [self.currentPage undo];

             if (self->_delegateRespondsTo.documentContentDidUndo) {
                 [self.delegate documentContentDidUndo:self];
             }
         }
     }
     
     ];

    [redoButton addTarget:self action:redoSelector forControlEvents:UIControlEventTouchUpInside],
    [self.class addMethodWithSelector:redoSelector withBlockImplementation:

     ^(typeof(self) self) {
         if ([self.currentPage canRedo]) {
             [popup dismissAnimated:YES];

             [self.currentPage redo];

             if (self->_delegateRespondsTo.documentContentDidRedo) {
                 [self.delegate documentContentDidRedo:self];
             }
         }
     }
     
     ];

    id recognizer = [UILongPressGestureRecognizer new];

    [undoButton addGestureRecognizer:recognizer], [recognizer addTarget:self action:menuSelector],
    [self.class addMethodWithSelector:menuSelector withBlockImplementation:

     ^(typeof(self) self) {
         BOOL canRedo = [self.currentPage canRedo];

         [redoButton setTitleColor:canRedo? [UIColor LTealColor]: [UIColor LGrayColor] forState:UIControlStateNormal];

         [popup presentPointingAtView:undoButton inView:[self.parentViewController view] animated:YES];
     }
     
     ];
}

- (void)setupExportButton:(LButton *)exportButton
{
    SEL selector = NSSelectorFromString(@"didPressExportButton");

    [exportButton addTarget:self action:selector forControlEvents:UIControlEventTouchUpInside],
    [self.class addMethodWithSelector:selector withBlockImplementation:

     ^(typeof(self) self) {
         NSString	*exportName         = @Basename(self.document.URL.path);
         NSString	*exportPath         = [NSTemporaryDirectory() stringByAppendingPathComponent:exportName];
         NSURL		*exportURL          = [NSURL fileURLWithPath:
                                           @Join(exportPath.stringByDeletingPathExtension, @"_export.pdf")];

         [self exportURLToEmail:exportURL withName:exportName];
     }

     ];
}


@end
