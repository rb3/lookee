//
//  LPageLoader.m
//  Writeability
//
//  Created by Ryan Blonna on 30/10/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LPageLoader.h"

#import "LPage.h"

#import "LPageContent.h"

#import <Utilities/LMacros.h>
#import <Utilities/LOperationStack.h>

#import <Utilities/NSThread+Block.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LPageLoader
#pragma mark -
//*********************************************************************************************************************//




@implementation LPageLoader

#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (LOperationStack *)stackForScale:(CGFloat)scale
{
    static NSMutableDictionary  *instances;
    static dispatch_once_t      predicate;

    dispatch_once(&predicate, ^{
        instances = [NSMutableDictionary new];
    });

    id instance;

    @synchronized(instances) {
        if (!(instance = instances[@(scale)])) {
            instances[@(scale)] = instance
            =
            [LOperationStack new];
        }
    }

    return instance;
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)initWithFrame:(CGRect)frame page:(LPage *)page scale:(CGFloat)scale
{
    if ((self = [super initWithFrame:frame])) {
        _page   = page;
        _scale  = scale;

        [self loadPageContent];
    }

    return self;
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (void)loadPageContent
{
    LPageContent *$sub(content, void) {
        LPageContent *content
        =
        [[LPageContent alloc]
         initWithFrame  :self.bounds
         page           :self.page
         scale          :self.page.scale];

        [content setUserInteractionEnabled:NO];
        [content setTagsVisible:NO];
        [content scaleContentToFitSize:self.bounds.size];

        return content;
    };

    if ([self.page isCachedAtScale:self.scale]) {
        [self addSubview:content()];
        return;
    }

    UIView *hud = [UIView new];
    {
        [hud setFrame:self.bounds];
        [hud setBackgroundColor:self.backgroundColor];
    }

    [self addSubview:hud];

    $weakify(self) {
        [[self.class stackForScale:self.scale] addOperationWithBlock:^{
            $safify(self) {
                /*
                 * We pre-cache the page image in 
                 * a background thread to avoid lag.
                 */
                [self.page cacheWithScale:self.scale];

                [$mainqueue addOperationWithBlock:^{
                    [self insertSubview:content() belowSubview:hud];
                    
                    [UIView animateWithDuration:.3 animations:^{
                        [hud setAlpha:0.];
                    } completion:^(BOOL finished) {
                        [hud removeFromSuperview];
                    }];
                }];
            }
        }];
    }
}

@end
