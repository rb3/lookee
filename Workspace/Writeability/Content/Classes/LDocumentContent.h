//
//  LDocumentContent.h
//  Writeability
//
//  Created by Ryan on 3/19/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <UIKit/UIKit.h>


@class LDocument;
@class LPage;

@class LATool;

@class LPageContent;



@interface LDocumentContent : UIViewController

@property (nonatomic, readonly  , strong) LDocument     *document;

@property (nonatomic, readonly  , assign) NSUInteger    pageCount;
@property (nonatomic, readwrite , assign) NSUInteger    pageNumber;

@property (nonatomic, readwrite , assign) CGRect        zoomRect;

@property (nonatomic, readonly  , strong) NSDictionary  *textEditingAttributes;

@property (nonatomic, readwrite , weak  ) id            delegate;

@property (nonatomic, readwrite , strong) NSArray       *leftUtilityItems;
@property (nonatomic, readwrite , strong) NSArray       *rightUtilityItems;

@property (nonatomic, readwrite , strong) NSArray       *leftToolbarItems;
@property (nonatomic, readwrite , strong) NSArray       *rightToolbarItems;
@property (nonatomic, readwrite , strong) UIView        *centerToolbarView;

@property (nonatomic, readwrite , assign, getter=isInteractionEnabled)  BOOL interactionEnabled;


- (instancetype)initWithDocument:(LDocument *)document;

- (void)setPageNumber:(NSUInteger)pageNumber animated:(BOOL)animated;
- (void)setPageNumber:(NSUInteger)pageNumber animated:(BOOL)animated completion:(void(^)(void))completion;
- (void)setZoomRect:(CGRect)zoomRect animated:(BOOL)animated;

- (void)setToDefaultState;

@end




// !!!: We use an informal protocol here to make it easy to extend
@interface NSObject (LDocumentContentDelegate)

- (void)documentContentDidLoad:(LDocumentContent *)content;

- (BOOL)documentContentShouldAllowUserPageTurn:(LDocumentContent *)content;

- (BOOL)documentContent:(LDocumentContent *)content shouldShowPageAtLocation:(NSUInteger)location;

- (void)documentContent:(LDocumentContent *)content didBeginAnnotationAtPoint:(CGPoint)point;
- (void)documentContent:(LDocumentContent *)content didMoveAnnotationToPoint:(CGPoint)point;
- (void)documentContent:(LDocumentContent *)content didEndAnnotationAtPoint:(CGPoint)point;

- (void)documentContent:(LDocumentContent *)content didBeginEditingTextInRect:(CGRect)rect;
- (void)documentContent:(LDocumentContent *)content isEditingText:(NSAttributedString *)text inRect:(CGRect)rect;
- (void)documentContent:(LDocumentContent *)content didEndEditingTextInRect:(CGRect)rect;

- (void)documentContent:(LDocumentContent *)content willEditAnnotationAtPoint:(CGPoint)point;
- (void)documentContent:(LDocumentContent *)content didPlaceAnnotationInRect:(CGRect)rect;

- (void)documentContentDidUndo:(LDocumentContent *)content;
- (void)documentContentDidRedo:(LDocumentContent *)content;

@end
