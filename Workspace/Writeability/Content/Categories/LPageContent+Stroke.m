//
//  LPageContent+Stroke.m
//  Writeability
//
//  Created by Ryan on 3/4/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LPageContent+Stroke.h"
#import "LPageContent_Private.h"

#import <Annotation/LAnnotator.h>

#import <Utilities/LMacros.h>
#import <Utilities/LRuntime.h>
#import <Utilities/LTouchRecognizer.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LPageContent+Stroke
#pragma mark -
//*********************************************************************************************************************//




@interface LPageContent (Stroke_Properties)

@property (nonatomic, readwrite, strong) NSMutableArray *strokeContentRecognizers;

@end


@implementation LPageContent (Stroke)

#pragma mark - Initializers
//*********************************************************************************************************************//

- (void)initializeStrokeContent
{
    if (![self strokeContentRecognizers]) {
        [self setStrokeContentRecognizers:[NSMutableArray new]];

        id recognizer;

        recognizer = [LTouchRecognizer new];
        [recognizer addTarget:self action:@selector(trackStroke:)];
        [self.overlay addGestureRecognizer:recognizer];

        [self.strokeContentRecognizers addObject:recognizer];
        
        [self setStrokeContentEnabled:NO];
    }
}


#pragma mark - Properties
//*********************************************************************************************************************//

@implement(LPageContent, strokeContentEnabled, BOOL, nonatomic, readwrite, assign, getter = isStrokeContentEnabled);

- (void)setStrokeContentEnabled:(BOOL)strokeContentEnabled
{
    LRuntimeSetAssociatedValue(LPageContent, strokeContentEnabled, BOOL, strokeContentEnabled, kLRunTimeAssociationAssign);

    for (UIGestureRecognizer *recognizer in [self strokeContentRecognizers]) {
        [recognizer setEnabled:strokeContentEnabled];
    }
}


#pragma mark - UIGestureRecognizer Callbacks
//*********************************************************************************************************************//

- (void)trackStroke:(LTouchRecognizer *)recognizer
{
    CGPoint location        = [recognizer location];
    CGPoint startPoint      = [recognizer startPoint];
    CGPoint previousPoint   = [recognizer previousPoint];

    switch ([recognizer state]) {
        case UIGestureRecognizerStateBegan: {

        } break;
        case UIGestureRecognizerStateChanged: {
            if ([recognizer justBegan]) {
                [self.page.currentAnnotator.annotationTool beginAtPoint:location];

                if ([self.delegate respondsToSelector:@selector(pageContent:didBeginAnnotationAtPoint:)]) {
                    [self.delegate pageContent:self didBeginAnnotationAtPoint:startPoint];
                }
            }

            [self.page.currentAnnotator.annotationTool moveToPoint:location];

            if ([self.delegate respondsToSelector:@selector(pageContent:didMoveAnnotationToPoint:)]) {
                [self.delegate pageContent:self didMoveAnnotationToPoint:location];
            }
        } break;
        case UIGestureRecognizerStateEnded: {
			if ([recognizer didBegin] && ![recognizer didMove]) {
                [self.page.currentAnnotator.annotationTool beginAtPoint:location];

                if ([self.delegate respondsToSelector:@selector(pageContent:didBeginAnnotationAtPoint:)]) {
                    [self.delegate pageContent:self didBeginAnnotationAtPoint:startPoint];
                }
            }

            [self.page.currentAnnotator.annotationTool endAtPoint:location];

            if ([self.delegate respondsToSelector:@selector(pageContent:didEndAnnotationAtPoint:)]) {
                [self.delegate pageContent:self didEndAnnotationAtPoint:location];
            }
        } break;
        case UIGestureRecognizerStateCancelled: {
            if ([recognizer didMove]) {
                [self.page.currentAnnotator.annotationTool endAtPoint:location];

                if ([self.delegate respondsToSelector:@selector(pageContent:didEndAnnotationAtPoint:)]) {
                    [self.delegate pageContent:self didEndAnnotationAtPoint:previousPoint];
                }
            }
        } break;
        default: {
            
        } break;
    }
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Protected Implementations
#pragma mark -
//*********************************************************************************************************************//




@implementation LPageContent (Stroke_Properties)

@implement(LPageContent, strokeContentRecognizers, NSMutableArray *, nonatomic, readwrite, strong);

@end
