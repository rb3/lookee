//
//  LPageContent+Text.h
//  Writeability
//
//  Created by Ryan on 2/28/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LPageContent.h"


@interface LPageContent (Text)

@property (nonatomic, readwrite , assign, getter = isTextContentEnabled)    BOOL textContentEnabled;

@property (nonatomic, readonly  , strong) NSDictionary                      *editingAttributes;
@property (nonatomic, readonly  , strong) NSAttributedString                *editingText;
@property (nonatomic, readonly  , assign) CGRect                            editingTextRect;


- (void)initializeTextContent;

- (void)setText:(NSAttributedString *)text inRect:(CGRect)rect;
- (void)deleteTextAtPoint:(CGPoint)point;

- (BOOL)editTextAtPoint:(CGPoint)point;
- (void)finishEditing;

- (NSAttributedString *)textAtPoint:(CGPoint)point;
- (CGRect)textRectAtPoint:(CGPoint)point;

@end


@interface NSObject (LPageContentDelegate_Text)

- (NSDictionary *)pageContent:(LPageContent *)content metadataForTextAtPoint:(CGPoint)point;

- (void)pageContent:(LPageContent *)content willInsertTextAtPoint:(CGPoint)point;
- (void)pageContent:(LPageContent *)content willEditTextAtPoint:(CGPoint)point;

- (void)pageContent:(LPageContent *)content didBeginEditingInRect:(CGRect)rect;
- (void)pageContent:(LPageContent *)content isEditingText:(NSAttributedString *)text inRect:(CGRect)rect;
- (void)pageContent:(LPageContent *)content didEndEditingInRect:(CGRect)rect;

- (void)pageContent:(LPageContent *)content didMoveTextToRect:(CGRect)rect;

@end
