//
//  LPageContent+Text.m
//  Writeability
//
//  Created by Ryan on 2/28/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LPageContent+Text.h"
#import "LPageContent_Private.h"

#import "LTextBox.h"

#import <Annotation/LAnnotator+Text.h>

#import <Utilities/LRuntime.h>
#import <Utilities/LGeometry.h>

#import <Utilities/NSObject+Utilities.h>
#import <Utilities/NSData+Utilities.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LPageContent+Text
#pragma mark -
//*********************************************************************************************************************//




@interface LPageContent (Text_Properties)

@property (nonatomic, readwrite, strong) NSMutableArray *textContentRecognizers;

@property (nonatomic, readwrite, strong) NSDictionary   *editingAttributes;

@property (nonatomic, readwrite, strong) LTextBox       *textBox;

@property (nonatomic, readwrite, assign) CGPoint        textOrigin;

@end


@interface LPageContent (Text_LTextBoxDelegate) <LTextBoxDelegate>

@end


@implementation LPageContent (Text)

#pragma mark - Static Objects
//*********************************************************************************************************************//

static NSString *const  kLPCMetadataAttributedText          = @"attributedText";
static NSString *const  kLPCMetadataTextRect                = @"textRect";

static CGFloat const    kLPCMinimumTextWidth                = 32.;


#pragma mark - Initializers
//*********************************************************************************************************************//

- (void)initializeTextContent
{
    if (![self textContentRecognizers]) {
        [self setTextContentRecognizers:[NSMutableArray new]];

        id recognizer;

        recognizer = [UITapGestureRecognizer new];
        [recognizer addTarget:self action:@selector(trackTextSelect:)];
        [self.overlay addGestureRecognizer:recognizer];

        [self.textContentRecognizers addObject:recognizer];

        recognizer = [UIPanGestureRecognizer new];
        [recognizer setMaximumNumberOfTouches:1];
        [recognizer setMinimumNumberOfTouches:1];
        [recognizer addTarget:self action:@selector(trackTextSize:)];
        [self.overlay addGestureRecognizer:recognizer];

        [self.textContentRecognizers addObject:recognizer];

        [self setTextContentEnabled:NO];

        $weakify(self) {
            [self initializedObserverForKeyPath:@Keypath(self.frame)] (^{
                $strongify(self) {
                    if ([self textBox]) {
                        [self
                         scrollRectToVisible:[self
                                              convertRect   :self.textBox.bounds
                                              fromView      :self.textBox]
                         animated           :YES];
                    }
                }
            });
        }
    }
}


#pragma mark - Properties
//*********************************************************************************************************************//

@implement(LPageContent, textContentEnabled, BOOL, nonatomic, readwrite, assign);

- (void)setTextContentEnabled:(BOOL)textContentEnabled
{
    LRuntimeSetAssociatedValue(LPageContent, textContentEnabled, BOOL, textContentEnabled, kLRunTimeAssociationAssign);

    for (UIGestureRecognizer *recognizer in [self textContentRecognizers]) {
        [recognizer setEnabled:textContentEnabled];
    }

    if (!textContentEnabled) {
        [self finishEditing];
    }
}

@dynamic editingText;

- (NSAttributedString *)editingText
{
    return [self.textBox attributedText];
}

@dynamic editingTextRect;

- (CGRect)editingTextRect
{
    if ([self textBox]) {
        return [self.textBox textRect];
    }

    return CGRectNull;
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (void)setText:(NSAttributedString *)text inRect:(CGRect)rect
{
    [self.page.currentAnnotator setText:text inRect:rect];
}

- (void)deleteTextAtPoint:(CGPoint)point
{
    [self.page.currentAnnotator removeAnnotationAtIndex:
     [self.page.currentAnnotator textAnnotationIndexForPoint:point]];
}

- (BOOL)editTextAtPoint:(CGPoint)point
{
    LTextBox *textBox = [self textBoxAtPoint:point];

    if (textBox) {
        [self finishEditing];

        [self.overlay addSubview:textBox];
        [self setTextBox:textBox];

        [textBox mapKeyPath:@Keypath(textBox.editingAttributes)](self);

        [self deleteTextAtPoint:point];
        [self.textBox becomeFirstResponder];

        return YES;
    }

    return NO;
}

- (void)finishEditing
{
    [self.textBox resignFirstResponder];
}

- (NSAttributedString *)textAtPoint:(CGPoint)point
{
    if (CGRectContainsPoint([self.textBox frame], point)) {
        return [self.textBox attributedText];
    }

    if ([self.delegate respondsToSelector:@selector(pageContent:metadataForTextAtPoint:)]) {
        return [self.delegate pageContent:self metadataForTextAtPoint:point][kLPCMetadataAttributedText];
    }

    return nil;
}

- (CGRect)textRectAtPoint:(CGPoint)point
{
    if (CGRectContainsPoint([self.textBox frame], point)) {
        return [self.textBox textRect];
    }

    if ([self.delegate respondsToSelector:@selector(pageContent:metadataForTextAtPoint:)]) {
        return [[self.delegate pageContent:self metadataForTextAtPoint:point][kLPCMetadataTextRect] CGRectValue];
    }

    return CGRectNull;
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (LTextBox *)textBoxWithFrame:(CGRect)frame
{
    LTextBox *textBox = [[LTextBox alloc]
                         initWithFrame  :CGRectIntegral(frame)
                         limits         :self.overlay.bounds
                         attributes     :self.editingAttributes];

    [textBox setDelegate:self];

    $weakify(self, textBox) {
        [textBox observerForKeyPath:@Keypath(textBox.frame)] (^{
            $strongify(self, textBox) {
                if (textBox && [textBox superview] == [self overlay]) {
                    [self
                     scrollRectToVisible:[self
                                          convertRect   :textBox.bounds
                                          fromView      :textBox]
                     animated           :NO];
                }
            }
        });
    }

    return textBox;
}

- (LTextBox *)textBoxAtPoint:(CGPoint)point
{
    if (CGRectContainsPoint([self.textBox frame], point)) {
        return [self textBox];
    }

    NSAttributedString  *text       = [self textAtPoint:point];
    CGRect              textRect    = [self textRectAtPoint:point];

    if (!CGRectIsNull(textRect) && text) {
        LTextBox        *textBox    = [[LTextBox alloc] initWithFrame:textRect limits:self.overlay.bounds];

        [textBox setAttributedText:text];
        [textBox setDelegate:self];

        $weakify(self, textBox) {
            [textBox observerForKeyPath:@Keypath(textBox.frame)] (^{
                $strongify(self, textBox) {
                    if (textBox && [textBox superview] == [self overlay]) {
                        [self
                         scrollRectToVisible:[self
                                              convertRect   :textBox.bounds
                                              fromView      :textBox]
                         animated           :NO];
                    }
                }
            });
        }
        
        return textBox;
    }

    return nil;
}


#pragma mark - UIGestureRecognizer Callbacks
//*********************************************************************************************************************//

- (void)trackTextSelect:(UITapGestureRecognizer *)recognizer
{
    CGPoint location = [recognizer locationInView:self.overlay];

    switch ([recognizer state]) {
        case UIGestureRecognizerStateRecognized: {
            if ([self textBox]) {
                [self finishEditing];
            } else {
                if ([self.delegate respondsToSelector:@selector(pageContent:willEditTextAtPoint:)]) {
                    [self.delegate pageContent:self willEditTextAtPoint:location];
                }

                [self editTextAtPoint:location];
            }
        } break;
        default: {

        } break;
    }
}

- (void)trackTextSize:(UIPanGestureRecognizer *)recognizer
{
    CGPoint location = [recognizer locationInView:self.overlay];

    switch ([recognizer state]) {
        case UIGestureRecognizerStateBegan      : {
            if ([self textBox]) {
                [self finishEditing];
            } else {
                if ([self.delegate respondsToSelector:@selector(pageContent:willInsertTextAtPoint:)]) {
                    [self.delegate pageContent:self willInsertTextAtPoint:location];
                }

                LTextBox *textBox = [self textBoxWithFrame:((CGRect) {
                    location,
                    { kLPCMinimumTextWidth, 0. }
                })];

                [self.overlay addSubview:textBox];
                [self setTextBox:textBox];

                [textBox mapKeyPath:@Keypath(textBox.editingAttributes)](self);

                self.textOrigin = location;
            }
        } break;
        case UIGestureRecognizerStateChanged    : {
            if ([self textBox]) {
                location.y = (self.textBox.frame.origin.y
                              +self.textBox.minimumHeight);

                [self.textBox setFrame:
                 CGRectIntersection(
                    CGRectFromPoints(self.textOrigin, location),
                        self.overlay.bounds)];
            }
        } break;
        case UIGestureRecognizerStateCancelled  :
        case UIGestureRecognizerStateEnded      : {
            CGRect frame = [self.textBox frame];

            if (isless(frame.size.height, self.textBox.minimumHeight)) {
                frame.size.height   = self.textBox.minimumHeight;
            }

            if (isless(frame.size.width, kLPCMinimumTextWidth)) {
                frame.size.width    = kLPCMinimumTextWidth;
            }

            [UIView animateWithDuration:.3 animations:^{
                [self.textBox setFrame:CGRectClampToRect(frame, self.overlay.bounds)];
            } completion:^(BOOL finished) {
                [self.textBox becomeFirstResponder];

                if ([self.delegate respondsToSelector:@selector(pageContent:didMoveTextToRect:)]) {
                    [self.delegate pageContent:self didMoveTextToRect:self.textBox.textRect];
                }
            }];
        } break;
        default: {
            
        } break;
    }
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Protected Implementations
#pragma mark -
//*********************************************************************************************************************//




@implementation LPageContent (Text_Properties)

@implement(LPageContent, textOrigin, CGPoint, nonatomic, readwrite, assign);
@implement(LPageContent, textBox, LTextBox *, nonatomic, readwrite, weak);
@implement(LPageContent, textContentRecognizers, NSMutableArray *, nonatomic, readwrite, strong);
@implement(LPageContent, editingAttributes, NSDictionary *, nonatomic, readwrite, strong);

@end


@implementation LPageContent (Text_LTextBoxDelegate)

- (void)textBoxDidBeginEditing:(LTextBox *)textBox
{
    if ([self.delegate respondsToSelector:@selector(pageContent:didBeginEditingInRect:)]) {
        [self.delegate pageContent:self didBeginEditingInRect:textBox.textRect];
    }
}

- (void)textBoxDidEndEditing:(LTextBox *)textBox
{
    for (UIView *subview in self.overlay.subviews) {
        if ([subview isKindOfClass:[LTextBox class]]) {
            [subview removeFromSuperview];
        }
    }

    [self.page.currentAnnotator setText:textBox.attributedText inRect:textBox.textRect];

    if ([self.delegate respondsToSelector:@selector(pageContent:didEndEditingInRect:)]) {
        [self.delegate pageContent:self didEndEditingInRect:textBox.textRect];
    }

    [self setTextBox:nil];
}

- (void)textBoxDidChangeText:(LTextBox *)textBox
{
    if ([self.delegate respondsToSelector:@selector(pageContent:isEditingText:inRect:)]) {
        [self.delegate pageContent:self isEditingText:textBox.attributedText inRect:textBox.textRect];
    }
}

- (void)textBoxDidChangeTextRect:(LTextBox *)textBox
{
    if ([self.delegate respondsToSelector:@selector(pageContent:didMoveTextToRect:)]) {
        [self.delegate pageContent:self didMoveTextToRect:textBox.textRect];
    }
}

@end
