//
//  LGenericTestObject.h
//  Writeability
//
//  Created by Ryan Blonna on 16/1/15.
//  Copyright (c) 2015 CNL. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface LGenericTestObject : NSObject

@property (nonatomic, readwrite, assign) Class                  type;
@property (nonatomic, readwrite, assign) NSUInteger             integer;
@property (nonatomic, readwrite, strong) NSString               *string;
@property (nonatomic, readwrite, assign) CGRect                 rect;
@property (nonatomic, readwrite, strong) NSArray                *array;
@property (nonatomic, readwrite, strong) NSMutableDictionary    *mutableDictionary;
@property (nonatomic, readwrite, strong) NSData                 *data;
@property (nonatomic, readwrite, strong) NSInvocation           *invocation;

@property (nonatomic, readwrite, strong) id                     nilObject;
@property (nonatomic, readwrite, strong) UIView                 *view;

@property (nonatomic, readonly, strong) id readonly;

@end
