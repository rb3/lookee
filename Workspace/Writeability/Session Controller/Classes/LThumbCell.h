//
//  LThumbCell.h
//  Writeability
//
//  Created by Ryan on 6/3/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface LThumbCell : UICollectionViewCell

@property (nonatomic, readwrite , assign) NSUInteger    index;

@property (nonatomic, readwrite , strong) NSString      *labelText;
@property (nonatomic, readwrite , strong) NSString      *detailText;
@property (nonatomic, readwrite , strong) UIImage       *thumbnail;

@property (nonatomic, readwrite , copy  ) void(^selectBlock)(void);

@end
