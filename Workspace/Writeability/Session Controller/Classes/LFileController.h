//
//  LFileController.h
//  Writeability
//
//  Created by Ryan on 6/4/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <UIKit/UIKit.h>



@class      LNDocument;
@protocol   LFileControllerDelegate;



@interface LFileController : UICollectionViewController

@property (nonatomic, readwrite , strong) NSString                      *titleText;

@property (nonatomic, readwrite , weak  ) id<LFileControllerDelegate>   delegate;

@end


@protocol LFileControllerDelegate <NSObject>
@required

- (void)fileController:(LFileController *)controller didSelectDocument:(LNDocument *)file;
- (void)fileControllerWasDismissed:(LFileController *)controller;

@end