//
//  LFileController.m
//  Writeability
//
//  Created by Ryan on 6/4/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LFileController.h"

#import "LConstants.h"

#import "LStickyHeaderLayout.h"

#import "LThumbCell.h"

#import "LFileHeaderView.h"

#import <Networking/LNetworking.h>
#import <Networking/LNDocument.h>
//#import <Networking/LNThumb.h>

#import <Utilities/LMacros.h>

#import <Utilities/NSObject+Utilities.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Private Classes
#pragma mark -
//*********************************************************************************************************************//




@interface LFileThumbCell : LThumbCell

//@property (nonatomic, readwrite, weak) LNThumb *fileThumb;

@end

@implementation LFileThumbCell

- (instancetype)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) {
        [self.layer setBorderColor:[UIColor LTealColor].CGColor];
        [self.layer setBorderWidth:1.];
    }

    return self;
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LFileController
#pragma mark -
//*********************************************************************************************************************//




@interface LFileController () <UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate>

@property (nonatomic, readwrite , weak  ) UIGestureRecognizer   *dismissRecognizer;

@property (nonatomic, readwrite , weak  ) LFileHeaderView       *header;

@property (nonatomic, readwrite , strong) NSArray               *files;
@property (nonatomic, readwrite , strong) NSArray               *thumbs;

@end

@implementation LFileController

#pragma mark - Static Objects
//*********************************************************************************************************************//

StringConst(kLFCCellReuseIdentifier);
StringConst(kLFCHeaderReuseIdentifier);

static CGFloat  const   kLFCHeaderHeight            = 24.;
static CGFloat  const   kLFCMargin                  = 16.;


#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (NSSet *)validFileTypes
{
    static NSSet            *types;
    static dispatch_once_t  token;

    dispatch_once(&token, ^{
        types = [NSSet setWithArray:
                 (@[ @"pdf" ])];
    });

    return types;
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)init
{
    if ((self = [super initWithCollectionViewLayout:[LStickyHeaderLayout new]])) {
        [self setModalPresentationStyle:UIModalPresentationFormSheet];
        [self setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    }

    return self;
}

- (void)dealloc
{
    DBGMessage(@"deallocated %@", self);
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self setClearsSelectionOnViewWillAppear:YES];

    [self.collectionView setBackgroundColor:[UIColor whiteColor]];
    [self.collectionView setAlwaysBounceVertical:YES];
    
    [[self collectionView]
     registerClass              :[LFileThumbCell class]
     forCellWithReuseIdentifier :kLFCCellReuseIdentifier];

    [[self collectionView]
     registerClass              :[LFileHeaderView class]
     forSupplementaryViewOfKind :UICollectionElementKindSectionHeader
     withReuseIdentifier        :kLFCHeaderReuseIdentifier];

    [self startMonitoringFiles];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    UITapGestureRecognizer *recognizer = [UITapGestureRecognizer new];

    [recognizer setCancelsTouchesInView:NO];
    [recognizer setDelegate:self];

    [recognizer addTarget:self action:@selector(tapGestureRecognized:)],
    [self.class addMethodWithSelector:@selector(tapGestureRecognized:) withBlockImplementation:

     ^(typeof(self) self, UITapGestureRecognizer *recognizer) {
         [self dismissWithCompletion:^{
             [self.delegate fileControllerWasDismissed:self];
         }];
     }
     
     ];

    [self.view.window addGestureRecognizer:recognizer];

    [self setDismissRecognizer:recognizer];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    [self.view.window removeGestureRecognizer:self.dismissRecognizer];
}


#pragma mark - Properties
//*********************************************************************************************************************//

@synthesize titleText = _titleText;

- (void)setTitleText:(NSString *)titleText
{
    {_titleText = titleText;}
    
    if ([self header]) {
        [self.header.titleLabel setText:titleText];
    }
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (void)startMonitoringFiles
{
    void(^updateController)(void);

    $weakify(self) {
        updateController = ^{
            $strongify(self) {
                [self updateFiles];
                [self.collectionView reloadData];
            }
        };
    }

    updateController();

    [self observerForNotificationKey:kLNetworkingUpdatedFileAnnouncementNotification](updateController);
}

- (void)updateFiles
{
    NSMutableArray *documents   = [NSMutableArray new];
    NSMutableArray *thumbs  = [NSMutableArray new];

    id comparator = ^(LNDocument *document, LNDocument *other) {
        return [document.fileName compare:other.fileName];
    };

    for (LNDocument *document in [[LNetworking documents] sortedArrayUsingComparator:comparator]) {
        if ([self.class.validFileTypes containsObject:document.fileName.pathExtension]) {
            //LNThumb *thumb = [LNThumb thumbWithFile:file];

            [documents addObject:document];
            //[thumbs addObject:thumb];
        }
    }

    [self setFiles:documents];
    //[self setThumbs:thumbs];
}

- (void)dismissWithCompletion:(void(^)(void))completion
{
    if ([self.navigationController topViewController] == self) {
        [UIView animateWithDuration:.3 animations:^{
            [self.navigationController popViewControllerAnimated:YES];
        } completion:^(BOOL finished) {
            completion();
        }];
    } else {
        [self.presentingViewController dismissViewControllerAnimated:YES completion:completion];
    }
}


#pragma mark - UICollectionViewDataSource
//*********************************************************************************************************************//

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.files count];
}

- (UICollectionViewCell *)
collectionView          :(UICollectionView *)collectionView
cellForItemAtIndexPath  :(NSIndexPath *     )indexPath
{
    NSUInteger      index   = [indexPath item];

    LNDocument  *document   = [self files][ index ];
    //LNThumb     *thumb  = [self thumbs][ index ];

    LFileThumbCell  *cell   = [self.collectionView dequeueReusableCellWithReuseIdentifier:kLFCCellReuseIdentifier
                                                                             forIndexPath:indexPath];

    NSDateFormatter *formatter = [NSDateFormatter new];

    [formatter setDateFormat:@"hh:mm:ss MMM dd yyyy"];

    [cell setIndex:index];
    //[cell setFileThumb:thumb];
    [cell setLabelText:document.fileName.stringByDeletingPathExtension];
    [cell setDetailText:[formatter stringFromDate:document.dateCreated]];

    $weakify(self) {
        [cell setSelectBlock:^{
            $strongify(self) {
                [self.delegate fileController:self didSelectDocument:document];
            }
        }];
    }
    /*
    $weakify(cell) {
        [thumb loadImage:^(UIImage *image) {
            $strongify(cell) {
                if ([cell index] == index) {
                    [cell setThumbnail:image];
                }
            }
        }];
    }
     */
    return cell;
}

- (UICollectionReusableView *)
collectionView                      :(UICollectionView *)collectionView
viewForSupplementaryElementOfKind   :(NSString *        )kind
atIndexPath                         :(NSIndexPath *     )indexPath
{
    if (kind == UICollectionElementKindSectionHeader) {
        LFileHeaderView *header = [self.collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                                          withReuseIdentifier:kLFCHeaderReuseIdentifier
                                                                                 forIndexPath:indexPath];

        [header.titleLabel setText:self.titleText];

        $weakify(self) {
            [header setDismissBlock:^{
                $strongify(self) {
                    [self dismissWithCompletion:^{
                        [self.delegate fileControllerWasDismissed:self];
                    }];
                }
            }];
        }

        [self setHeader:header];

        return header;
    }

    return nil;
}


#pragma mark - UICollectionViewDelegate
//*********************************************************************************************************************//

- (void)
collectionView      :(UICollectionView *)collectionView
didEndDisplayingCell:(LFileThumbCell *  )cell
forItemAtIndexPath  :(NSIndexPath *     )indexPath
{
    //[cell.fileThumb pauseLoadImage];
}


#pragma mark - UICollectionViewDelegateFlowLayout
//*********************************************************************************************************************//

- (CGSize)
collectionView                  :(UICollectionView *        )collectionView
layout                          :(UICollectionViewLayout *  )collectionViewLayout
referenceSizeForHeaderInSection :(NSInteger                 )section
{
    return [LFileHeaderView size];
}

- (CGSize)
collectionView          :(UICollectionView *        )collectionView
layout                  :(UICollectionViewLayout *  )collectionViewLayout
sizeForItemAtIndexPath  :(NSIndexPath *             )indexPath
{
    //return [LNThumb size];
    return ((CGSize){144., 144.});
}

- (UIEdgeInsets)
collectionView          :(UICollectionView *        )collectionView
layout                  :(UICollectionViewLayout *  )collectionViewLayout
insetForSectionAtIndex  :(NSInteger                 )section
{
    return ((UIEdgeInsets){kLFCMargin, kLFCMargin, kLFCMargin, kLFCMargin});
}

- (CGFloat)
collectionView                          :(UICollectionView *        )collectionView
layout                                  :(UICollectionViewLayout *  )collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger                 )section
{
    return kLFCMargin;
}

- (CGFloat)
collectionView                      :(UICollectionView *        )collectionView
layout                              :(UICollectionViewLayout *  )collectionViewLayout
minimumLineSpacingForSectionAtIndex :(NSInteger                 )section
{
    return kLFCMargin;
}


#pragma mark - UIGestureRecognizerDelegate
//*********************************************************************************************************************//

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if (gestureRecognizer != [self dismissRecognizer]) {
        return YES;
    }

    BOOL recognize = ![[self view]
                       pointInside:[[self view]
                                    convertPoint:[touch locationInView:nil]
                                    fromView    :[self.view window]]
                       withEvent  :nil];

    return recognize;
}

@end
