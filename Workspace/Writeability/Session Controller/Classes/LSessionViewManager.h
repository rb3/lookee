//
//  LSessionViewManager.h
//  Writeability
//
//  Created by Ryan on 7/5/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface LSessionViewManager : UINavigationController

+ (instancetype)manager;


- (void)displayNotificationWithMessage:(NSString *)message;

@end
