//
//  LSessionController.m
//  Writeability
//
//  Created by Ryan on 6/3/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LSessionController.h"

#import "LConstants.h"

#import "LSessionDelegate.h"

#import "LDocument.h"

#import "LListView.h"

#import "LPopupView.h"

#import "LButton.h"

#import "LThumbCell.h"

#import "LSessionViewManager.h"

#import "LFileController.h"

#import "LPopoverController.h"

#import "LDocumentContent.h"

#import <Networking/LNetworking.h>
#import <Networking/LNDocument.h>
#import <Networking/LNUser.h>

#import <Utilities/LMacros.h>

#import <Utilities/NSObject+Utilities.h>
#import <Utilities/NSThread+Block.h>
#import <Utilities/NSOperationQueue+Utilities.h>
#import <Utilities/UIView+Utilities.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Private Classes
#pragma mark -
//*********************************************************************************************************************//




@interface LSessionCell : LThumbCell

@property (nonatomic, readwrite , weak) LNSession *session;

@end

@implementation LSessionCell
@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LSessionController
#pragma mark -
//*********************************************************************************************************************//




@interface LSessionController ()
<
    UICollectionViewDelegateFlowLayout,
    UITextFieldDelegate,
    LFileControllerDelegate,
    LListViewDelegate
>

@property (nonatomic, readwrite , strong) id<LSessionDelegate>  sessionDelegate;

@property (nonatomic, readwrite , strong) NSArray               *sessions;

@property (nonatomic, readwrite , strong) NSArray               *participants;

@property (nonatomic, readwrite , weak  ) LButton               *backButton;

@property (nonatomic, readwrite , weak  ) LListView             *participantList;

@property (nonatomic, readwrite , strong) LPopupView            *popup;

@property (nonatomic, readwrite , strong) LPopoverController    *popover;

@property (nonatomic, readwrite , weak  ) LButton               *softLockButton;

@property (nonatomic, readwrite , weak  ) LDocumentContent      *content;

@property (nonatomic, readwrite , weak  ) UIImageView           *connectivityIndicator;

@property (nonatomic, readwrite , weak  ) LNUser                *viewingParticipant;

@end

@interface LSessionController (Setup)

- (void)setupNavigationBar;
- (void)setupContent;

@end

@implementation LSessionController

#pragma mark - Static Objects
//*********************************************************************************************************************//

static StringConst(kLSessionCellReuseIdentifier);

static NSString *const  kLSessionControllerTitle        = @"ENTER A CLASS";
static NSString *const  kLFileControllerTitle           = @"BEGIN A CLASS";

static CGFloat  const   kLSessionControllerMargin       = 16.;

static CGFloat  const   kLSessionControllerStartDelay   = 1.;


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)init
{
    if (self = [super initWithCollectionViewLayout:[UICollectionViewFlowLayout new]]) {

    }

    return self;
}

- (void)dealloc
{
    DBGMessage(@"deallocated %@", self);
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self setClearsSelectionOnViewWillAppear:YES];

    [self.collectionView setBackgroundColor:[UIColor LBorderColor]];
    [self.collectionView setAlwaysBounceVertical:YES];

    [self.collectionView registerClass:[LSessionCell class] forCellWithReuseIdentifier:kLSessionCellReuseIdentifier];

    [self setupNavigationBar];

    [self startMonitoringSessions];
    [self startMonitoringParticipants];

    [$mainthread performBlock:^{[LNetworking start];} afterDelay:kLSessionControllerStartDelay];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [[LNetworking session] leave];
}

- (void)viewDidAppear:(BOOL)animated
{
    static BOOL authorized = NO;

    [super viewDidAppear:animated];

    [self setSessionDelegate:[LSessionDelegate new]];

#if !TEST && !ADHOC
    if (!authorized) {
        authorized = YES;

        [self authorizeApplication];
    }
#endif
}


#pragma mark - Properties
//*********************************************************************************************************************//

@dynamic locked;

- (BOOL)isLocked
{
    return [self softLockButton] && ![self.softLockButton isUserInteractionEnabled];
}

- (void)setLocked:(BOOL)locked
{
    if (locked) {
        [self setSoftLocked:YES];
    }

    [self.softLockButton setUserInteractionEnabled:!locked];
}

@dynamic softLocked;

- (BOOL)isSoftLocked
{
    return [self.softLockButton isSelected];
}

- (void)setSoftLocked:(BOOL)softLocked
{
    [self.softLockButton setSelected:softLocked];
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (void)authorizeApplication
{
    UIAlertView *authorizationAlert
    =
    [[UIAlertView alloc]
     initWithTitle      :@"Authorize P2P mode"
     message            :@"Please enter the authorization key:"
     delegate           :self
     cancelButtonTitle  :nil
     otherButtonTitles  :@"OK", nil];

    [authorizationAlert setAlertViewStyle:UIAlertViewStylePlainTextInput];

    [authorizationAlert show];
}

- (void)startMonitoringSessions
{
    void(^updateController)(void);

    $weakify(self) {
        updateController = ^{
            $strongify(self) {
                [$mainqueue addSerialOperationWithBlock:^{
                    [self updateSessions];
                    [self.collectionView reloadData];
                }];
            }
        };

        updateController();

        [self observerForNotificationKey:kLNetworkingSessionWasLeftNotification](updateController);
        [self observerForNotificationKey:kLNetworkingLostSessionNotification](updateController);
        [self observerForNotificationKey:kLNetworkingFoundSessionNotification](updateController);

        [self observerForNotificationKey:kLNSessionFinishedUserSynchronizeNotification]
        (^(NSNotification *notification) {
            $strongify(self) {
                [$mainqueue addSerialOperationWithBlock:^{
                    [self showContentAtPath:[(LNDocument *)notification.userInfo[kLNSessionSynchronizeFileKey] fullPath]];
                }];
            }
        });
    }
}

- (void)startMonitoringParticipants
{
    $weakify(self) {
        [self observerForNotificationKey:kLNSessionJoinedUserNotification] (^{
            $strongify(self) {
                if (LNetworkingIsCurrentDeviceSessionHost()) {
                    [$mainqueue addSerialOperationWithBlock:^{
                        [self updateParticipants];

                        [self.participantList reload];
                    }];
                }
            }
        });

        [self observerForNotificationKey:kLNSessionLeftUserNotification] (^{
            $strongify(self) {
                if (LNetworkingIsCurrentDeviceSessionHost()) {
                    [$mainqueue addSerialOperationWithBlock:^{
                        [self updateParticipants];

                        [self.participantList reload];
                    }];
                }
            }
        });
    }
}

- (void)beginUpdatingConnectivityIndicator
{
    NSArray *images =
    (@[
      [UIImage imageNamed:@"high_connection"    ],
      [UIImage imageNamed:@"medium_connection"  ],
      [UIImage imageNamed:@"low_connection"     ],
      [UIImage imageNamed:@"no_connection"      ]
      ]);

    UIImage *image  = [images lastObject];

    CGRect  frame   = {{0}, [image size]};

    [self.connectivityIndicator setFrame:frame];
    [self.connectivityIndicator setNeedsLayout];

    [self.connectivityIndicator setAnimationImages:images];
    [self.connectivityIndicator setAnimationDuration:1.];

    [self.connectivityIndicator startAnimating];

    [self observerForNotificationKey:kLNSessionConnectivityChangedNotification]
    (^(NSNotification *notification) {
        NSString *state = notification.userInfo[kLNSessionConnectivityKey];

        [self.connectivityIndicator stopAnimating];

        $switch(state) {
                $case(kLNSessionConnectivityGood): {
                    [self.connectivityIndicator setImage:images[0]];
                }
                $case(kLNSessionConnectivityModerate): {
                    [self.connectivityIndicator setImage:images[1]];
                }
                $case(kLNSessionConnectivityPoor): {
                    [self.connectivityIndicator setImage:images[2]];
                }
                $case(kLNSessionConnectivityNone): {
                    [self.connectivityIndicator setImage:images[3]];
                }
                $default: {
                    [self.connectivityIndicator setAnimationImages:images];
                    [self.connectivityIndicator setAnimationDuration:1.];
                }
        }
    });
}

- (void)updateSessions
{
    NSMutableArray *sessions = [NSMutableArray new];

    for (LNSession *session in [LNetworking sessions]) {
        [session generateThumbnail];
        [sessions addObject:session];
    }

    [self setSessions:sessions];
}

- (void)updateParticipants
{
    NSMutableArray *participants = [NSMutableArray new];

    for (LNUser *participant in LNetworkingCurrentSessionUsers()) {
        [participants addObject:participant];
    }

    [self setParticipants:participants];
}

- (void)showFileController
{
    LFileController *fileController = [LFileController new];

    [fileController setDelegate:self];
    [fileController setTitleText:kLFileControllerTitle];

    [self presentViewController:fileController animated:YES completion:nil];
}

- (void)hideFileControllerWithCompletion:(void(^)(void))completion
{
    [self dismissViewControllerAnimated:YES completion:completion];
}

- (void)showContentAtPath:(NSString *)path
{
    LDocument           *document   = [LDocument documentForURL:[NSURL fileURLWithPath:path] scale:$screenscale];

    LDocumentContent    *content    = [[LDocumentContent alloc] initWithDocument:document];

    [content setDelegate:self.sessionDelegate];

    [self.sessionDelegate sessionController:self willBeginSessionWithContent:content];

    [UIView animateWithDuration:.3 animations:^{
        [self setContent:content];
        [self setupContent];

        [self beginUpdatingConnectivityIndicator];

        [self.navigationController pushViewController:content animated:YES];
    } completion:^(BOOL finished) {
        [self.sessionDelegate sessionController:self didBeginSessionWithContent:content];
    }];
}

- (void)showParticipantListFromButton:(LButton *)button
{
    static CGRect frame = {
        .origin = {0},
        .size   = {
            .width  = 256.,
            .height = 512.
        }
    };

    if (![self popup]) {
        LListView *listView = [[LListView alloc] initWithFrame:frame];
        [listView setDelegate:self];

        [self setPopup:[LPopupView new]];
        [self.popup setBackgroundColor:[UIColor whiteColor]];
        [self.popup setBorderColor:[UIColor LTealColor]];
        [self.popup setHasShadow:YES];
        [self.popup setShouldDismissOnTapAnywhere:YES];
        [self.popup setCustomView:listView];

        [self setParticipantList:listView];
    }

    [self.popup presentPointingAtView:button inView:self.parentViewController.view animated:YES];
}

- (void)startViewingParticipant:(LNUser *)participant
{
    if ([self viewingParticipant] != participant) {
        [self setViewingParticipant:participant];

        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.35* NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [UIView setAnimationsEnabled:NO];

            [self.sessionDelegate sessionController:self toggleViewParticipantWithID:participant.ID];

            [self.participantList reloadItemAtIndex:[self.participants indexOfObject:participant] animated:NO];

            [self.content setInteractionEnabled:NO];

            [self.content.navigationItem setTitle:@Join(@"Viewing: ", participant.name)];
            [self.backButton setTitle:@"unview" forState:UIControlStateNormal];

            [UIView setAnimationsEnabled:YES];
        });

        [UIView
         transitionWithView :self.parentViewController.view
         duration           :.7
         options            :UIViewAnimationOptionTransitionFlipFromLeft
         animations         :nil
         completion         :nil];
    }
}

- (void)stopViewingParticipant
{
    if ([self viewingParticipant]) {
        LNUser *participant = [self viewingParticipant];

        [self setViewingParticipant:nil];

        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.35* NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [UIView setAnimationsEnabled:NO];

            [self.sessionDelegate sessionController:self toggleViewParticipantWithID:participant.ID];

            [self.participantList reloadItemAtIndex:[self.participants indexOfObject:participant] animated:NO];

            [self.content setInteractionEnabled:YES];

            [self.content.navigationItem setTitle:LNetworkingCurrentSessionName()];
            [self.backButton setTitle:@"classes" forState:UIControlStateNormal];

            [UIView setAnimationsEnabled:YES];
        });

        [UIView
         transitionWithView :self.parentViewController.view
         duration           :.7
         options            :UIViewAnimationOptionTransitionFlipFromRight
         animations         :nil
         completion         :nil];
    }
}


#pragma mark - LListViewDelegate
//*********************************************************************************************************************//

- (NSUInteger)itemCountForListView:(LListView *)listView
{
    LButton *$sub(newButtonWithTitle, NSString *title) {
        LButton *button = [LButton new];

        [button setBackgroundColor:[self.participants count]? [UIColor LTealColor]: [UIColor LGrayColor]];
        [button setTitle:title forState:UIControlStateNormal];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [button setRightBorderColor:[UIColor whiteColor]];
        [button setRightBorderWidth:1.];
        [button setLeftBorderColor:[UIColor whiteColor]];
        [button setLeftBorderWidth:1.];

        return button;
    };

    NSString *$sub(sessionInfoLabelText, void) {
        NSUInteger count = [self.participants count];

        return @Format(@"%ld participant%s", (unsigned long)count, count != 1? "s": "");
    };

    [listView setFooterAttributes   :
     (@{@"textLabel.textColor"      :[UIColor LTealColor],
        @"textLabel.font"           :[[UIFont LDefaultFont] fontWithSize:16.],
        @"textLabel.text"           :sessionInfoLabelText()})];

    [listView setHeaderAttributes   :
     (@{@"textLabel.textColor"      :[UIColor LTealColor],
        @"textLabel.font"           :[[UIFont LDefaultFont] fontWithSize:20.],
        @"textLabel.text"           :(@"Participants"),
        @"rightUtilityButtons"      :@[newButtonWithTitle(@"unlock"), newButtonWithTitle(@"lock")]})];

    return [self.participants count];
}

- (NSUInteger)listView:(LListView *)listView optionCountForItemAtIndex:(NSUInteger)index
{
    return 2;
}

- (NSDictionary *)listView:(LListView *)listView attributesForItemAtIndex:(NSUInteger)index
{
    NSMutableDictionary *attributes = [NSMutableDictionary new];
    {
        id participant = [self participants][index];

        id textLabelAttributes
        =
        (@{@"textLabel.textColor"   :[UIColor LTealColor],
           @"textLabel.font"        :[[UIFont LDefaultFont] fontWithSize:18.],
           @"textLabel.text"        :[participant name]?:(@"<unknown>"),
           });

        id detailTextLabelAttributes
        =
        (@{@"detailTextLabel.textColor" :[UIColor LGrayColor],
           @"detailTextLabel.font"      :[[UIFont LDefaultFont] fontWithSize:12.],
           @"detailTextLabel.text"      :({
                NSMutableArray *states = [NSMutableArray new];

                [states addObject:[participant isLocked]? (@"locked"): (@"unlocked")];

                if ([self viewingParticipant] == participant) {
                    [states addObject:@"viewing"];
                }

                [states componentsJoinedByString:@", "];
            }),
           });

        [attributes addEntriesFromDictionary:textLabelAttributes];
        [attributes addEntriesFromDictionary:detailTextLabelAttributes];
    }

    return attributes;
}

- (NSDictionary *)listView:(LListView *)listView attributesForOptionAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *attributes = [NSMutableDictionary new];
    {
        id participant = [self participants][indexPath.itemIndex];

        id textLabelAttributes;

        switch ([indexPath optionIndex]) {
            case 0: {
                textLabelAttributes
                =
                (@{@"textLabel.textColor"   :[UIColor LTealColor],
                   @"textLabel.font"        :[[UIFont LDefaultFont] fontWithSize:16.],
                   @"textLabel.text"        :[participant isLocked]? (@"unlock"): (@"lock")
                   });
            } break;
            default: {
                textLabelAttributes
                =
                (@{@"textLabel.textColor"   :[UIColor LTealColor],
                   @"textLabel.font"        :[[UIFont LDefaultFont] fontWithSize:16.],
                   @"textLabel.text"        :[self viewingParticipant] == participant? (@"unview"): (@"view")
                   });
            } break;
        }

        [attributes addEntriesFromDictionary:textLabelAttributes];
    }
    
    return attributes;
}

- (void)listView:(LListView *)listView didSelectOptionAtIndexPath:(NSIndexPath *)indexPath
{
    id participant = [self participants][indexPath.itemIndex];

    switch ([indexPath optionIndex]) {
        case 0: {
            [self.sessionDelegate sessionController:self toggleLockParticipantWithID:[participant ID]];
        } break;
        default: {
            if (!LNetworkingIsUserLinkedToCurrentSession(participant)) {
                [self startViewingParticipant:participant];
            } else {
                [self stopViewingParticipant];
            }
        } break;
    }
}

- (void)listView:(LListView *)listView didSelectRightHeaderOptionAtIndex:(NSUInteger)index
{
    void $sub(setParticipantsLockState, BOOL locked) {
        NSUInteger index = 0;

        if ([self.participants count]) {
            [self.participantList hideHeaderOptionsAnimated:YES];
        }

        for (LNUser *participant in [self participants]) {
            if ([participant isLocked] != locked) {
                [self.sessionDelegate sessionController:self toggleLockParticipantWithID:participant.ID];

                [self.participantList reloadItemAtIndex:index animated:YES];
            }

            ++ index;
        }
    };

    switch (index) {
        case 0: {
            setParticipantsLockState(NO);
        } break;
        case 1: {
            setParticipantsLockState(YES);
        } break;
        default: {
        } break;
    }
}


#pragma mark - LFileControllerDelegate
//*********************************************************************************************************************//

- (void)fileController:(LFileController *)controller didSelectDocument:(LNDocument *)document
{
    NSString    *name       = [[document fileName] stringByDeletingPathExtension];
    LNSession   *session    = [LNetworking createSessionWithName:name fileID:document.ID];

    [self hideFileControllerWithCompletion:^{
        [session hostWithPassword:@"password"];
    }];
}

- (void)fileControllerWasDismissed:(LFileController *)controller
{
}


#pragma mark - UICollectionViewDataSource
//*********************************************************************************************************************//

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.sessions count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger          index       = [indexPath item];

    LNSession           *session    = [self sessions][ index ];

    LSessionCell        *cell       = [self.collectionView dequeueReusableCellWithReuseIdentifier:kLSessionCellReuseIdentifier
                                                                                     forIndexPath:indexPath];

    NSDateFormatter     *formatter  = [NSDateFormatter new];

    [formatter setDateFormat:@"hh:mm:ss MMM dd yyyy"];

    [cell setIndex:index];
    [cell setSession:session];
    [cell setLabelText:session.name];
    [cell setDetailText:[formatter stringFromDate:[NSDate date]]];

    $weakify(session) {
        [cell setSelectBlock:^{
            $strongify(session) {
                [session joinWithPassword:@"password"];
            }
        }];
    }

    $weakify(cell) {
        [session loadThumbnail:^(UIImage *image) {
            $strongify(cell) {
                [$mainqueue addSerialOperationWithBlock:^{
                    if ([cell index] == index) {
                        [cell setThumbnail:image];
                    }
                }];
            }
        }];
    }

    return cell;
}


#pragma mark - UICollectionViewDelegate
//*********************************************************************************************************************//

- (void)
collectionView      :(UICollectionView *)collectionView
didEndDisplayingCell:(LSessionCell *    )cell
forItemAtIndexPath  :(NSIndexPath *     )indexPath
{
    [cell.session stopLoadThumbnail];
}


#pragma mark - UICollectionViewDelegateFlowLayout
//*********************************************************************************************************************//

- (CGSize)
collectionView          :(UICollectionView *        )collectionView
layout                  :(UICollectionViewLayout *  )collectionViewLayout
sizeForItemAtIndexPath  :(NSIndexPath *             )indexPath
{
    //return [LNThumb size];
    return ((CGSize){144., 144.});
}

- (UIEdgeInsets)
collectionView          :(UICollectionView *        )collectionView
layout                  :(UICollectionViewLayout *  )collectionViewLayout
insetForSectionAtIndex  :(NSInteger                 )section
{
    return ((UIEdgeInsets){kLSessionControllerMargin, kLSessionControllerMargin, kLSessionControllerMargin, kLSessionControllerMargin});
}

- (CGFloat)
collectionView                          :(UICollectionView *        )collectionView
layout                                  :(UICollectionViewLayout *  )collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger                 )section
{
    return kLSessionControllerMargin;
}

- (CGFloat)
collectionView                      :(UICollectionView *        )collectionView
layout                              :(UICollectionViewLayout *  )collectionViewLayout
minimumLineSpacingForSectionAtIndex :(NSInteger                 )section
{
    return kLSessionControllerMargin;
}


#pragma mark - UIAlertViewDelegate
//*********************************************************************************************************************//

- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
    UITextField    *textField  = [alertView textFieldAtIndex:0];
    NSString       *validKey   = [NSString stringWithFormat:@"l00k33v%@", $versionstring];

    if ([textField.text isEqualToString:validKey]) {
        return YES;
    }

    return NO;
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Private Implementations
#pragma mark -
//*********************************************************************************************************************//




@implementation LSessionController (Setup)

#pragma mark - Public Methods
//*********************************************************************************************************************//

- (void)setupNavigationBar
{
#pragma message "FIXME: Bug in SWTableViewCell causes crash on iOS 6"
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(7.0)) {
        UIBarButtonItem *fileButton = [UIBarButtonItem new];
        [fileButton setTintColor:[UIColor LTealColor]];
        [fileButton setImage:[UIImage imageNamed:@"add_group"]];
        [fileButton setTarget:self];
        [fileButton setAction:@selector(showFileController)];

        [self.navigationItem setLeftBarButtonItem:fileButton];
    }

    [self.navigationItem setTitle:kLSessionControllerTitle];
}

- (void)setupContent
{
    NSMutableArray *rightUtilities  = [NSMutableArray new];

    [self setupQuitButton];

    if (LNetworkingIsCurrentDeviceSessionHost()) {
        [self setupContentAttendeesUtilities:rightUtilities];
    }


    NSMutableArray *leftItems       = [NSMutableArray new];
    NSMutableArray *rightItems      = [NSMutableArray new];


    [self setupContentConnectivityIndicatorItems:leftItems];
    [self setupContentLockItems:rightItems];

    [self.content setRightUtilityItems:rightUtilities];

    [self.content setLeftToolbarItems:leftItems];
    [self.content setRightToolbarItems:rightItems];
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (void)setupQuitButton
{
    [self.navigationItem setHidesBackButton:YES];

    id backItem     = [UIBarButtonItem new];

    id backButton   = [LButton button];

    [backButton setImage:[UIImage imageNamed:@"back-arrow"] forState:UIControlStateNormal];
    [backButton setTitle:@"classes" forState:UIControlStateNormal];
    [backButton setTitleColor:[UIColor LTealColor] forState:UIControlStateNormal];
    [[backButton titleLabel] setFont:[[UIFont LDefaultFont] fontWithSize:16.]];
    [backButton sizeToFit];

    SEL selector    = NSSelectorFromString(@"didPressBackButton");

    [backButton addTarget:self action:selector forControlEvents:UIControlEventTouchUpInside],
    [self.class addMethodWithSelector:selector withBlockImplementation:

     ^(typeof(self) self) {
         if ([self.content isInteractionEnabled]) {
             UIAlertView *alertView = [UIAlertView new];

             [alertView setTitle:@"Leave Session"];
             [alertView setMessage:@"Are you sure you want to leave this session?"];
             [alertView setCancelButtonIndex:0];
             [alertView addButtonWithTitle:@"YES"];
             [alertView addButtonWithTitle:@"NO"];
             [alertView setDelegate:self];
             
             [alertView show];
         } else {
             [self stopViewingParticipant];
         }
     }
     
     ];

    [backItem setCustomView:backButton];
    [self setBackButton:backButton];

    [self.content setLeftUtilityItems:@[backItem]];
}

- (void)setupContentAttendeesUtilities:(NSMutableArray *)utilities
{
    LButton *attendeesButton = [LButton button];
    [attendeesButton setNormalImage:[UIImage imageNamed:LC_ATTENDEES_ICON]];
    [attendeesButton setSelectedImage:[UIImage imageNamed:LC_ATTENDEES_TAP_ICON]];

    [attendeesButton
     addTarget          :self
     action             :@selector(tapAttendeesButton:)
     forControlEvents   :UIControlEventTouchUpInside];

    [utilities addObject:[[UIBarButtonItem alloc] initWithCustomView:attendeesButton]];
}

- (void)setupContentConnectivityIndicatorItems:(NSMutableArray *)items
{
    UIImageView     *indicator          = [UIImageView new];

    UIBarButtonItem *connectivityItem   = [[UIBarButtonItem alloc] initWithCustomView:indicator];

    [items addObject:connectivityItem];

    [self setConnectivityIndicator:indicator];

    if (LNetworkingIsCurrentDeviceSessionHost()) {
        [indicator setHidden:YES];
    }
}

- (void)setupContentLockItems:(NSMutableArray *)items
{
    LButton *softLockButton = [LButton button];
    [softLockButton setNormalImage:[UIImage imageNamed:LC_SOFT_LOCK_ICON]];
    [softLockButton setSelectedImage:[UIImage imageNamed:LC_SOFT_LOCK_TAP_ICON]];

    [softLockButton
     addTarget          :self
     action             :@selector(tapSoftLockButton:)
     forControlEvents   :UIControlEventTouchUpInside];

    UIBarButtonItem *softLockItem = [[UIBarButtonItem alloc] initWithCustomView:softLockButton];

    [items addObject:softLockItem];

    [self setSoftLockButton:softLockButton];

    if (LNetworkingIsCurrentDeviceSessionHost()) {
        [softLockButton setHidden:YES];
    }
}


#pragma mark - UIButton Callbacks
//*********************************************************************************************************************//

- (void)tapSoftLockButton:(LButton *)button
{
    [self setSoftLocked:![self isSoftLocked]];
}

- (void)tapAttendeesButton:(LButton *)button
{
    [self showParticipantListFromButton:button];
}


#pragma mark - UIAlertView Callbacks
//*********************************************************************************************************************//

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != 1) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

@end
