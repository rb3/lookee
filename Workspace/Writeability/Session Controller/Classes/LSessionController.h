//
//  LSessionController.h
//  Writeability
//
//  Created by Ryan on 6/3/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <UIKit/UIKit.h>



@class LDocumentContent;

@protocol LSessionDelegate;



@interface LSessionController : UICollectionViewController

@property (nonatomic, readwrite, assign, getter = isLocked      ) BOOL locked;

@property (nonatomic, readwrite, assign, getter = isSoftLocked  ) BOOL softLocked;

@end


@protocol LSessionDelegate <NSObject>
@required

- (void)sessionController:(LSessionController *)controller willBeginSessionWithContent:(LDocumentContent *)content;
- (void)sessionController:(LSessionController *)controller didBeginSessionWithContent:(LDocumentContent *)content;

- (void)sessionController:(LSessionController *)controller toggleLockParticipantWithID:(NSString *)ID;

- (void)sessionController:(LSessionController *)controller toggleViewParticipantWithID:(NSString *)ID;

@end
