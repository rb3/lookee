//
//  LFileHeaderView.h
//  Writeability
//
//  Created by Ryan on 6/4/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface LFileHeaderView : UICollectionReusableView

@property (nonatomic, readonly  , weak) UILabel *titleLabel;

@property (nonatomic, readwrite , copy) void(^dismissBlock)(void);


+ (CGSize)size;

@end
