//
//  LAppearanceConstants.m
//  Writeability
//
//  Created by Jelo Agnasin on 10/1/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//

#import "LAppearanceConstants.h"

// categories
#import <Utilities/UIColor+Utilities.h>


//*********************************************************************************************************************//
#pragma mark -
#pragma mark Constants
#pragma mark -
//*********************************************************************************************************************//

const CGFloat LC_DEFAULT_MARGIN = 12.0;
const CGFloat LC_DEFAULT_SPACING = 8.0;


//*********************************************************************************************************************//
#pragma mark -
#pragma mark Icons
#pragma mark -
//*********************************************************************************************************************//

NSString *const LC_ATTENDEES_ICON = @"attendees";
NSString *const LC_ATTENDEES_TAP_ICON = @"attendees-tap";

NSString *const LC_CLEARPAGE_ICON = @"clear-page";
NSString *const LC_CLEARPAGE_TAP_ICON = @"clear-page-tap";

NSString *const LC_ENTER_PANEL_ICON = @"enter-panel";
NSString *const LC_ENTER_PANEL_TAP_ICON = @"enter-panel-tap";

NSString *const LC_ERASER_ICON = @"eraser";
NSString *const LC_ERASER_TAP_ICON = @"eraser-tap";

NSString *const LC_EXPORT_ICON = @"export";
NSString *const LC_EXPORT_TAP_ICON = @"export-tap";

NSString *const LC_HILITER_ICON = @"highlighter";
NSString *const LC_HILITER_TAP_ICON = @"highlighter-tap";

NSString *const LC_LEFT_PANEL_ICON = @"left-panel";
NSString *const LC_LEFT_PANEL_TAP_ICON = @"left-panel-tap";

NSString *const LC_MAGNIFY_PLUS_ICON = @"magnify-on";
NSString *const LC_MAGNIFY_PLUS_TAP_ICON = @"magnify-on-tap";

NSString *const LC_PEN_ICON = @"pen";
NSString *const LC_PEN_TAP_ICON = @"pen-tap";

NSString *const LC_REDO_ICON = @"redo";
NSString *const LC_REDO_TAP_ICON = @"redo-tap";

NSString *const LC_SELECTED_BACKGROUND_IMAGE = @"selected-background";

NSString *const LC_SETTINGS_ICON = @"settings";
NSString *const LC_SETTINGS_SELECTED_ICON = @"settings-menu";
NSString *const LC_SETTINGS_BACKGROUND_ICON = @"settings-menu-background";

NSString *const LC_SOFT_LOCK_ICON = @"locator";
NSString *const LC_SOFT_LOCK_TAP_ICON = @"locator-tap";

NSString *const LC_TEXT_ICON = @"text";
NSString *const LC_TEXT_TAP_ICON = @"text-tap";

NSString *const LC_UNDO_ICON = @"undo";
NSString *const LC_UNDO_TAP_ICON = @"undo-tap";

NSString *const LC_VISIBILITY_ICON = @"visibility";
NSString *const LC_VISIBILITY_TAP_ICON = @"visibility-tap";


//*********************************************************************************************************************//
#pragma mark -
#pragma mark Constants Colors
#pragma mark -
//*********************************************************************************************************************//

NSString *const LC_DEFAULT_TEAL_COLOR = @"3ac7d0";
NSString *const LC_DEFAULT_BORDER_COLOR = @"dcdcdc";
NSString *const LC_DEFAULT_GRAY_COLOR = @"bdbdbd";
NSString *const LC_DEFAULT_DARK_GRAY_COLOR = @"262626";

NSString *const LC_BLACK_COLOR = @"000000";
NSString *const LC_RED_COLOR = @"e0173d";
NSString *const LC_ORANGE_COLOR = @"f0991c";
NSString *const LC_YELLOW_COLOR = @"ffe614";
NSString *const LC_GREEN_COLOR = @"8fcd18";
NSString *const LC_BLUE_COLOR = @"1698cd";
NSString *const LC_VIOLET_COLOR = @"c900e7";
NSString *const LC_PINK_COLOR = @"ff0090";


//*********************************************************************************************************************//
#pragma mark -
#pragma mark UIFont + LAppearance
#pragma mark -
//*********************************************************************************************************************//

@implementation UIFont (LAppearance)

+ (UIFont *)LDefaultFont
{
    static NSString *const defaultFontName = @"Questrial-Regular";
    static const CGFloat defaultSize = 12.0;
    return [UIFont fontWithName:defaultFontName size:defaultSize];
}

@end


//*********************************************************************************************************************//
#pragma mark -
#pragma mark UIColor + LAppearance
#pragma mark -
//*********************************************************************************************************************//

@implementation UIColor (LAppearance)

+ (UIColor *)LTealColor { return [UIColor colorFromHexString:LC_DEFAULT_TEAL_COLOR]; }
+ (UIColor *)LBorderColor { return [UIColor colorFromHexString:LC_DEFAULT_BORDER_COLOR]; }
+ (UIColor *)LGrayColor { return [UIColor colorFromHexString:LC_DEFAULT_GRAY_COLOR]; }
+ (UIColor *)LDarkGrayColor { return [UIColor colorFromHexString:LC_DEFAULT_DARK_GRAY_COLOR]; }

+ (UIColor *)LBlackColor { return [UIColor colorFromHexString:LC_BLACK_COLOR]; }
+ (UIColor *)LRedColor { return [UIColor colorFromHexString:LC_RED_COLOR]; }
+ (UIColor *)LOrangeColor { return [UIColor colorFromHexString:LC_ORANGE_COLOR]; }
+ (UIColor *)LYellowColor { return [UIColor colorFromHexString:LC_YELLOW_COLOR]; }
+ (UIColor *)LGreenColor { return [UIColor colorFromHexString:LC_GREEN_COLOR]; }
+ (UIColor *)LBlueColor { return [UIColor colorFromHexString:LC_BLUE_COLOR]; }
+ (UIColor *)LVioletColor { return [UIColor colorFromHexString:LC_VIOLET_COLOR]; }
+ (UIColor *)LPinkColor { return [UIColor colorFromHexString:LC_PINK_COLOR]; }


@end

