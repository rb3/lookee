//
//  LConstants.h
//  Writeability
//
//  Created by Jelo Agnasin on 9/6/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "LAppearanceConstants.h"

// LC = Lookee Constants
extern NSString *const LC__BundleIdentifierDebug;
extern NSString *const LC__BundleIdentifierRelease;

extern const NSUInteger LC__MaximumDocumentDimension;
extern const NSUInteger LC__MinimumDocumentDimension;


// TODO: these should be changed to notifications
extern NSString *const LC_OPEN_IN_FILE;
extern NSString *const LC_SESSION_JOIN_URL;
extern NSString *const LC_SESSION_QUIT;


// LN = Lookee Notifications
extern NSString *const LN__ChangedUserProfile;
extern NSString *const LN__DownloadDocumentFailed;
extern NSString *const LN__UploadDocumentFailed;

