//
//  main.m
//  Writeability
//
//  Created by CNL on 3/31/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import <UIKit/UIKit.h>

#import "WApplicationDelegate.h"


int main(int argc, char *argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([WApplicationDelegate class]));
	}
}
