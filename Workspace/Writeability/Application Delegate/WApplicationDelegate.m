//
//  WApplicationDelegate.m
//  Writeability
//
//  Created by CNL on 7/12/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//

#import "WApplicationDelegate.h"

#import "LConstants.h"

#import <Utilities/LPath.h>

// view controllers
#import "LSessionViewManager.h"

#ifndef TEST
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <sys/xattr.h>
#endif

@interface WApplicationDelegate ()

@property (nonatomic, readwrite, assign) BOOL didFinishLaunching;

@property (nonatomic, readwrite, assign, getter = isServerMode) BOOL serverMode;

@property (nonatomic, readwrite, strong) UIViewController *controller;

@end

@implementation WApplicationDelegate

#pragma mark - Properties
//*********************************************************************************************************************//

@synthesize window;


#pragma mark - Initialize Document
//*********************************************************************************************************************//

- (void)initializeDocument
{
    // copy Lookee.pdf to documents directory if needed
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];

    NSArray *initialDocuments = @[@"Welcome.pdf",
                                  @"Graph Paper.pdf",
                                  @"Lined Paper.pdf"
                                  ];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];

    for (NSString *document in initialDocuments) {
        NSString *lookeePath = [documentsDirectory stringByAppendingPathComponent:document];

        if (![fileManager fileExistsAtPath:lookeePath]) {
            // doesn't exist! copy file
            NSError *error;
            NSString *lookeeBundlePath = [[NSBundle mainBundle].resourcePath stringByAppendingPathComponent:document];
            [fileManager copyItemAtPath:lookeeBundlePath toPath:lookeePath error:&error];

            if (error) {
                NSLog(@"[Error] %@", error);
            }
        }
    }
}


#pragma mark - UIApplicationDelegate
//*********************************************************************************************************************//

- (BOOL)application:(UIApplication *)application
			openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
		 annotation:(id)annotation
{
    if (url.isFileURL) {
        NSString *filename = [url lastPathComponent];
        NSString *documents = LPathDocuments();

        NSString *path = [documents stringByAppendingPathComponent:filename];

        if (![[NSFileManager defaultManager] fileExistsAtPath:path]) {
            if (![[NSFileManager defaultManager] moveItemAtPath:url.path toPath:path error:nil]) {
                return NO;
            }

            [[NSNotificationCenter defaultCenter]
             postNotificationName:LC_OPEN_IN_FILE
             object:self
             userInfo:@{@"url": [NSURL fileURLWithPath:path]}];
        }
        
        return YES;
    } else if ([url.scheme isEqualToString:@"lookee"]) {
        if ([url.host isEqualToString:@"session"] &&
            [url.pathComponents containsObject:@"join"]) {
            // open a session
            [[NSNotificationCenter defaultCenter]
             postNotificationName:LC_SESSION_JOIN_URL
             object:self
             userInfo:@{@"sessionID": url.lastPathComponent}];
        }
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.didFinishLaunching = YES;

    // initialize document
    [self initializeDocument];

    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
	self.window.backgroundColor = [UIColor whiteColor];

    UIViewController *rootViewController = [UIViewController new];

    [self.window setRootViewController:rootViewController];

    [self.window makeKeyAndVisible];
#ifndef TEST
    [Fabric with:@[CrashlyticsKit]];
#endif
    [self setController:[LSessionViewManager manager]];
    [rootViewController presentViewController:[self controller] animated:YES completion:nil];

	return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of
	// temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application
	// and it begins the transition to the background state. Use this method to pause ongoing tasks, disable timers,
	// and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
	// Use this method to release shared resources, save user data, invalidate timers, and store enough
	// application state information to restore your application to its current state in case it is terminated later.
	// If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
	// Called as part of transition from the background to the inactive state: here you can undo many
	// of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
	// Restart any tasks that were paused (or not yet started) while the application was inactive.
	// If the application was previously in the background, optionally refresh the user interface.

    if ([self didFinishLaunching]) {
        self.didFinishLaunching = NO;
    }
}

- (void)applicationWillTerminate:(UIApplication *)application
{
	// Called when the application is about to terminate.
	// See also applicationDidEnterBackground:.
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
	// Free up as much memory as possible by purging cached data objects that can be recreated
	// (or reloaded from disk) later.
}

@end
