//
//  LAnnotationPicker.m
//  Writeability
//
//  Created by Jelo Agnasin on 10/3/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//

#import "LAnnotationPicker.h"

#import "LConstants.h"

// custom views
#import "LColorPalette.h"
#import "LThicknessPalette.h"

// first-party
#import <QuartzCore/QuartzCore.h>


#pragma mark - LAnnotationPicker Interface
//*********************************************************************************************************************//

@interface LAnnotationPicker () <LColorPaletteDelegate, LThicknessPaletteDelegate>

@property (nonatomic, readwrite, strong) LColorPalette *colorPalette;
@property (nonatomic, readwrite, strong) LThicknessPalette *thicknessPalette;

@end


//*********************************************************************************************************************//
#pragma mark -
#pragma mark LAnnotationPicker
#pragma mark -
//*********************************************************************************************************************//

@implementation LAnnotationPicker


#pragma mark - Static Constants
//*********************************************************************************************************************//

static const CGFloat kWidth = 170.0;
static const CGFloat kHeight = 130.0;

static const NSInteger kRows = 2;
static const NSInteger kColumns = 4;

static const CGFloat kColorPaletteItemSize = 32.0;

static const CGFloat kThicknessPaletteWidth = 120.0;
static const CGFloat kThicknessPaletteHeight = 36.0;


#pragma mark - Memory Management
//*********************************************************************************************************************//

- (instancetype)init
{
    self = [super initWithFrame:CGRectMake(0, 0, kWidth, kHeight)];
    if (!self) return nil;
    
    self.backgroundColor = [UIColor whiteColor];
    
    self.colorPalette = [[LColorPalette alloc]
                         initWithColors :[LAnnotationPicker colors]
                         rows           :kRows
                         columns        :kColumns
                         size           :CGSizeMake(kColorPaletteItemSize, kColorPaletteItemSize)];
    self.colorPalette.delegate = self;
    [self addSubview:self.colorPalette];
    
    self.thicknessPalette = [LThicknessPalette new];
    self.thicknessPalette.delegate = self;
    [self addSubview:self.thicknessPalette];
    
    return self;
}


#pragma mark - Layout Subviews
//*********************************************************************************************************************//

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect frame;
    
    CGFloat paletteWidth = kColumns * kColorPaletteItemSize;
    CGFloat paletteHeight = kRows * kColorPaletteItemSize;
    
    frame.origin.x = (kWidth - paletteWidth) / 2.0;
    frame.origin.y = LC_DEFAULT_MARGIN;
    frame.size.width = paletteWidth;
    frame.size.height = paletteHeight;
    self.colorPalette.frame = frame;
    

    frame.origin.y = 1.5 * LC_DEFAULT_MARGIN + paletteHeight;
    frame.size.width = kThicknessPaletteWidth;
    frame.size.height = kThicknessPaletteHeight;
    self.thicknessPalette.frame = frame;
}


#pragma mark - Properties
//*********************************************************************************************************************//

- (void)setColor:(UIColor *)color
{
    if ([[LAnnotationPicker colors] containsObject:color]) {
        _color = color;
        self.colorPalette.selectedColor = color;
    }
}

- (void)setWeight:(float)weight
{
    if ([[LAnnotationPicker weights] containsObject:@(weight)]) {
        _weight = weight;
        NSInteger idx = [[LAnnotationPicker weights] indexOfObject:@(weight)];
        self.thicknessPalette.index = idx;
    }
}


#pragma mark - Utilities
//*********************************************************************************************************************//

+ (NSArray *)colors
{
    static NSArray *colors = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        colors = @[
            [UIColor LBlackColor],
            [UIColor LRedColor],
            [UIColor LOrangeColor],
            [UIColor LYellowColor],
            [UIColor LGreenColor],
            [UIColor LBlueColor],
            [UIColor LVioletColor],
            [UIColor LPinkColor],
        ];
    });
    return colors;
}

+ (NSArray *)weights
{
    static NSArray *widths = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        widths = @[@3., @9., @15., @21.];
    });
    return widths;
}

+ (UIColor *)colorForIndex:(NSInteger)index
{
    assert(index >= 0 && index < [LAnnotationPicker colors].count);
    return [LAnnotationPicker colors][index];
}

+ (float)weightForIndex:(NSInteger)index
{
    assert(index >= 0 && index < [LAnnotationPicker weights].count);
    return [[LAnnotationPicker weights][index] floatValue];
}


#pragma mark - LColorPalette Delegate
//*********************************************************************************************************************//

- (void)colorPalette:(LColorPalette *)colorPalette didSelectColor:(UIColor *)color
{
    self.color = color;
    [self.delegate annotationPicker:self didChangeColor:color];
}


#pragma mark - LThicknessPalette Delegate
//*********************************************************************************************************************//

- (void)thicknessPalette:(LThicknessPalette *)thicknessPalette didChangeIndex:(NSInteger)index
{
    float weight = [LAnnotationPicker weightForIndex:index];
    self.weight = weight;
    [self.delegate annotationPicker:self didChangeWeight:weight];
}


@end

