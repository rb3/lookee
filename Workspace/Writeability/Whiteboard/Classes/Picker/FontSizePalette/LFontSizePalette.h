//
//  LFontSizePalette.h
//  Writeability
//
//  Created by Jelo Agnasin on 11/26/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LFontSizePalette;

@protocol LFontSizePaletteDelegate <NSObject>

- (void)fontSizePalette:(LFontSizePalette *)fontSizePalette didChangeIndex:(NSInteger)index;

@end


@interface LFontSizePalette : UIView

@property (nonatomic, readwrite, weak) id <LFontSizePaletteDelegate> delegate;
@property (nonatomic, readwrite, assign) NSInteger selectedIndex;

- (instancetype)initWithSizes:(NSArray *)sizes
               rows:(NSInteger)rows
            columns:(NSInteger)columns
               size:(CGSize)size;

@end
