//
//  LFontSelector.h
//  Writeability
//
//  Created by Jelo Agnasin on 11/26/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LFontSelector;

@protocol LFontSelectorDelegate <NSObject>

- (void)fontSelector:(LFontSelector *)fontSelector didChangeFontName:(NSString *)fontName;

@end

@interface LFontSelector : UIView

@property (nonatomic, readwrite, weak) id <LFontSelectorDelegate> delegate;
@property (nonatomic, readwrite, strong) NSString *selectedFontName;
@property (nonatomic, readwrite, assign) NSInteger selectedIndex;

- (instancetype)initWithReadableFontNames:(NSArray *)readableFontNames
                    fontMapping:(NSDictionary *)fontMapping;

@end
