//
//  LFontSelector.m
//  Writeability
//
//  Created by Jelo Agnasin on 11/26/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//

#import "LFontSelector.h"

#import "LConstants.h"

// custom view
#import "LPopoverController.h"


#pragma mark - LFontTableView Interface
//*********************************************************************************************************************//

@interface LFontTableView : UITableView

- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style fontSelector:(LFontSelector *)fontSelector;

@end


#pragma mark - LFontSelector Interface
//*********************************************************************************************************************//

@interface LFontSelector ()

@property (nonatomic, readwrite, strong) NSArray *readableFontNames;
@property (nonatomic, readwrite, strong) NSDictionary *fontMapping;
@property (nonatomic, readwrite, strong) UIButton *fontDisplay;

@property (nonatomic, readwrite, strong) LPopoverController *popover;
@property (nonatomic, readwrite, strong) LFontTableView *fontTableView;

@end


//*********************************************************************************************************************//
#pragma mark -
#pragma mark LFontSelector
#pragma mark -
//*********************************************************************************************************************//

@implementation LFontSelector


#pragma mark - Static Constants
//*********************************************************************************************************************//

static const CGFloat kWidth = 170.0;
static const CGFloat kHeight = 44.0;

static const CGFloat kFontSize = 16.0;


#pragma mark - Memory Management
//*********************************************************************************************************************//

- (instancetype)initWithReadableFontNames:(NSArray *)readableFontNames
                    fontMapping:(NSDictionary *)fontMapping
{
    self = [super initWithFrame:CGRectMake(0, 0, kWidth, kHeight)];
    if (!self) return nil;
    
    self.readableFontNames = readableFontNames;
    self.fontMapping = fontMapping;
    self.fontTableView = [[LFontTableView alloc]
                          initWithFrame :CGRectMake(0, 0, kWidth, kHeight * 8)
                          style         :UITableViewStylePlain
                          fontSelector  :self];
    self.selectedFontName = self.fontMapping[self.readableFontNames[self.selectedIndex]];
    
    [self setupFontDisplay];
    [self updateFontDisplay];
    
    return self;
}


#pragma mark - Properties
//*********************************************************************************************************************//

- (void)setSelectedFontName:(NSString *)selectedFontName
{
    if (![_selectedFontName isEqualToString:selectedFontName]) {
        _selectedFontName = selectedFontName;

        __weak typeof(self) this = self;
        [self.fontMapping enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *obj, BOOL *stop) {
            if ([obj isEqualToString:selectedFontName]) {
                this.selectedIndex = [this.readableFontNames indexOfObject:key];
                *stop = YES;
            }
        }];
        
        [self.delegate fontSelector:self didChangeFontName:selectedFontName];
    }
}

- (void)setSelectedIndex:(NSInteger)selectedIndex
{
    if (_selectedIndex != selectedIndex) {
        _selectedIndex = selectedIndex;
        
        NSString *readableFontName = self.readableFontNames[selectedIndex];
        NSString *fontName = self.fontMapping[readableFontName];
        self.selectedFontName = fontName;
        
        [self updateFontDisplay];
    }
}


#pragma mark - Setup Methods
//*********************************************************************************************************************//

- (void)setupFontDisplay
{
    self.fontDisplay = [[UIButton alloc] initWithFrame:self.bounds];
    [self.fontDisplay setTitleColor:[UIColor LBlackColor] forState:UIControlStateNormal];
    [self.fontDisplay addTarget:self action:@selector(didTap:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.fontDisplay];
}


#pragma mark - Utilities
//*********************************************************************************************************************//

- (void)updateFontDisplay
{
    NSString *readableFontName = self.readableFontNames[self.selectedIndex];
    NSString *fontName = self.fontMapping[readableFontName];
    
    self.fontDisplay.titleLabel.font = [UIFont fontWithName:fontName size:kFontSize];
    [self.fontDisplay setTitle:readableFontName forState:UIControlStateNormal];
}


#pragma mark - Actions
//*********************************************************************************************************************//

- (void)didTap:(UIButton *)sender
{
    LPopoverController *popover = [[LPopoverController alloc] initWithContentView:self.fontTableView];
    self.popover = popover;
    self.popover.presenterView = sender;
    self.popover.arrowDirection = UIPopoverArrowDirectionDown;
    
    __weak typeof(self) this = self;
    [self.popover setContentSizeCallback:^CGSize(LPopoverController *popover) {
        return this.fontTableView.frame.size;
    }];
    
    [self.popover setDismissCallback:^(LPopoverController *popover) {
        if ([this.popover isEqual:popover]) { this.popover = nil; }
    }];
    
    [self.fontTableView reloadData];
    [self.popover presentPopoverAnimated:YES];
}

@end


//*********************************************************************************************************************//
#pragma mark -
#pragma mark LFontTableView
#pragma mark -
//*********************************************************************************************************************//

@interface LFontTableView () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, readwrite, weak) LFontSelector *fontSelector;

@end

@implementation LFontTableView

- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style fontSelector:(LFontSelector *)fontSelector
{
    self = [super initWithFrame:frame style:style];
    if (!self) return nil;
    
    self.fontSelector = fontSelector;
    self.delegate = self;
    self.dataSource = self;
    
    self.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    return self;
}


#pragma mark - Table View Delegate
//*********************************************************************************************************************//

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.fontSelector.selectedIndex = indexPath.row;
    [self.fontSelector.popover dismissPopoverAnimated:YES];
}


#pragma mark - Table View Data Source
//*********************************************************************************************************************//

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *const kCellIdentifier = @"FontCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kCellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    NSString *readableFontName = self.fontSelector.readableFontNames[indexPath.row];
    NSString *fontName = self.fontSelector.fontMapping[readableFontName];
    BOOL selected = [fontName isEqualToString:self.fontSelector.selectedFontName];
    
    cell.textLabel.text = readableFontName;
    cell.textLabel.font = [UIFont fontWithName:fontName size:kFontSize];
    cell.accessoryType = selected ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.fontSelector.readableFontNames.count;
}

@end