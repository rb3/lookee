//
//  LColorPalette.h
//  Writeability
//
//  Created by Jelo Agnasin on 11/21/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//

#import <UIKit/UIKit.h>

#pragma mark - LColorPalette Delegate
//*********************************************************************************************************************//

@class LColorPalette;
@protocol LColorPaletteDelegate <NSObject>

- (void)colorPalette:(LColorPalette *)colorPalette didSelectColor:(UIColor *)color;

@end


#pragma mark - LColorPalette Interface
//*********************************************************************************************************************//

@interface LColorPalette : UIView

@property (nonatomic, readwrite, weak) id <LColorPaletteDelegate> delegate;
@property (nonatomic, readwrite, weak) UIColor *selectedColor;
@property (nonatomic, readwrite, assign) NSInteger selectedIndex;

- (instancetype)initWithColors:(NSArray *)colors
                rows:(NSInteger)rows
             columns:(NSInteger)columns
                size:(CGSize)size;

@end

