//
//  LColorPalette.m
//  Writeability
//
//  Created by Jelo Agnasin on 11/21/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//

#import "LColorPalette.h"

#import "LConstants.h"

// first-party
#import <QuartzCore/QuartzCore.h>


#pragma mark - LColorPalette Interface
//*********************************************************************************************************************//

@interface LColorPalette ()

@property (nonatomic, readwrite, strong) NSArray *colors;
@property (nonatomic, readwrite, assign) NSInteger rows;
@property (nonatomic, readwrite, assign) NSInteger columns;
@property (nonatomic, readwrite, assign) CGSize size;

@property (nonatomic, readwrite, strong) NSArray *colorLayers;
@property (nonatomic, readwrite, strong) CAShapeLayer *selectedLayer;

@end


//*********************************************************************************************************************//
#pragma mark -
#pragma mark LColorPalette
#pragma mark -
//*********************************************************************************************************************//

@implementation LColorPalette


#pragma mark - Static Constants
//*********************************************************************************************************************//

static const CGFloat kMargin = 6.0;


#pragma mark - Memory Management
//*********************************************************************************************************************//

- (instancetype)init
{
    assert(NO);
}

- (instancetype)initWithColors:(NSArray *)colors
                rows:(NSInteger)rows
             columns:(NSInteger)columns
                size:(CGSize)size
{
    self = [super init];
    if (!self) return nil;
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTap:)];
    [self addGestureRecognizer:tapGesture];
    
    self.colors = colors;
    self.rows = rows;
    self.columns = columns;
    self.size = size;
    
    [self setupColorLayers];
    [self setupSelectedLayer];
    
    return self;
}


#pragma mark - Properties
//*********************************************************************************************************************//

- (void)setSelectedColor:(UIColor *)selectedColor
{
    if (![_selectedColor isEqual:selectedColor]) {
        _selectedColor = selectedColor;
        
        NSInteger idx = [self.colors indexOfObject:selectedColor];
        self.selectedIndex = idx;
        
        [self.delegate colorPalette:self didSelectColor:selectedColor];
    }
}

- (void)setSelectedIndex:(NSInteger)selectedIndex
{
    if (_selectedIndex != selectedIndex) {
        _selectedIndex = selectedIndex;
        
        UIColor *color = self.colors[selectedIndex];
        self.selectedColor = color;
        
        [self updateSelectedLayer];
    }
}


#pragma mark - Setup Methods
//*********************************************************************************************************************//

- (void)setupColorLayers
{
    CGRect ovalFrame = CGRectZero;
    ovalFrame.origin.x = kMargin;
    ovalFrame.origin.y = kMargin;
    ovalFrame.size.width = self.size.width - 2 * kMargin;
    ovalFrame.size.height = self.size.height - 2 * kMargin;
    
    __block CGRect frame = CGRectZero;
    frame.size = self.size;
    
    NSMutableArray *colorLayers = [NSMutableArray arrayWithCapacity:self.colors.count];
    [self.colors enumerateObjectsUsingBlock:^(UIColor *color, NSUInteger idx, BOOL *stop) {
        CAShapeLayer *colorLayer = [[CAShapeLayer alloc] init];
        colorLayer.fillColor = color.CGColor;
        colorLayer.path = [UIBezierPath bezierPathWithOvalInRect:ovalFrame].CGPath;
        colorLayer.frame = frame;
        
        [self.layer addSublayer:colorLayer];
        [colorLayers addObject:colorLayer];
        
        // next frame
        frame.origin.x = [self computeX:(idx + 1)];
        frame.origin.y = [self computeY:(idx + 1)];
    }];
    self.colorLayers = colorLayers;
}

- (void)setupSelectedLayer
{
    CGRect ovalFrame = CGRectZero;
    ovalFrame.origin.x = kMargin / 2.0;
    ovalFrame.origin.y = kMargin / 2.0;
    ovalFrame.size.width = self.size.width - kMargin;
    ovalFrame.size.height = self.size.height - kMargin;
    
    self.selectedLayer = [[CAShapeLayer alloc] init];
    self.selectedLayer.fillColor = nil;
    self.selectedLayer.strokeColor = [UIColor LGrayColor].CGColor;
    self.selectedLayer.path = [UIBezierPath bezierPathWithOvalInRect:ovalFrame].CGPath;
    [self.layer addSublayer:self.selectedLayer];
}


#pragma mark - Utilities
//*********************************************************************************************************************//

- (CGFloat)computeX:(NSInteger)idx
{
    return (idx % self.columns) * self.size.width;
}

- (CGFloat)computeY:(NSInteger)idx
{
    return (idx / self.columns) * self.size.height;
}

- (void)updateSelectedLayer
{
    CGRect frame = CGRectZero;
    frame.size = self.size;
    frame.origin.x = [self computeX:self.selectedIndex];
    frame.origin.y = [self computeY:self.selectedIndex];
    self.selectedLayer.frame = frame;
}


#pragma mark - Actions
//*********************************************************************************************************************//

- (void)didTap:(UITapGestureRecognizer *)sender
{
    CGPoint point = [sender locationInView:self];
    NSInteger index = (NSInteger)(point.x / self.size.width) + (NSInteger)(point.y / self.size.height) * self.columns;
    self.selectedIndex = index;
}


@end