//
//  LThicknessPalette.h
//  Writeability
//
//  Created by Jelo Agnasin on 11/25/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//

#import <UIKit/UIKit.h>

#pragma mark - LThicknessPalette Delegate
//*********************************************************************************************************************//

@class LThicknessPalette;
@protocol LThicknessPaletteDelegate <NSObject>

- (void)thicknessPalette:(LThicknessPalette *)thicknessPalette didChangeIndex:(NSInteger)index;

@end


#pragma mark - LThicknessPalette Interface
//*********************************************************************************************************************//

@interface LThicknessPalette : UIView

@property (nonatomic, readwrite, weak) id <LThicknessPaletteDelegate> delegate;
@property (nonatomic, readwrite, assign) NSInteger index;

@end

