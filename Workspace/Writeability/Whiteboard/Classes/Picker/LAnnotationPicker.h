//
//  LAnnotationPicker.h
//  Writeability
//
//  Created by Jelo Agnasin on 10/3/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LAnnotationPicker;

@protocol LAnnotationPickerDelegate <NSObject>

- (void)annotationPicker:(LAnnotationPicker *)annotationPicker didChangeColor:(UIColor *)color;
- (void)annotationPicker:(LAnnotationPicker *)annotationPicker didChangeWeight:(float)weight;

@end


@interface LAnnotationPicker : UIView

@property (nonatomic, readwrite, weak) id <LAnnotationPickerDelegate> delegate;

// selected properties
@property (nonatomic, readwrite, strong) UIColor *color;
@property (nonatomic, readwrite, assign) float weight;

+ (UIColor *)colorForIndex:(NSInteger)index;
+ (float)weightForIndex:(NSInteger)index;

@end
