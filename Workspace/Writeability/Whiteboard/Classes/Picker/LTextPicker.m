//
//  LTextPicker.m
//  Writeability
//
//  Created by Jelo Agnasin on 11/25/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//

#import "LTextPicker.h"

#import "LConstants.h"

// custom views
#import "LColorPalette.h"
#import "LFontSelector.h"
#import "LFontSizePalette.h"
#import "LLineLayer.h"

#pragma mark - LTextPicker Interface
//*********************************************************************************************************************//

@interface LTextPicker () <LColorPaletteDelegate, LFontSelectorDelegate, LFontSizePaletteDelegate>

@property (nonatomic, readwrite, strong) LLineLayer *lineLayer;

@property (nonatomic, readwrite, strong) LColorPalette *colorPalette;
@property (nonatomic, readwrite, strong) LFontSelector *fontSelector;
@property (nonatomic, readwrite, strong) LFontSizePalette *fontSizePalette;

@property (nonatomic, readwrite, assign) NSInteger colorPaletteColumns;
@property (nonatomic, readwrite, assign) NSInteger fontSizePaletteColumns;

@end


//*********************************************************************************************************************//
#pragma mark -
#pragma mark LTextPicker
#pragma mark -
//*********************************************************************************************************************//

@implementation LTextPicker


#pragma mark - Static Constants
//*********************************************************************************************************************//

static const CGFloat kHeight = 44.0;

static const NSInteger kColorPaletteRows = 1;
static const NSInteger kFontSizePaletteRows = 1;

static const CGFloat kColorPaletteItemSize = 32.0;
static const CGFloat kFontSizePaletteSize = 32.0;


#pragma mark - Memory Management
//*********************************************************************************************************************//

- (instancetype)init
{
    self = [super initWithFrame:CGRectMake(0, 0, 0, kHeight)];
    if (!self) return nil;
    
    self.backgroundColor = [UIColor whiteColor];
    
    self.lineLayer = [[LLineLayer alloc] init];
    self.lineLayer.backgroundColor = [UIColor LBorderColor].CGColor;
    [self.layer addSublayer:self.lineLayer];
    
    self.colorPaletteColumns = ceil([LTextPicker colors].count / kFontSizePaletteRows);
    self.colorPalette = [[LColorPalette alloc]
                         initWithColors :[LTextPicker colors]
                         rows           :kColorPaletteRows
                         columns        :self.colorPaletteColumns
                         size           :CGSizeMake(kColorPaletteItemSize, kColorPaletteItemSize)];
    self.colorPalette.delegate = self;
    [self addSubview:self.colorPalette];
    
    self.fontSelector = [[LFontSelector alloc]
                         initWithReadableFontNames:[LTextPicker readableFontNames]
                         fontMapping:[LTextPicker fontMapping]];
    self.fontSelector.delegate = self;
    [self addSubview:self.fontSelector];
    
    self.fontSizePaletteColumns = ceil([LTextPicker sizes].count / kFontSizePaletteRows);
    self.fontSizePalette = [[LFontSizePalette alloc]
                            initWithSizes   :[LTextPicker sizes]
                            rows            :kFontSizePaletteRows
                            columns         :self.fontSizePaletteColumns
                            size            :CGSizeMake(kFontSizePaletteSize, kFontSizePaletteSize)];
    self.fontSizePalette.delegate = self;
    [self addSubview:self.fontSizePalette];
    
    return self;
}


#pragma mark - Layout Subviews
//*********************************************************************************************************************//

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect frame;
    
    frame.origin = CGPointZero;
    frame.size.width = self.bounds.size.width;
    self.lineLayer.frame = frame;
    
    
    CGFloat colorPaletteWidth = self.colorPaletteColumns * kColorPaletteItemSize;
    CGFloat colorPaletteHeight = kColorPaletteRows * kColorPaletteItemSize;
    
    frame.origin.x = self.bounds.size.width - colorPaletteWidth - LC_DEFAULT_MARGIN;
    frame.origin.y = (self.bounds.size.height - colorPaletteHeight) / 2.0;
    frame.size.width = colorPaletteWidth;
    frame.size.height = colorPaletteHeight;
    self.colorPalette.frame = frame;
    
    
    frame = self.fontSelector.frame;
    frame.origin.x = LC_DEFAULT_MARGIN;
    frame.origin.y = (self.bounds.size.height - frame.size.height) / 2.0;
    self.fontSelector.frame = frame;
    
    
    CGFloat fontSizePaletteWidth = self.fontSizePaletteColumns * kColorPaletteItemSize;
    CGFloat fontSizePaletteHeight = kFontSizePaletteRows * kFontSizePaletteSize;
    
    frame.origin.x = (self.bounds.size.width - fontSizePaletteWidth) / 2.0;
    frame.origin.y = (self.bounds.size.height - fontSizePaletteHeight) / 2.0;
    frame.size.width = fontSizePaletteWidth;
    frame.size.height = fontSizePaletteHeight;
    self.fontSizePalette.frame = frame;
}


#pragma mark - Properties
//*********************************************************************************************************************//

- (void)setColor:(UIColor *)color
{
    if ([[LTextPicker colors] containsObject:color]) {
        _color = color;
        self.colorPalette.selectedColor = color;
    }
}

- (void)setSize:(NSNumber *)size
{
    if ([[LTextPicker sizes] containsObject:size]) {
        _size = size;
        self.fontSizePalette.selectedIndex = [[LTextPicker sizes] indexOfObject:size];
    }
}

- (void)setFontName:(NSString *)fontName
{
    if ([[LTextPicker readableFontNames] containsObject:fontName]) {
        _fontName = fontName;
        self.fontSelector.selectedIndex = [[LTextPicker readableFontNames] indexOfObject:fontName];
    }
}


#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (NSArray *)colors
{
    static NSArray *colors = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        colors = @[
            [UIColor LBlackColor],
            [UIColor LRedColor],
            [UIColor LOrangeColor],
            [UIColor LYellowColor],
            [UIColor LGreenColor],
            [UIColor LBlueColor],
            [UIColor LVioletColor],
            [UIColor LPinkColor],
        ];
    });
    return colors;
}

+ (NSArray *)sizes
{
    static NSArray *sizes = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizes = @[@12, @16, @20, @24];
    });
    return sizes;
}

+ (NSArray *)readableFontNames
{
    static NSArray *fonts = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        fonts = @[
            @"Arial",
            @"Chalkduster",
            @"Courier New",
            @"Georgia",
            @"Helvetica",
            @"Noteworthy",
            @"Times New Roman",
            @"Verdana",
        ];
    });
    return fonts;
}

+ (NSDictionary *)fontMapping
{
    static NSDictionary *fontMapping = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        fontMapping = @{
            @"Arial"            :@"ArialMT",
            @"Chalkduster"      :@"Chalkduster",
            @"Courier New"      :@"CourierNewPSMT",
            @"Georgia"          :@"Georgia",
            @"Helvetica"        :@"Helvetica",
            @"Noteworthy"       :@"Noteworthy-Light",
            @"Times New Roman"  :@"TimesNewRomanPSMT",
            @"Verdana"          :@"Verdana",
        };
    });
    return fontMapping;
}

+ (UIColor *)colorForIndex:(NSInteger)index
{
    assert(index >= 0 && index < [LTextPicker colors].count);
    return [LTextPicker colors][index];
}

+ (NSNumber *)sizeForIndex:(NSInteger)index
{
    assert(index >= 0 && index < [LTextPicker sizes].count);
    return [LTextPicker sizes][index];
}

+ (NSString *)fontNameForIndex:(NSInteger)index
{
    assert(index >= 0 && index < [LTextPicker readableFontNames].count);
    return [LTextPicker fontMapping][[LTextPicker readableFontNames][index]];
}


#pragma mark - Utilities
//*********************************************************************************************************************//

+ (void)testFonts
{
    NSArray *familyNames = [[UIFont familyNames] sortedArrayUsingSelector:@selector(compare:)];
    for (NSString *familyName in familyNames) {
        NSArray *fontNames = [[UIFont fontNamesForFamilyName:familyName] sortedArrayUsingSelector:@selector(compare:)];
        NSLog(@"family: %@", familyName);
        NSLog(@"fonts: %@", fontNames);
    }
}


#pragma mark - LColorPalette Delegate
//*********************************************************************************************************************//

- (void)colorPalette:(LColorPalette *)colorPalette didSelectColor:(UIColor *)color
{
    self.color = color;
    [self.delegate textPicker:self didChangeColor:color];
}

#pragma mark - LFontSizePalette Delegate
//*********************************************************************************************************************//

- (void)fontSizePalette:(LFontSizePalette *)fontSizePalette didChangeIndex:(NSInteger)index
{
    NSNumber *size = [LTextPicker sizeForIndex:index];
    self.size = size;
    [self.delegate textPicker:self didChangeSize:size];
}


#pragma mark - LFontSelector Delegate
//*********************************************************************************************************************//

- (void)fontSelector:(LFontSelector *)fontSelector didChangeFontName:(NSString *)fontName
{
    // TODO: implement
    self.fontName = fontName;
    [self.delegate textPicker:self didChangeFontName:fontName];
}


@end
