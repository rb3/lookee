//
//  LPage+UndoManager.m
//  Writeability
//
//  Created by Ryan on 7/3/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//

#import "LPage+UndoManager.h"

#import <Annotation/LAnnotator.h>





//*********************************************************************************************************************//
#pragma mark -
#pragma mark LPage+UndoManager
#pragma mark -
//*********************************************************************************************************************//





@implementation LPage (UndoManager)

#pragma mark - Public Methods
//*********************************************************************************************************************//

- (instancetype)recordUndoOperation
{
    return [self.currentAnnotator.undoManager prepareWithInvocationTarget:self];
}

- (void)addAnnotation:(LAnnotation *)annotation
{
    [self insertAnnotation:annotation atIndex:self.currentAnnotator.annotationCount];
}

- (void)insertAnnotation:(LAnnotation *)annotation atIndex:(NSUInteger)index
{
    if (index != NSNotFound) {
        [self.currentAnnotator insertAnnotation:annotation atIndex:index];

        [[self recordUndoOperation] removeAnnotationAtIndex:index];
    }
}

- (void)removeLastAnnotation
{
    NSUInteger annotationCount = [self.currentAnnotator annotationCount];

    if (annotationCount -- > 0) {
        [self removeAnnotationAtIndex:annotationCount];
    }
}

- (void)removeAnnotationAtIndex:(NSUInteger)index
{
    if (index != NSNotFound) {
        LAnnotation *annotation = [self.currentAnnotator annotationAtIndex:index];

        [[self recordUndoOperation] insertAnnotation:annotation atIndex:index];

        [self.currentAnnotator removeAnnotationAtIndex:index];
    }
}

- (BOOL)canUndo
{
    return [self.currentAnnotator.undoManager canUndo];
}

- (BOOL)canRedo
{
    return [self.currentAnnotator.undoManager canRedo];
}

- (void)undo
{
    [self.currentAnnotator.undoManager undo];
}

- (void)redo
{
    [self.currentAnnotator.undoManager redo];
}

@end
