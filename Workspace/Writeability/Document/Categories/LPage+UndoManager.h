//
//  LPage+UndoManager.h
//  Writeability
//
//  Created by Ryan on 7/3/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LPage.h"



@class LAnnotation;



@interface LPage (UndoManager)

- (instancetype)recordUndoOperation;

- (void)addAnnotation:(LAnnotation *)annotation;
- (void)insertAnnotation:(LAnnotation *)annotation atIndex:(NSUInteger)index;

- (void)removeLastAnnotation;
- (void)removeAnnotationAtIndex:(NSUInteger)index;

- (BOOL)canUndo;
- (BOOL)canRedo;

- (void)undo;
- (void)redo;

@end
