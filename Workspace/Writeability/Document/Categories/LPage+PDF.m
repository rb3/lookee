//
//  LPage+PDF.m
//  Writeability
//
//  Created by Ryan Blonna on 26/10/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LPage+PDF.h"

#import "LDocument.h"

#import <Utilities/LPDF.h>





//*********************************************************************************************************************//
#pragma mark -
#pragma mark LPage+PDF
#pragma mark -
//*********************************************************************************************************************//





@implementation LPage (PDF)

#pragma mark - Protected Methods
//*********************************************************************************************************************//

- (CGSize)getPDFDimensions
{
    NSUInteger  pageNumber = [self pageNumber];

    CGSize      dimensions;

    LPDFInfo([self.document URL], &pageNumber, &dimensions);

    return dimensions;
}

- (void)renderPDFContent
{
    LPDFRenderToContext([self.document URL], [self pageNumber], UIGraphicsGetCurrentContext());
}

@end
