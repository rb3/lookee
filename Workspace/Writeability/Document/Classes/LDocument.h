//
//  LDocument.h
//  Writeability
//
//  Created by Ryan on 2/5/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//



#import <UIKit/UIKit.h>



@class      LPage;

@protocol   LDocumentDelegate;



FOUNDATION_EXTERN NSString *const kLDocumentBadFileNotification;



@interface LDocument : NSObject

@property (nonatomic, readonly  , strong) NSURL             *URL;

@property (nonatomic, readonly  , assign) NSUInteger        pageCount;

@property (nonatomic, readonly  , assign) CGFloat           scale;

@property (nonatomic, readonly  , strong) NSArray           *pages;

@property (nonatomic, readwrite , weak  ) id                <LDocumentDelegate>delegate;


+ (instancetype)documentForURL:(NSURL *)URL scale:(CGFloat)scale;

- (instancetype)initWithURL:(NSURL *)URL scale:(CGFloat)scale;


- (void)addPage:(LPage *)page;
- (void)insertPage:(LPage *)page atLocation:(NSUInteger)location;

- (LPage *)pageAtLocation:(NSUInteger)location;

- (void)removePageAtLocation:(NSUInteger)location;
- (void)removeAllPages;

/**
 * Virtual methods
 */
- (void)exportToDestination:(NSURL *)destination;

@end


@protocol LDocumentDelegate <NSObject>
@optional

- (void)document:(LDocument *)document didExportPageAtLocation:(NSUInteger)location;

@end
