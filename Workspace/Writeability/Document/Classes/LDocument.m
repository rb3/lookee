//
//  LDocument.m
//  Writeability
//
//  Created by Ryan on 2/5/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//



#import "LDocument.h"

#import "LPage.h"

#import <Utilities/LMacros.h>
#import <Utilities/NSObject+Utilities.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Global Constants
#pragma mark -
//*********************************************************************************************************************//




StringConst(kLDocumentBadFileNotification);




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LFileProperties
#pragma mark -
//*********************************************************************************************************************//




@implementation LDocument

#pragma mark - Instance Variables
//*********************************************************************************************************************//
{
    NSMutableArray *_pages;
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

+ (instancetype)documentForURL:(NSURL *)URL scale:(CGFloat)scale
{
    return [[self alloc] initWithURL:URL scale:scale];
}

- (instancetype)init __ILLEGALMETHOD__;

- (instancetype)initWithURL:(NSURL *)URL scale:(CGFloat)scale
{
    DBGParameterAssert(URL != nil);

    if ((self = [super init])) {
        _URL    = URL;

        _scale  = scale;
        
        _pages  = [NSMutableArray new];

        NSUInteger pageCount = [self getPageCount];

        for (NSUInteger
             pageNumber  = 1;
             pageNumber <= pageCount;
             pageNumber ++)
        {
            [self addPage:[LPage pageForDocument:self pageNumber:pageNumber]];
        }
    }

    return self;
}

- (void)dealloc
{
    {_pages = nil;}

    DBGMessage(@"deallocated %@", self);
}


#pragma mark - Properties
//*********************************************************************************************************************//

@dynamic pageCount;

- (NSUInteger)pageCount
{
    return [_pages count];
}

@dynamic pages;

- (NSArray *)pages
{
    return [_pages copy];
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (NSUInteger)getPageCount
{
    NSString *fileExtension = [self.URL.pathExtension uppercaseString];

    NSString *selname  = @Format(@"get%@PageCount", fileExtension);

    SEL selector = NSSelectorFromString(selname);

    if ([self respondsToSelector:selector]) {
        NSInvocation    *invocation = [self invocationForSelector:selector];

        NSUInteger      pageCount   ; [invocation invoke], [invocation getReturnValue:&pageCount];

        return pageCount;
    }

    return 0;
}

- (void)exportToDestination:(NSURL *)destination
{
    NSString *fileExtension = [self.URL.pathExtension uppercaseString];

    NSString *selname  = @Format(@"export%@ToDestination:", fileExtension);

    SEL selector = NSSelectorFromString(selname);

    if ([self respondsToSelector:selector]) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [self performSelector:selector withObject:destination];
#pragma clang diagnostic pop
    }
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (void)addPage:(LPage *)page
{
    [self insertPage:page atLocation:self.pageCount];
}

- (void)insertPage:(LPage *)page atLocation:(NSUInteger)location
{
    DBGParameterAssert(location <= self.pageCount);

    if (location == [_pages count]) {
        [_pages addObject:page];
    } else {
        [_pages insertObject:page atIndex:location];
    }
}

- (LPage *)pageAtLocation:(NSUInteger)location
{
    DBGParameterAssert((location > 0) && (location <= self.pageCount));

    return _pages[location-1];
}

- (void)removePageAtLocation:(NSUInteger)location
{
    DBGParameterAssert((location > 0) && (location <= self.pageCount));

    [_pages removeObjectAtIndex:location-1];
}

- (void)removeAllPages
{
    [_pages removeAllObjects];
}

@end
