//
//  LPoint.m
//  Writeability
//
//  Created by Ryan on 6/26/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LPoint.h"

#import <Utilities/LMacros.h>
#import <Utilities/LGeometry.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LPoint
#pragma mark -
//*********************************************************************************************************************//




@implementation LPoint

#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (instancetype)pointWithCoordinates:(CGPoint)coordinates
{
    return [[self alloc] initWithCoordinates:coordinates];
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)initWithCoordinates:(CGPoint)coordinates
{
    if ((self = [super init])) {
        _coordinates = coordinates;
    }

    return self;
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (NSString *)description
{
    return [NSString stringWithFormat:
            @"<%@: %p, coordinates: %@>",
            $classname,
            self,
            NSStringFromCGPoint(self.coordinates)];
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LStartPoint
#pragma mark -
//*********************************************************************************************************************//




@implementation LStartPoint

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LEndPoint
#pragma mark -
//*********************************************************************************************************************//




@implementation LEndPoint

@end
