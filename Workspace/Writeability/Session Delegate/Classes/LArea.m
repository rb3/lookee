//
//  LRect.m
//  Writeability
//
//  Created by Ryan on 8/25/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LArea.h"

#import <Utilities/LMacros.h>





//*********************************************************************************************************************//
#pragma mark -
#pragma mark LArea
#pragma mark -
//*********************************************************************************************************************//





@implementation LArea

#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (instancetype)areaWithRect:(CGRect)rect
{
    return [[self alloc] initWithRect:rect];
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)initWithRect:(CGRect)rectangle
{
    if ((self = [super init])) {
        _rect = rectangle;
    }

    return self;
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (NSString *)description
{
    return [NSString stringWithFormat:
            @"<%@: %p, rectangle: %@>",
            $classname,
            self,
            NSStringFromCGRect(self.rect)];
}

@end
