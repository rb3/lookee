//
//  LRect.h
//  Writeability
//
//  Created by Ryan on 8/25/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <UIKit/UIKit.h>

#import <Utilities/LSerializable.h>


@interface LArea : NSObject <LSerializable>

@property (nonatomic, readwrite, assign) CGRect rect;


+ (instancetype)areaWithRect:(CGRect)rect;

@end
