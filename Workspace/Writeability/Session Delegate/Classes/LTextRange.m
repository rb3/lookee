//
//  LTextIndex.m
//  Writeability
//
//  Created by Ryan on 8/26/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LTextRange.h"

#import <Utilities/LMacros.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LTextIndex
#pragma mark -
//*********************************************************************************************************************//




@implementation LTextRange

#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (instancetype)textRangeWithBounds:(NSRange)bounds
{
    return [[self alloc] initWithBounds:bounds];
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)initWithBounds:(NSRange)bounds
{
    if ((self = [super init])) {
        _bounds = bounds;
    }

    return self;
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (NSString *)description
{
    return [NSString stringWithFormat:
            @"<%@: %p, bounds: %@>",
            $classname,
            self,
            NSStringFromRange(self.bounds)];
}

@end
