//
//  LPageZoom.h
//  Writeability
//
//  Created by Ryan on 6/28/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <UIKit/UIKit.h>

#import <Utilities/LSerializable.h>


@interface LPageZoom : NSObject <LSerializable>

@property (nonatomic, readwrite, assign) CGRect rect;


+ (instancetype)pageZoomToRect:(CGRect)rect;

@end
