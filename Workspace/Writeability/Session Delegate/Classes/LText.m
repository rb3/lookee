//
//  LText.m
//  Writeability
//
//  Created by Ryan on 8/25/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LText.h"

#import <Utilities/LMacros.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LText
#pragma mark -
//*********************************************************************************************************************//




@implementation LText

#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (instancetype)textWithString:(NSString *)string
{
    return [[self alloc] initWithString:string];
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)initWithString:(NSString *)string
{
    if ((self = [super init])) {
        _string = [string copy];
    }

    return self;
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (NSString *)description
{
    return [NSString stringWithFormat:
            @"<%@: %p, string: %@>",
            $classname,
            self,
            self.string];
}

@end
