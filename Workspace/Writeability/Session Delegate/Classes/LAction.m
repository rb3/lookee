//
//  LAction.m
//  Writeability
//
//  Created by Ryan on 6/30/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LAction.h"

#import <Utilities/LMacros.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LAction
#pragma mark -
//*********************************************************************************************************************//




@implementation LAction

#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (instancetype)actionWithName:(NSString *)name
{
    return [[self alloc] initWithName:name];
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)initWithName:(NSString *)name
{
    if ((self = [super init])) {
        _name = name;
    }

    return self;
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (NSString *)description
{
    return [NSString stringWithFormat:
            @"<%@: %p, name: %@>",
            $classname,
            self,
            self.name];
}

@end
