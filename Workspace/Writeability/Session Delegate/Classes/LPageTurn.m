//
//  LPageTurn.m
//  Writeability
//
//  Created by Ryan on 6/28/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LPageTurn.h"

#import <Utilities/LMacros.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LPageTurn
#pragma mark -
//*********************************************************************************************************************//




@implementation LPageTurn

#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (instancetype)pageTurnToPageNumber:(NSUInteger)pageNumber
{
    return [[self alloc] initWithPageNumber:pageNumber];
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)initWithPageNumber:(NSUInteger)pageNumber
{
    if ((self = [super init])) {
        _pageNumber = pageNumber;
    }

    return self;
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (NSString *)description
{
    return [NSString stringWithFormat:
            @"<%@: %p, pageNumber: %lu>",
            $classname,
            self,
            self.pageNumber];
}

@end
