### Lookee ###

This is the repository for Lookee, the most awesome classroom collaboration application ever made. Because it is so awesome, it is also very hard to obtain the code for it.

Okay, now lets get serious. Lookee is made up of the following modules:

1. The front end (this repository)
2. [Networking](https://bitbucket.org/rb3/lookee.networking)
3. [Rendering](https://bitbucket.org/rb3/lookee.rendering)
4. [Annotation](https://bitbucket.org/rb3/lookee.annotation)
5. [Utilities](https://bitbucket.org/rb3/lookee.utilities)

Each module lives in its own private bitbucket repository, and are included as git submodules in the front end module. However, since each submodule is private, anyone executing: 

  ~~`git clone --recursive https://<bitbucket-user>@bitbucket.org/rb3/lookee.git Lookee`~~

 ...will run into authentication problems for each submodule. One solution to this problem is to clone each module independently by executing the following commands:
```
#!bash

cd <root-directory>
git clone https://<your-bitbucket-username>@bitbucket.org/rb3/lookee.git Lookee
nano Lookee/.gitmodules # edit each submodule entry so that https://<bitbucket-username>@bitbucket.org/rb3/<submodule-name>.git has the correct username, save it with CTRL-X, and hit enter
cd Lookee
git submodule update --init --recursive # wait for it...
```

After everything is cloned, you will have to check out the correct branches for certain submodules:
```
#!bash

cd Workspace/Core/Networking
git checkout release
cd Vendor/OpenSSL
git checkout OpenSSL_1_0_1-stable
```

...and we're done! You may now open Writeability.xcworkspace in the Lookee directory, and code away. :)

**Notes:**

* In order to successfully build the project, `scons` and `cocoapods` must be installed on the system.

* The `pods update` command may need to be run before opening the project for the first time.

* If linker errors related to `OpenSSL` are encountered, one may need to switch to a later stable branch than the one indicated in this README.

* The bundled `AllJoyn` and `OpenSSL` libraries may take significant time to build the first time.

* The `Paho` and `OpenSSL` libraries may not build properly on the iPad Air simulator, because the build scripts generate x86_64 binaries instead of i386 binaries. This needs to be fixed in the next Lookee update.

* A more in-depth guide can be found [here](https://drive.google.com/open?id=0B6PoRNCfTX2hcjFaZ3YwX3psdlk) if any difficulties are encountered.